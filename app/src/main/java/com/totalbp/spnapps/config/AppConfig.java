package com.totalbp.spnapps.config;

/**
 * Created by Ezra.R on 28/07/2017.
 */

public class AppConfig {

    //TESCIS
//    public static String URL_LOGIN = "http://10.100.3.11:2017/API/Security/RequestToken";
//    public static String URL_LOGIN_OLD = "http://10.100.3.11/TBPService/Service.svc/rest/LoginRest";
//    public static final String URL_PAGING = "http://10.100.3.11/TBPService/Service.svc/rest/GetDDLRest";
//    public static final String URL_IMAGE_PREFIX = "http://testcis.totalbp.com/CISUploads";
//    public static final String URL_CHECK_TOKEN = "http://10.100.3.11:2017/API/Security/CheckToken";
//    public static final String URL_CRUD_SPN = "http://testcis.totalbp.com/REST/API/Transaction/SPN/Save";
//    public static final String URL_PAGING_RESTFULL_NEWDLL = "http://testcis.totalbp.com/REST/API/General/GetDataDDL";
//    public static final String URL_NEW_APPROVAL = "http://testcis.totalbp.com/REST/API/General/Approval/Core";

    //APP
    public static String URL_LOGIN = "API/Security/RequestToken";
    public static String URL_LOGIN_OLD = "TBPService/Service.svc/rest/LoginRest";
    public static final String URL_PAGING = "TBPService/Service.svc/rest/GetDDLRest";
    public static final String URL_IMAGE_PREFIX = "CISUploads";
    public static final String URL_CHECK_TOKEN = "API/Security/CheckToken";
    public static final String URL_CRUD_SPN = "REST/API/Transaction/SPN/Save";
    public static final String URL_DELETE_SPN = "REST/API/Transaction/SPN/Delete";
    public static final String URL_PAGING_RESTFULL_NEWDLL = "REST/API/General/GetDataDDL";
    public static final String URL_NEW_APPROVAL = "REST/API/General/Approval/Core";
    public static final String urlProfileFromTBP = "assets/images/avatar/";
    public static final String URL_PRIVILAGE = "REST/API/General/GetUserPrivileges";
}
