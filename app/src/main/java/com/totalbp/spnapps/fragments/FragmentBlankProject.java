package com.totalbp.spnapps.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.totalbp.spnapps.R;
import com.totalbp.spnapps.adapter.SortAdapter;
import com.totalbp.spnapps.config.SessionManager;
import com.totalbp.spnapps.controller.AppController;
import com.totalbp.spnapps.model.CommonEnt;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class FragmentBlankProject extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ArrayList<CommonEnt> listItems = new ArrayList<>();
    private ListView listViewProject;
    public SortAdapter adapter;
    JSONArray jsonListSPNDummy = null;
    JSONObject item;
    private SessionManager session;
    private AppController controller;

    public FragmentBlankProject() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new AppController();
        session = new SessionManager(getActivity());
        //Log.d("EZRA", loadJSONFromAsset());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_project_blank, container, false);

        return view;
    }


}
