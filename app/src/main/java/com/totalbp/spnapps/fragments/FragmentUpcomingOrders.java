package com.totalbp.spnapps.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.totalbp.spnapps.MainActivity;
import com.totalbp.spnapps.R;
import com.totalbp.spnapps.activity.ListPOActivity;
import com.totalbp.spnapps.activity.SPNDetail;
import com.totalbp.spnapps.config.AppConfig;
import com.totalbp.spnapps.config.SessionManager;
import com.totalbp.spnapps.controller.AppController;
import com.totalbp.spnapps.interfaces.VolleyCallback;
import com.totalbp.spnapps.model.UpcomingListPoEnt;
import com.totalbp.spnapps.receiver.ConnectivityReceiver;
import com.totalbp.spnapps.utils.MProgressDialog;
import com.totalbp.spnapps.utils.PicassoTransform.CircleTransform;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

/**
 * Created by Ezra.R on 11/08/2017.
 */

public class FragmentUpcomingOrders extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ConnectivityReceiver.ConnectivityReceiverListener{

    private SectionedRecyclerViewAdapter sectionAdapter;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
    private SessionManager session;
    private AppController controller;
    public ArrayList<UpcomingListPoEnt> listItems = new ArrayList<>();
    public ArrayList<UpcomingListPoEnt> listItems2 = new ArrayList<>();
    //List<UpcomingListPoEnt> Resultlist;
    String[] stringArray;
    ArrayList<UpcomingListPoEnt> commonEntArrayList;
    ArrayList<UpcomingListPoEnt> arraylist;
    Context context;
    String today, tomorrow, tomorrow2;
    String PoCodeSample;
    public ArrayList<UpcomingListPoEnt> list = new ArrayList<>();

    private static final int request_data_from  = 91;
    private Handler mHandler = new Handler();

    SwipeRefreshLayout swipeRefreshLayout;
    int sectionOneUp = 1;
    int sectionTwoUp = 1;
    int sectionThreeUp = 1;
    int sectionFourUp = 1;
    int sectionFiveUp = 1;
    int sectionSixUp = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        controller = new AppController();
        session = new SessionManager(getActivity());
        Calendar calendar = new GregorianCalendar();
       // calendar.add(Calendar.DAY_OF_MONTH, -1);
        today = dateFormat.format(calendar.getTime());
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        tomorrow = dateFormat.format(calendar.getTime());
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        tomorrow2 = dateFormat.format(calendar.getTime());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upcoming_orders, container, false);
        //SetUpcomingData();

//        MProgressDialog.showProgressDialog(getActivity(), true, new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                MProgressDialog.dismissProgressDialog();
//            }
//        });

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        sectionAdapter = new SectionedRecyclerViewAdapter();

        NewsSection worldNews = new NewsSection(NewsSection.WORLD, "", "");
        NewsSection worldBussines = new NewsSection(NewsSection.BUSINESS, "", "");
        NewsSection worldTechnology = new NewsSection(NewsSection.TECHNOLOGY, "", "");
        NewsSection POWITHOUTDR = new NewsSection(NewsSection.POWITHOUTDR, "", "");
        NewsSection KONTRAKWITHOUTDR = new NewsSection(NewsSection.KONTRAKWITHOUTDR, "", "");
        NewsSection NPO = new NewsSection(NewsSection.NPO, "", "");


        sectionAdapter.addSection(worldNews);
        sectionAdapter.addSection(worldBussines);
        sectionAdapter.addSection(worldTechnology);
        sectionAdapter.addSection(POWITHOUTDR);
        sectionAdapter.addSection(KONTRAKWITHOUTDR);
        sectionAdapter.addSection(NPO);


//        loadNews(worldNews);
//        loadNews(worldBussines);
//        loadNews(worldTechnology);
//        SetUpcomingDataInitial(0);
//        SetUpcomingDataInitial(1);
//        SetUpcomingDataInitial(2);

        //int timeInMills = new Random().nextInt((700 - 300) + 1) + 300;
//        int timeInMills = new Random().nextInt((2500 - 1200) + 1) + 1200;
        sectionAdapter.notifyDataSetChanged();

//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                int failed = new Random().nextInt((3 - 1) + 1) + 1;
//
//                if (failed == 1) {
//                    //section.setState(Section.State.FAILED);
//                    MProgressDialog.dismissProgressDialog();
//                }
//                else {
//                    SetUpcomingDataInitial(0);
//                    SetUpcomingDataInitial(1);
//                    SetUpcomingDataInitial(2);
//                    SetUpcomingDataInitial(3);
//                    MProgressDialog.dismissProgressDialog();
//                }
//
//                sectionAdapter.notifyDataSetChanged();
//            }
//        }, timeInMills);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(sectionAdapter);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if( requestCode == request_data_from) {
            if (data != null){

                List<Fragment> allFragments = getFragmentManager().getFragments();
                if (allFragments != null) {
                    for (Fragment fragment : allFragments) {
                        if(fragment.getClass() == FragmentAllSPN.class) {
                            Log.d("LogRefreshTrigger", "yes");
                            FragmentAllSPN f1 = (FragmentAllSPN) fragment;
                            f1.refreshDataFromActivity();

                            MainActivity main = (MainActivity) getActivity();
                            main.changeTab();
                        }
                    }
                }
            }
        }

    }


    @Override
    public void onResume() {
        super.onResume();
//
//        sectionAdapter.removeAllSections();
//        list.clear();
//
//        NewsSection worldNews = new NewsSection(NewsSection.WORLD, "", "");
//        NewsSection worldBussines = new NewsSection(NewsSection.BUSINESS, "", "");
//        NewsSection worldTechnology = new NewsSection(NewsSection.TECHNOLOGY, "", "");
//
//        sectionAdapter.addSection(worldNews);
//        sectionAdapter.addSection(worldBussines);
//        sectionAdapter.addSection(worldTechnology);
//
//        SetUpcomingDataInitial(0);
//        SetUpcomingDataInitial(1);
//        SetUpcomingDataInitial(2);
//
//        sectionAdapter.notifyDataSetChanged();
//        swipeRefreshLayout.setRefreshing(false);

    }

    private List<String> getNewsTest(int arrayResource) {
        return new ArrayList<>(Arrays.asList(getResources().getStringArray(arrayResource)));
    }

    private ArrayList getNews(int arrayResource) {
        return new ArrayList<>(Arrays.asList(getResources().getStringArray(arrayResource)));
    }


    public void updateDataFromActivity(String kodeKatalog, String kodeVendor, String dateRange, String endDate){

        sectionAdapter.removeAllSections();
        list.clear();

        if(!dateRange.equals(""))
        {
            List<String> myList = new ArrayList<String>(Arrays.asList(dateRange.split(",")));
            for (String date : myList) {
                String finalStr = date.replace("[", "");
                finalStr = finalStr.replace("]", "");
                finalStr = finalStr.replace(" ", "");

                Log.d("LogFilterUpcomingFrg", finalStr);
                NewsSection worldSport = new NewsSection(NewsSection.SPORTS, finalStr, kodeVendor);
                sectionAdapter.addSection(worldSport);
                //SetUpcomingDataInitialSearch(finalStr, kodeVendor);
            }
        }
        else
        {
            //Log.d("LogFilterUpcomingFrg", finalStr);
            NewsSection worldSport = new NewsSection(NewsSection.SPORTS, "", kodeVendor);
            sectionAdapter.addSection(worldSport);
        }
        sectionAdapter.notifyDataSetChanged();
    }

    public void updateDataFromActivitySort(String param1, String param2, String value, String param4){

        sectionAdapter.removeAllSections();
        list.clear();

        if(value.equals("ascnamavendor"))
        {
            value = " Upcoming.Nama_Vendor ASC ";

            List<String> dates = getDates(today, tomorrow2);
            //List<String> myList = new ArrayList<String>(Arrays.asList(dates.toString().split(",")));

            for(int i = 0; i < dates.size(); i ++)
            {
//                String finalStr = dates.toString().replace("[","");
//                finalStr = finalStr.replace("]","");
//                finalStr = finalStr.replace(" ","");

                Log.d("LogFilterUpcomingFrg",dates.get(i));
                Log.d("LogFilterUpcomingFrg",value);
                NewsSection worldSport = new NewsSection(NewsSection.SPORTS2, dates.get(i), value);
                sectionAdapter.addSection(worldSport);
                SetUpcomingDataInitialSearch2(dates.get(i), value);
            }
            sectionAdapter.notifyDataSetChanged();

        }
        else if(value.equals("descnamavendor"))
        {
            value = " Upcoming.Nama_Vendor DESC ";

            List<String> dates = getDates(today, tomorrow2);
            //List<String> myList = new ArrayList<String>(Arrays.asList(dates.toString().split(",")));

            for(int i = dates.size()-1; i >= 0; i --)
            {
//                String finalStr = dates.toString().replace("[","");
//                finalStr = finalStr.replace("]","");
//                finalStr = finalStr.replace(" ","");

                Log.d("LogFilterUpcomingFrg",dates.get(i));
                Log.d("LogFilterUpcomingFrg",value);
                NewsSection worldSport = new NewsSection(NewsSection.SPORTS2, dates.get(i), value);
                sectionAdapter.addSection(worldSport);
                SetUpcomingDataInitialSearch2(dates.get(i), value);
            }
            sectionAdapter.notifyDataSetChanged();
        }
        else if(value.equals("asctgldelivery"))
        {
            value = null;

            List<String> dates = getDates(today, tomorrow2);
            //List<String> myList = new ArrayList<String>(Arrays.asList(dates.toString().split(",")));

            for(int i = 0; i < dates.size(); i ++)
            {
//                String finalStr = dates.toString().replace("[","");
//                finalStr = finalStr.replace("]","");
//                finalStr = finalStr.replace(" ","");

                Log.d("LogFilterUpcomingFrg",dates.get(i));
//                Log.d("LogFilterUpcomingFrg",value);
                NewsSection worldSport = new NewsSection(NewsSection.SPORTS2, dates.get(i), value);
                sectionAdapter.addSection(worldSport);
                SetUpcomingDataInitialSearch2(dates.get(i), value);
            }
            sectionAdapter.notifyDataSetChanged();
        }
        else if(value.equals("desctgldelivery"))
        {
            value = null;

            List<String> dates = getDates(today, tomorrow2);
            //List<String> myList = new ArrayList<String>(Arrays.asList(dates.toString().split(",")));

            for(int i = dates.size()-1; i >= 0; i --)
            {
//                String finalStr = dates.toString().replace("[","");
//                finalStr = finalStr.replace("]","");
//                finalStr = finalStr.replace(" ","");

                Log.d("LogFilterUpcomingFrg",dates.get(i));
//                Log.d("LogFilterUpcomingFrg",value);
                NewsSection worldSport = new NewsSection(NewsSection.SPORTS2, dates.get(i), value);
                sectionAdapter.addSection(worldSport);
                SetUpcomingDataInitialSearch2(dates.get(i), value);
            }
            sectionAdapter.notifyDataSetChanged();
        }


    }

    @Override
    public void onRefresh() {
        sectionAdapter.removeAllSections();
        list.clear();

        NewsSection worldNews = new NewsSection(NewsSection.WORLD, "", "");
        NewsSection worldBussines = new NewsSection(NewsSection.BUSINESS, "", "");
        NewsSection worldTechnology = new NewsSection(NewsSection.TECHNOLOGY, "", "");
        NewsSection POWITHOUTDR = new NewsSection(NewsSection.POWITHOUTDR, "", "");
        NewsSection KONTRAKWITHOUTDR = new NewsSection(NewsSection.KONTRAKWITHOUTDR, "", "");
        NewsSection NPO = new NewsSection(NewsSection.NPO, "", "");


        sectionAdapter.addSection(worldNews);
        sectionAdapter.addSection(worldBussines);
        sectionAdapter.addSection(worldTechnology);
        sectionAdapter.addSection(POWITHOUTDR);
        sectionAdapter.addSection(KONTRAKWITHOUTDR);
        sectionAdapter.addSection(NPO);

//        SetUpcomingDataInitial(0);
//        SetUpcomingDataInitial(1);
//        SetUpcomingDataInitial(2);
//        SetUpcomingDataInitial(3);

        sectionAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private class NewsSection extends StatelessSection {

        final static int WORLD = 0;
        final static int BUSINESS = 1;
        final static int TECHNOLOGY = 2;
        final static int SPORTS = 3;
        final static int SPORTS2 = 4;
        final static int NPO = 5;
        final static int KONTRAKWITHOUTDR = 6;
        final static int POWITHOUTDR = 7;

        final int topic;

        String title;

        ArrayList<UpcomingListPoEnt> list = new ArrayList<>();
        int imgPlaceholderResId;
        boolean expanded = true;

        NewsSection(int topic, String tanggal, String kodeVendor) {
            super(new SectionParameters.Builder(R.layout.fragment_upcoming_orders_row)
                    .headerResourceId(R.layout.fragment_upcoming_orders_row_header)
                    .footerResourceId(R.layout.fragment_upcoming_orders_row_footer)
                    .build());

            this.topic = topic;
            this.list.clear();
            switch (topic) {
                case WORLD:
                    this.title = today;
                    this.list = SetUpcomingData2(0, String.valueOf("1"), String.valueOf("10"));
                    this.imgPlaceholderResId = R.drawable.ic_spn;
                    break;
                case BUSINESS:
                    this.title = tomorrow;
                    this.list = SetUpcomingData2(1, String.valueOf("1"), String.valueOf("10"));
                    this.imgPlaceholderResId = R.drawable.ic_spn;
                    break;
                case TECHNOLOGY:
                    this.title = tomorrow2;
                    this.list = SetUpcomingData2(2, String.valueOf("1"), String.valueOf("10"));
                    this.imgPlaceholderResId = R.drawable.ic_spn;
                    break;
                case SPORTS:
                    this.title = tanggal;
                    this.list = SetUpcomingDataSearch(tanggal, kodeVendor);
                    this.imgPlaceholderResId = R.drawable.ic_spn;
                    break;
                case SPORTS2:
                    this.title = tanggal;
                    this.list = SetUpcomingDataSearch2(tanggal, kodeVendor);
                    this.imgPlaceholderResId = R.drawable.ic_spn;
                    break;
                case KONTRAKWITHOUTDR:
                    this.title = "KONTRAK";
                    this.list = SetUpcomingData2(4, String.valueOf("1"), String.valueOf("10"));
                    this.imgPlaceholderResId = R.drawable.ic_spn;
                    break;
                case POWITHOUTDR:
                    this.title = "PO";
                    this.list = SetUpcomingData2(5, String.valueOf("1"), String.valueOf("10"));
                    this.imgPlaceholderResId = R.drawable.ic_spn;
                    break;
                case NPO:
                    this.title = "NPO";
                    this.list = SetUpcomingData2(3, String.valueOf("1"), String.valueOf("10"));
                    this.imgPlaceholderResId = R.drawable.ic_spn;
                    break;
            }
        }



        public void setList(ArrayList<UpcomingListPoEnt> list) {
            this.list = list;
        }

        int getTopic() {
            return topic;
        }

        public ArrayList SetUpcomingData2(int number, String pageup, String pagedown){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getActivity());
            String date = null;
            //listItems2.clear();

            switch (number) {
                case 0:
                    date = today;
                    break;
                case 1:
                    date = tomorrow;
                    break;
                case 2:
                    date = tomorrow2;
                    break;
                case 3:
                    date = "";
                    break;
                case 4:
                    date = "kontrakwithoutdr";
                    break;
                case 5:
                    date = "powithoutdr";
                    break;
            }

            //jika date tidak kosong dan bukan kontrakwithoutdr maka fungsi ini untuk PO dan Kontrak dengan DR
            if(date != "" && !date.equals("kontrakwithoutdr") && !date.equals("powithoutdr")) {
            controller.InqGeneralPagingFullEzra(getActivity().getApplicationContext(),"Get_Upcoming_PO_Orders_Mobile_Delivery_Request",
                    "@KodeProyek","'" + session.getKodeProyek() + "'",
                    "@currentpage",pageup,
                    "@pagesize",pagedown,
                    "@sortby","",
                    "@wherecond"," AND CONVERT(VARCHAR,tdDR.TanggalRencanaKirim,105) = '"+date+"' ",
                    "@nik",session.isNikUserLoggedIn(),
                    "@formid","PC.02.13",
                    "@zonaid","",

                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        UpcomingListPoEnt itemEnts = new UpcomingListPoEnt(
                                                item.getString("Nama_Vendor"),item.getString("Kode_PO"),item.getString("Total_Item") + " items",item.getString("ID"),item.getString("TanggalRencanaKirim"), item.getString("IDVendor"), item.getString("Tanggal_Rencana_Terima2"),item.getString("TransactionType"),item.getString("NoDeliveryRequest")
                                        );
                                        list.add(itemEnts);
                                    }
                                }
                                sectionAdapter.notifyDataSetChanged();
                            }catch (JSONException e){
                                Toast.makeText(getActivity(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            }
            else if (date.equals("kontrakwithoutdr")) //fungsi ini menampilkan kontrak tanpa DR
            {
                controller.InqGeneralPagingFullEzra(getActivity().getApplicationContext(), "Get_Upcoming_PO_Orders_Mobile_Delivery_Request",
                        "@KodeProyek", "'" + session.getKodeProyek() + "'",
                        "@currentpage", pageup,
                        "@pagesize", pagedown,
                        "@sortby", "",
                        "@wherecond", "kontrakwithoutdr",
                        "@nik", session.isNikUserLoggedIn(),
                        "@formid", "PC.02.13",
                        "@zonaid", "",

                        new VolleyCallback() {
                            @Override
                            public void onSave(String output) {

                            }

                            @Override
                            public void onSuccess(JSONArray result) {
                                try {
                                    swipeRefreshLayout.setRefreshing(false);
                                    if (result.length() > 0) {
                                        for (int i = 0; i < result.length(); i++) {
                                            JSONObject item = result.getJSONObject(i);

                                            UpcomingListPoEnt itemEnts = new UpcomingListPoEnt(
                                                    item.getString("Nama_Vendor"), item.getString("Kode_PO"), item.getString("Total_Item") + " items", item.getString("ID"), item.getString("TanggalRencanaKirim"), item.getString("IDVendor"), item.getString("Tanggal_Rencana_Terima2"), item.getString("TransactionType"), item.getString("NoDeliveryRequest")
                                            );
                                            list.add(itemEnts);
                                        }
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                } catch (JSONException e) {
                                    Toast.makeText(getActivity(), "CATCH ", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
            else if (date.equals("powithoutdr")) //fungsi ini menampilkan kontrak tanpa DR
            {
                controller.InqGeneralPagingFullEzra(getActivity().getApplicationContext(), "Get_Upcoming_PO_Orders_Mobile_Delivery_Request",
                        "@KodeProyek", "'" + session.getKodeProyek() + "'",
                        "@currentpage", pageup,
                        "@pagesize", pagedown,
                        "@sortby", "",
                        "@wherecond", "powithoutdrAND tdDR.TanggalRencanaKirim is null",
                        "@nik", session.isNikUserLoggedIn(),
                        "@formid", "PC.02.13",
                        "@zonaid", "",

                        new VolleyCallback() {
                            @Override
                            public void onSave(String output) {

                            }

                            @Override
                            public void onSuccess(JSONArray result) {
                                try {
                                    swipeRefreshLayout.setRefreshing(false);
                                    if (result.length() > 0) {
                                        for (int i = 0; i < result.length(); i++) {
                                            JSONObject item = result.getJSONObject(i);

                                            UpcomingListPoEnt itemEnts = new UpcomingListPoEnt(
                                                    item.getString("Nama_Vendor"), item.getString("Kode_PO"), item.getString("Total_Item") + " items", item.getString("ID"), item.getString("TanggalRencanaKirim"), item.getString("IDVendor"), item.getString("Tanggal_Rencana_Terima2"), item.getString("TransactionType"), item.getString("NoDeliveryRequest")
                                            );
                                            list.add(itemEnts);
                                        }
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                } catch (JSONException e) {
                                    Toast.makeText(getActivity(), "CATCH ", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
            else //fungsi ini menampilkan NPO
            {
                controller.InqGeneralPagingFullEzra(getActivity().getApplicationContext(), "Get_Upcoming_PO_Orders_Mobile_Delivery_Request",
                        "@KodeProyek", "'" + session.getKodeProyek() + "'",
                        "@currentpage", pageup,
                        "@pagesize", pagedown,
                        "@sortby", "",
                        "@wherecond", "",
                        "@nik", session.isNikUserLoggedIn(),
                        "@formid", "PC.02.13",
                        "@zonaid", "",

                        new VolleyCallback() {
                            @Override
                            public void onSave(String output) {

                            }

                            @Override
                            public void onSuccess(JSONArray result) {
                                try {
                                    swipeRefreshLayout.setRefreshing(false);
                                    if (result.length() > 0) {
                                        for (int i = 0; i < result.length(); i++) {
                                            JSONObject item = result.getJSONObject(i);

                                            UpcomingListPoEnt itemEnts = new UpcomingListPoEnt(
                                                    item.getString("Nama_Vendor"), item.getString("Kode_PO"), item.getString("Total_Item") + " items", item.getString("ID"), item.getString("TanggalRencanaKirim"), item.getString("IDVendor"), item.getString("Tanggal_Rencana_Terima2"), item.getString("TransactionType"), item.getString("NoDeliveryRequest")
                                            );
                                            list.add(itemEnts);
                                        }
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                } catch (JSONException e) {
                                    Toast.makeText(getActivity(), "CATCH ", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
            return list;

        }

        public ArrayList SetUpcomingDataSearch(String date, String kodeVendor){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getActivity());

            if(kodeVendor != null)
            {
                kodeVendor = " AND mv.ID = '"+kodeVendor+"' ";
            }
            else
            {
                kodeVendor = "";
            }

            //jika tanggal dipilih maka Execute SP yang original dengan date selain itu Exe SP yang Spesifik
            if(date.equals(""))
            {
                date = "searchallsection";
            }
            else
            {
                date = " AND CONVERT(VARCHAR,tdDR.TanggalRencanaKirim,105) = '"+date+"' ";
            }

            controller.InqGeneralPagingFullEzra(getActivity().getApplicationContext(),"Get_Upcoming_PO_Orders_Mobile_Delivery_Request",
                    "@KodeProyek","'" + session.getKodeProyek() + "'",
                    "@currentpage","1",
                    "@pagesize","10",
                    "@sortby","",
                    "@wherecond",date+kodeVendor,
                    "@nik",session.isNikUserLoggedIn(),
                    "@formid","PC.02.13",
                    "@zonaid","",

                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        UpcomingListPoEnt itemEnts = new UpcomingListPoEnt(
                                                item.getString("Nama_Vendor"),item.getString("Kode_PO"),item.getString("Total_Item") + " items",item.getString("ID"),item.getString("TanggalRencanaKirim"), item.getString("IDVendor"), item.getString("Tanggal_Rencana_Terima2"),item.getString("TransactionType"),item.getString("NoDeliveryRequest")
                                        );
                                        list.add(itemEnts);
                                        Log.d("LogFilterUpcoming", item.getString("Kode_PO").toString());
                                    }
                                }
                                sectionAdapter.notifyDataSetChanged();
                            }catch (JSONException e){
                                Toast.makeText(getActivity(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            return list;
        }

        public ArrayList SetUpcomingDataSearch2(String date, String param){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getActivity());

            if(param != null)
            {
                param = param;
            }
            else
            {
                param = "";
            }

            controller.InqGeneralPagingFullEzra(getActivity().getApplicationContext(),"Get_Upcoming_PO_Orders_Mobile_Delivery_Request",
                    "@KodeProyek","'" + session.getKodeProyek() + "'",
                    "@currentpage","1",
                    "@pagesize","10",
                    "@sortby",param,
                    "@wherecond"," AND CONVERT(VARCHAR,tdDR.TanggalRencanaKirim,105) = '"+date+"' ",
                    "@nik",session.isNikUserLoggedIn(),
                    "@formid","PC.02.13",
                    "@zonaid","",

                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        UpcomingListPoEnt itemEnts = new UpcomingListPoEnt(
                                                item.getString("Nama_Vendor"),item.getString("Kode_PO"),item.getString("Total_Item") + " items",item.getString("ID"),item.getString("TanggalRencanaKirim"), item.getString("IDVendor"), item.getString("Tanggal_Rencana_Terima2"),item.getString("TransactionType"),item.getString("NoDeliveryRequest")
                                        );
                                        list.add(itemEnts);
                                        Log.d("LogFilterUpcoming", item.getString("Kode_PO").toString());
                                    }
                                }
                            }catch (JSONException e){
                                Toast.makeText(getActivity(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            return list;
        }

        @Override
        public int getContentItemsTotal() {
            //return expanded? list.size() : 0;
            return list.size();
        }

        @Override
        public RecyclerView.ViewHolder getItemViewHolder(View view) {
            return new ItemViewHolder(view);
        }

        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
            final ItemViewHolder itemHolder = (ItemViewHolder) holder;
            /*
            String[] item = list.get(position).split("\\|");

            itemHolder.tvHeader.setText(item[0]);
            itemHolder.tvDate.setText(item[1]);
            itemHolder.imgItem.setImageResource(imgPlaceholderResId);
            */

            String namavendor = list.get(position).getId();
            String kodepo = list.get(position).getKode_po();
            String item = list.get(position).getItemname();
            String idpo = list.get(position).getId_transaksi();
            String idvendor = list.get(position).getIdvendor();
            String tvRencanaKirim = list.get(position).getTglrencanaterima();
            String tvTransactionType = list.get(position).getTransactiontype();
            String tvNoDr = list.get(position).getNodr();

            itemHolder.tvListItemName.setText(namavendor);
            itemHolder.tvListItemBrand.setText(kodepo);
            itemHolder.tvItem.setText(item);
            itemHolder.tvIdPo.setText(idpo);
            if(tvTransactionType.equals("Kontrak")) {
                Picasso.with(getContext())
                        .load(R.drawable.ic_spn_basedkontrak)
                        .placeholder(R.drawable.default_placeholder_small)
                        .error(R.drawable.default_placeholder_small)
                        .transform(new CircleTransform())
                        .into(itemHolder.imgItem);

                //itemHolder.imgItem.setImageResource(R.drawable.ic_spn_basedkontrak);
                itemHolder.imgItem.setBackgroundResource(R.drawable.btn_orange_selector);
            }
            else if(tvTransactionType.equals("PO")) {
                Picasso.with(getContext())
                        .load(R.drawable.ic_spn_basedpo)
                        .placeholder(R.drawable.default_placeholder_small)
                        .error(R.drawable.default_placeholder_small)
                        .transform(new CircleTransform())
                        .into(itemHolder.imgItem);

                //itemHolder.imgItem.setImageResource(R.drawable.ic_spn_basedpo);
                itemHolder.imgItem.setBackgroundResource(R.drawable.btn_blue_selector);
            }
            else
            {
                Picasso.with(getContext())
                        .load(R.drawable.ic_spn_basednpo)
                        .placeholder(R.drawable.default_placeholder_small)
                        .error(R.drawable.default_placeholder_small)
                        .transform(new CircleTransform())
                        .into(itemHolder.imgItem);

                //itemHolder.imgItem.setImageResource(R.drawable.ic_spn_basedpo);
                itemHolder.imgItem.setBackgroundResource(R.drawable.btn_green_selector);
            }
            itemHolder.tvIDVendor.setText(idvendor);
            itemHolder.tvRencanaKirim.setText(tvRencanaKirim);
            itemHolder.tvTransactionType.setText(tvTransactionType);

            itemHolder.imgItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // String.valueOf(v.getTag());
                    //Toast.makeText(getActivity(), itemHolder.imgItem.getResources().toString(), Toast.LENGTH_LONG).show();
                 }
            });


            itemHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView tvNamaVendor = (TextView) v.findViewById(R.id.tvNamaVendor);
                    TextView tvIdPo = (TextView) v.findViewById(R.id.tvIdPo);
                    TextView tvKodePo = (TextView) v.findViewById(R.id.tvKodeSPN);
                    TextView tvIDVendor = (TextView) v.findViewById(R.id.tvIDVendor);
                    TextView tvRencanaKirim = (TextView) v.findViewById(R.id.tvTglRencanaTerima);
                    TextView tvTransactionType = (TextView) v.findViewById(R.id.tvTransactionType);
                    TextView tvNoDr = (TextView) v.findViewById(R.id.tvNoDr);

                    session.setIdPO(tvIdPo.getText().toString());
                    session.setCodePO(tvKodePo.getText().toString());
                    session.setTglRencanaKirim(tvRencanaKirim.getText().toString());
                    session.setNamaVendor(tvNamaVendor.getText().toString());
                    session.setIDVendor(tvIDVendor.getText().toString());

                    Intent intent = new Intent(getActivity(), ListPOActivity.class);
                    intent.putExtra("idpo", tvIdPo.getText().toString());
                    intent.putExtra("kodepo", tvKodePo.getText().toString());
                    intent.putExtra("tgldipilih", tvRencanaKirim.getText().toString());
                    intent.putExtra("transactiontype", tvTransactionType.getText().toString());
                    intent.putExtra("nodr", tvNoDr.getText().toString());
                    intent.putExtra("idvendor", tvIDVendor.getText().toString());
                    intent.putExtra("namavendor", tvNamaVendor.getText().toString());
                    Log.d("LogTglDipilih", tvRencanaKirim.getText().toString());
                    startActivityForResult(intent, request_data_from);

                }
            });

        }

        @Override
        public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
            return new HeaderViewHolder(view);
        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;

            headerHolder.tvTitle.setText(title);

//            headerHolder.rootView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    expanded = !expanded;
//                    headerHolder.imgArrow.setImageResource(
//                            expanded ? R.drawable.ic_keyboard_arrow_up_black_18dp : R.drawable.ic_keyboard_arrow_down_black_18dp
//                    );
//                    sectionAdapter.notifyDataSetChanged();
//                }
//            });

        }

        @Override
        public RecyclerView.ViewHolder getFooterViewHolder(View view) {
            return new FooterViewHolder(view);
        }

        @Override
        public void onBindFooterViewHolder(RecyclerView.ViewHolder holder) {
            FooterViewHolder footerHolder = (FooterViewHolder) holder;

            footerHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("LogTittle",title);
                    if(title.equals(today)) {
                        sectionOneUp = 1+sectionOneUp;
                        list = SetUpcomingData2(0, String.valueOf(sectionOneUp), String.valueOf("10"));
                    }
                    else if(title.equals(tomorrow)) {
                        sectionTwoUp = 1+sectionTwoUp;
                        list = SetUpcomingData2(1, String.valueOf(sectionTwoUp), String.valueOf("10"));
                    }
                    else if(title.equals(tomorrow2)) {
                        sectionThreeUp = 1+sectionThreeUp;
                        list = SetUpcomingData2(2, String.valueOf(sectionThreeUp), String.valueOf("10"));
                    }
                    else if(title.equals("PO")) {
                        sectionFourUp = 1+sectionFourUp;
                        Log.d("LogPoPage", String.valueOf(sectionFourUp));
                        list = SetUpcomingData2(5, String.valueOf(sectionFourUp), String.valueOf("10"));
                    }
                    else if(title.equals("KONTRAK")) {
                        sectionFiveUp = 1+sectionFiveUp;
                        list = SetUpcomingData2(4, String.valueOf(sectionFiveUp), String.valueOf("10"));
                    }
                    else if(title.equals("NPO")) {
                        sectionSixUp = 1+sectionSixUp;
                        list = SetUpcomingData2(3, String.valueOf(sectionSixUp), String.valueOf("10"));
                    }

                    //Toast.makeText(getContext(), String.format("Clicked on footer of Section %s", title), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public ArrayList SetUpcomingDataInitial(int number){
        swipeRefreshLayout.setRefreshing(true);
        session = new SessionManager(getActivity());
        String date = null;
        //listItems2.clear();

        switch (number) {
            case 0:
                date = today;
                break;
            case 1:
                date = tomorrow;
                break;
            case 2:
                date = tomorrow2;
                break;
            case 3:
                date = "";
                break;
        }

        //JIKA Untuk PO dan Kontrak (date = "")
        if(date != "") {
            controller.InqGeneralPagingFullEzra(getActivity().getApplicationContext(), "Get_Upcoming_PO_Orders_Mobile_Delivery_Request",
                    "@KodeProyek", "'" + session.getKodeProyek() + "'",
                    "@currentpage", "1",
                    "@pagesize", "10",
                    "@sortby", "",
                    "@wherecond", " AND CONVERT(VARCHAR,tdDR.TanggalRencanaKirim,105) = '" + date + "' ",
                    "@nik", session.isNikUserLoggedIn(),
                    "@formid", "PC.02.13",
                    "@zonaid", "",

                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        UpcomingListPoEnt itemEnts = new UpcomingListPoEnt(
                                                item.getString("Nama_Vendor"), item.getString("Kode_PO"), item.getString("Total_Item") + " items", item.getString("ID"), item.getString("TanggalRencanaKirim"), item.getString("IDVendor"), item.getString("Tanggal_Rencana_Terima2"), item.getString("TransactionType"), item.getString("NoDeliveryRequest")
                                        );
                                        list.add(itemEnts);
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                }
                            } catch (JSONException e) {
                                Toast.makeText(getActivity(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
        else
        {
            controller.InqGeneralPagingFullEzra(getActivity().getApplicationContext(), "Get_Upcoming_PO_Orders_Mobile_Delivery_Request",
                    "@KodeProyek", "'" + session.getKodeProyek() + "'",
                    "@currentpage", "1",
                    "@pagesize", "10",
                    "@sortby", "",
                    "@wherecond", "",
                    "@nik", session.isNikUserLoggedIn(),
                    "@formid", "PC.02.13",
                    "@zonaid", "",

                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        UpcomingListPoEnt itemEnts = new UpcomingListPoEnt(
                                                item.getString("Nama_Vendor"), item.getString("Kode_PO"), item.getString("Total_Item") + " items", item.getString("ID"), item.getString("TanggalRencanaKirim"), item.getString("IDVendor"), item.getString("Tanggal_Rencana_Terima2"), item.getString("TransactionType"), item.getString("NoDeliveryRequest")
                                        );
                                        list.add(itemEnts);
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                }
                            } catch (JSONException e) {
                                Toast.makeText(getActivity(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
        return list;
    }

    public ArrayList SetUpcomingDataInitialSearch(String tanggal, String kodeVendor){
        session = new SessionManager(getActivity());
        //listItems2.clear();
        if(kodeVendor != null)
        {
            kodeVendor = " AND mv.ID = '"+kodeVendor+"' ";
        }
        else
        {
            kodeVendor = "";
        }

        controller.InqGeneralPagingFullEzra(getActivity().getApplicationContext(),"Get_Upcoming_PO_Orders_Mobile_Delivery_Request",
                "@KodeProyek","'"+session.getKodeProyek()+"'",
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond"," AND CONVERT(VARCHAR,tdDR.TanggalRencanaKirim,105) = '"+tanggal+"' "+kodeVendor,
                "@nik",session.isNikUserLoggedIn(),
                "@formid","PC.02.13",
                "@zonaid","",

                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);

                                    UpcomingListPoEnt itemEnts = new UpcomingListPoEnt(
                                            item.getString("Nama_Vendor"),item.getString("Kode_PO"),item.getString("Total_Item") + " items",item.getString("ID"),item.getString("TanggalRencanaKirim"), item.getString("IDVendor"), item.getString("Tanggal_Rencana_Terima2"),item.getString("TransactionType"),item.getString("NoDeliveryRequest")
                                    );
                                    list.add(itemEnts);
                                }
                                sectionAdapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){
                            Toast.makeText(getActivity(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        return list;
    }

    public ArrayList SetUpcomingDataInitialSearch2(String tanggal, String param){
        session = new SessionManager(getActivity());
        //listItems2.clear();
        if(param != null)
        {
            param = param;
        }
        else
        {
            param = "";
        }

        controller.InqGeneralPagingFullEzra(getActivity().getApplicationContext(),"Get_Upcoming_PO_Orders_Mobile_Delivery_Request",
                "@KodeProyek","'"+session.getKodeProyek()+"'",
                "@currentpage","1",
                "@pagesize","10",
                "@sortby",param,
                "@wherecond"," AND CONVERT(VARCHAR,tdDR.TanggalRencanaKirim,105) = '"+tanggal+"' ",
                "@nik",session.isNikUserLoggedIn(),
                "@formid","PC.02.13",
                "@zonaid","",

                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);

                                    UpcomingListPoEnt itemEnts = new UpcomingListPoEnt(
                                            item.getString("Nama_Vendor"),item.getString("Kode_PO"),item.getString("Total_Item") + " items",item.getString("ID"),item.getString("TanggalRencanaKirim"), item.getString("IDVendor"), item.getString("Tanggal_Rencana_Terima2"),item.getString("TransactionType"),item.getString("NoDeliveryRequest")
                                    );
                                    list.add(itemEnts);
                                }
                                sectionAdapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){
                            Toast.makeText(getActivity(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        return list;
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvTitle;
        private final View rootView;
        private final ImageView imgArrow;

        HeaderViewHolder(View view) {
            super(view);
            rootView = view;
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            imgArrow = (ImageView) view.findViewById(R.id.imgArrow);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {

        private final View rootView;

        FooterViewHolder(View view) {
            super(view);

            rootView = view;
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        private final View rootView;
        private final ImageView imgItem;
        private final TextView tvListItemName;
        private final TextView tvListItemBrand;
        private final TextView tvItem;
        private final TextView tvIdPo;
        private final TextView tvIDVendor;
        private final TextView tvRencanaKirim;
        private final TextView tvTransactionType;
        private final TextView tvNoDr;

        ItemViewHolder(View view) {
            super(view);

            rootView = view;
            imgItem = (ImageView) view.findViewById(R.id.imgItem);
            tvListItemName = (TextView) view.findViewById(R.id.tvNamaVendor);
            tvListItemBrand = (TextView) view.findViewById(R.id.tvKodeSPN);
            tvItem = (TextView) view.findViewById(R.id.tvQtyReceived);
            tvIdPo = (TextView) view.findViewById(R.id.tvIdPo);
            tvIDVendor = (TextView) view.findViewById(R.id.tvIDVendor);
            tvRencanaKirim = (TextView) view.findViewById(R.id.tvTglRencanaTerima);
            tvTransactionType = (TextView) view.findViewById(R.id.tvTransactionType);
            tvNoDr = (TextView) view.findViewById(R.id.tvNoDr);
        }
    }

    public static ArrayList<String> getDates(String dateString1, String dateString2)
    {
        Log.d("LogFilterUpcoming",dateString1);
        ArrayList<String> dates = new ArrayList<String>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = dateFormat.parse(dateString1);
            date2 = dateFormat.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while(!cal1.after(cal2))
        {
            dates.add(dateFormat.format(cal1.getTime()));
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }
}
