package com.totalbp.spnapps.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.totalbp.spnapps.MainActivity;
import com.totalbp.spnapps.R;
import com.totalbp.spnapps.activity.SPNDetail;
import com.totalbp.spnapps.adapter.AllSPNRecyclerViewAdapter;
import com.totalbp.spnapps.config.AppConfig;
import com.totalbp.spnapps.config.SessionManager;
import com.totalbp.spnapps.controller.AppController;
import com.totalbp.spnapps.interfaces.VolleyCallback;
import com.totalbp.spnapps.model.HeadItemsEntity;
import com.totalbp.spnapps.model.NewSPNEnt;
import com.totalbp.spnapps.utils.PicassoTransform.CircleTransform;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import static android.nfc.tech.MifareUltralight.PAGE_SIZE;


public class FragmentAllSPN extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AllSPNRecyclerViewAdapter.MessageAdapterListener, View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ArrayList<NewSPNEnt> listItems = new ArrayList<>();
    public ArrayList<HeadItemsEntity> listHeadItems = new ArrayList<>();
    private ListView listViewProject;
    JSONArray jsonListSPNDummy = null;
    JSONObject item;
    private SessionManager session;
    private AppController controller;
    RecyclerView recyclerView;
    String urlPost;
    JSONObject jsonObjectDesignPosts;
    JSONArray jsonArrayDesignContent;
    SwipeRefreshLayout swipeRefreshLayout;
    String[] designTitle, designExcerpt, designImage, designImageFull, designContent;
    int postNumber = 99;
    Boolean error = false;

    RecyclerView.Adapter recyclerViewAdapter;
    View view;
    int recyclerViewPaddingTop;
    List<String> list;
    String today, tomorrow, tomorrow2;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
    public AllSPNRecyclerViewAdapter adapter;
    public Integer currentPage = 1;
    public Integer pageSize = 10;
    public Integer searchActive = 0;

    private Dialog dialog;
    private Button dImgBtnClose;
    private ImageView dImgImage;
    private TextView dImgTittle;
    private static final int request_data_fromallsn  = 93;



    public FragmentAllSPN() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        controller = new AppController();
        session = new SessionManager(getActivity());

        Calendar calendar = new GregorianCalendar();
        today = dateFormat.format(calendar.getTime());

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.general_dialog_imagedisplay);
        dImgTittle = (TextView) dialog.findViewById(R.id.dImgTittle);
        dImgImage = (ImageView) dialog.findViewById(R.id.dImgImage);
        dImgBtnClose = (Button) dialog.findViewById(R.id.dImgBtnClose);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(session.getKodeProyek() != "")
        {


            view = inflater.inflate(R.layout.fragment_listspn, container, false);

            recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
            swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
            swipeRefreshLayout.setOnRefreshListener(this);
            adapter = new AllSPNRecyclerViewAdapter(getActivity().getApplicationContext(), listItems, this);

            final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setHasFixedSize(true);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(adapter);

            swipeRefreshLayout.post(
                    new Runnable() {
                        @Override
                        public void run() {
                                SetListView("", currentPage.toString(), pageSize.toString());
                        }
                    }
            );

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    //JIKA BUKAN HASIL SEARCH MAKA ADD SCROOL AKTIF
                    Log.d("LogSearchActive",String.valueOf(searchActive));
                    if(searchActive != 1){
                        int visibleItemCount = mLayoutManager.getChildCount();
                        int totalItemCount = mLayoutManager.getItemCount();
                        int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();
                        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                                && firstVisibleItemPosition >= 0
                                && totalItemCount >= PAGE_SIZE) {

                            currentPage = currentPage + 1;
                            SetListView("", currentPage.toString(), pageSize.toString());
                            Log.d("onScroll", currentPage.toString());
                        }
                    }
                    else
                    {

                    }
                }
            });



        }
        else
        {
            view = inflater.inflate(R.layout.fragment_project_blank, container, false);
            Toast.makeText(getActivity(), "Silahkan Pilih Proyek", Toast.LENGTH_LONG).show();
        }

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if( requestCode == request_data_fromallsn) {
            if (data != null){

                refreshDataFromActivity();
            }
        }

    }
    @Override
    public void onResume() {
        super.onResume();

//        listItems.clear();
//        adapter.notifyDataSetChanged();
//        currentPage = 1;
//        SetListView("", "1", "10");

    }

    public void updateDataFromActivity(String kodeSpn, String kodeVendor, String startDate, String endDate, String status, String type){
        //Toast.makeText(getActivity(),"updated from activity"+kodeSpn+kodeVendor+startDate+endDate+status,Toast.LENGTH_SHORT).show();
        searchActive = 1;
            SetListViewSearch(kodeSpn, kodeVendor, startDate, endDate, status, type);
    }

    public void refreshDataFromActivity(){
        listItems.clear();
        adapter.notifyDataSetChanged();
        currentPage = 1;
        SetListView("", "1", "10");
    }

    public void SetListView(String wherecond, String currentpage, String pagesize){
        swipeRefreshLayout.setRefreshing(true);
        session = new SessionManager(getActivity());
//        listItems.clear();
        String whereCond = "";
        if (!wherecond.equals("")){
            whereCond = wherecond;
        }
        controller.InqGeneralPagingFullEzra(getActivity().getApplicationContext(),"GetListSpnDetails",
//                " and a.Kode_Proyek = '"+session.getKodeProyek()+"' AND Nama_Vendor IS NOT NULL ","1",
//                "10", "",
//                session.isNikUserLoggedIn(), session.getKodeProyek(),
//                "000", "PC.02.13",
//                "", "",
                "@KodeProyek",session.getKodeProyek(),
                "@currentpage",currentpage,
                "@pagesize",pagesize,
                "@sortby","",
                "@wherecond"," and a.Kode_Proyek = '"+session.getKodeProyek()+"' AND Nama_Vendor IS NOT NULL AND Status_SPN = 1 ",
                "@nik",session.isNikUserLoggedIn(),
                "@formid","PC.02.13",
                "@zonaid","",


                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);

                                    NewSPNEnt itemEnts = new NewSPNEnt(
                                            item.getString("ID"), item.getString("Kode_SPN"),
                                            item.getString("No_Revisi"), item.getString("Kode_Proyek"),
                                            item.getString("Tanggal_SPN"), item.getString("Kode_Zona"),
                                            item.getString("Kode_Vendor"), item.getString("Surat_Jalan"),
                                            item.getString("Keterangan"), item.getString("Nama_Vendor"),
                                            item.getString("Status_Approval"),item.getString("PO_Count"),
                                            item.getString("Approval_Number"), item.getString("pathImgVendor")
                                    );

                                    listItems.add(itemEnts);
                                    //Toast.makeText(getApplicationContext(), "WOI "+item.toString(), Toast.LENGTH_LONG).show();
                                    Log.d("EZRA", item.toString());
                                }

                                adapter.notifyDataSetChanged();
                                swipeRefreshLayout.setRefreshing(false);
                            }
                            swipeRefreshLayout.setRefreshing(false);
                            adapter.notifyDataSetChanged();
                        }catch (JSONException e){
                            swipeRefreshLayout.setRefreshing(false);
                            Toast.makeText(getActivity(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void SetListViewSearch(String kodeSpn, String kodeVendor, String startDate, String endDate, String freetext, String type){
        swipeRefreshLayout.setRefreshing(true);
        session = new SessionManager(getActivity());
        listItems.clear();

        if(type == "filter") {
            if (kodeSpn != null) {
                kodeSpn = " AND a.Kode_SPN = '" + kodeSpn + "' ";
            } else {
                kodeSpn = "";
            }
            if (kodeVendor != null) {
                kodeVendor = " AND a.Kode_Vendor = " + kodeVendor;
            } else {
                kodeVendor = "";
            }
            if (startDate.length() > 0) {
                startDate = " AND Tanggal_SPN between '" + startDate + "' AND '" + endDate + "' ";
            } else {
                startDate = "";
            }
            if (freetext != null) {
                //status = " AND Status_Approval = "+status;
            } else {
                freetext = "";
            }

            controller.InqGeneralPagingFullEzra(getActivity().getApplicationContext(), "GetListSpnDetails",
                    "@KodeProyek", session.getKodeProyek(),
                    "@currentpage", "1",
                    "@pagesize", "100",
                    "@sortby", "",
                    "@wherecond", " and a.Kode_Proyek = '" + session.getKodeProyek() + "' AND Nama_Vendor IS NOT NULL AND Status_SPN = 1 " + kodeSpn + kodeVendor + startDate,
                    "@nik", session.isNikUserLoggedIn(),
                    "@formid", "PC.02.13",
                    "@approvalcond", freetext,


                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        NewSPNEnt itemEnts = new NewSPNEnt(
                                                item.getString("ID"), item.getString("Kode_SPN"),
                                                item.getString("No_Revisi"), item.getString("Kode_Proyek"),
                                                item.getString("Tanggal_SPN"), item.getString("Kode_Zona"),
                                                item.getString("Kode_Vendor"), item.getString("Surat_Jalan"),
                                                item.getString("Keterangan"), item.getString("Nama_Vendor"),
                                                item.getString("Status_Approval"), item.getString("PO_Count"),
                                                item.getString("Approval_Number"), item.getString("pathImgVendor")
                                        );

                                        listItems.add(itemEnts);
                                        //Toast.makeText(getApplicationContext(), "WOI "+item.toString(), Toast.LENGTH_LONG).show();
                                        //Log.d("EZRA", item.toString());
                                    }

                                    adapter.notifyDataSetChanged();
                                    swipeRefreshLayout.setRefreshing(false);
                                }
                                swipeRefreshLayout.setRefreshing(false);
                                adapter.notifyDataSetChanged();
                            } catch (JSONException e) {
                                swipeRefreshLayout.setRefreshing(false);
                                Toast.makeText(getActivity(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
        else
        {
            if(freetext.equals("asckodespn"))
            {
                freetext = " a.Kode_SPN ASC ";
            }
            else if(freetext.equals("desckodespn"))
            {
                freetext = " a.Kode_SPN DESC ";
            }
            else if(freetext.equals("asctglspn"))
            {
                freetext = " Tanggal_SPN ASC ";
            }
            else if(freetext.equals("desctglspn"))
            {
                freetext = " Tanggal_SPN DESC ";
            }
            else if(freetext.equals("ascnamavendor"))
            {
                freetext = " Nama_Vendor ASC ";
            }
            else if(freetext.equals("descnamavendor"))
            {
                freetext = " Nama_Vendor DESC ";
            }
            controller.InqGeneralPagingFullEzra(getActivity().getApplicationContext(), "GetListSpnDetails",
                    "@KodeProyek", session.getKodeProyek(),
                    "@currentpage", "1",
                    "@pagesize", "100",
                    "@sortby", freetext,
                    "@wherecond", " and a.Kode_Proyek = '" + session.getKodeProyek() + "' AND Nama_Vendor IS NOT NULL AND Status_SPN = 1 ",
                    "@nik", session.isNikUserLoggedIn(),
                    "@formid", "PC.02.13",
                    "@approvalcond", "",


                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        NewSPNEnt itemEnts = new NewSPNEnt(
                                                item.getString("ID"), item.getString("Kode_SPN"),
                                                item.getString("No_Revisi"), item.getString("Kode_Proyek"),
                                                item.getString("Tanggal_SPN"), item.getString("Kode_Zona"),
                                                item.getString("Kode_Vendor"), item.getString("Surat_Jalan"),
                                                item.getString("Keterangan"), item.getString("Nama_Vendor"),
                                                item.getString("Status_Approval"), item.getString("PO_Count"),
                                                item.getString("Approval_Number"), item.getString("pathImgVendor")
                                        );

                                        listItems.add(itemEnts);
                                        //Toast.makeText(getApplicationContext(), "WOI "+item.toString(), Toast.LENGTH_LONG).show();
                                        //Log.d("EZRA", item.toString());
                                    }

                                    adapter.notifyDataSetChanged();
                                    swipeRefreshLayout.setRefreshing(false);
                                }
                                swipeRefreshLayout.setRefreshing(false);
                                adapter.notifyDataSetChanged();
                            } catch (JSONException e) {
                                swipeRefreshLayout.setRefreshing(false);
                                Toast.makeText(getActivity(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
    }

    @Override
    public void onRefresh() {
        listItems.clear();
        adapter.notifyDataSetChanged();
        currentPage = 1;
        SetListView("", "1", "10");
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onIconClicked(int position) {

    }

    @Override
    public void onIconImportantClicked(int position) {

        if (adapter.getSelectedItemCount() > 0) {
        } else {
            // read the message which removes bold from the row
            NewSPNEnt message = listItems.get(position);
            //message.setRead(true);
            listItems.set(position, message);
            adapter.notifyDataSetChanged();

            dImgTittle.setText(message.getNama_vendor());

            Picasso.with(getContext())
                    .load(session.getUrlConfig()+AppConfig.URL_IMAGE_PREFIX+message.getPathimagevendor())
                    .placeholder(R.drawable.default_placeholder_small)
                    .error(R.drawable.default_placeholder_small)
                    .into(dImgImage);

            dImgBtnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            dialog.show();
            //Log.d("dadapaha", message.getKode_spn().toString());

        }
    }

    @Override
    public void onMessageRowClicked(int position) {
        if (adapter.getSelectedItemCount() > 0) {
        } else {
            // read the message which removes bold from the row
            NewSPNEnt message = listItems.get(position);
            //message.setRead(true);
            listItems.set(position, message);
            adapter.notifyDataSetChanged();

            //Log.d("dadapaha", message.getKode_spn().toString());
            Intent intent = new Intent(getActivity(), SPNDetail.class);
            intent.putExtra("kodespn", message.getKode_spn().toString());
            intent.putExtra("id", message.getId().toString());
            intent.putExtra("approvalnumber", message.getApproval_number().toString());
            intent.putExtra("statusapproval", message.getStatus_approval().toString());
            intent.putExtra("norevisi", message.getNo_revisi().toString());
            intent.putExtra("kodevendor", message.getKode_vendor().toString());
            intent.putExtra("namavendor", message.getNama_vendor().toString());
            startActivityForResult(intent, request_data_fromallsn);
        }
    }

    @Override
    public void onRowLongClicked(int position) {

    }

    /*
    public String loadJSONFromAsset() {
        String json = null;
        listItems.clear();
        try {
            InputStream is = getActivity().getAssets().open("list_items_spn.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        try {
            jsonListSPNDummy = new JSONArray(json);

            if (jsonListSPNDummy.length() > 0) {
                for (int i = 0; i < jsonListSPNDummy.length(); i++) {
                    item = jsonListSPNDummy.getJSONObject(i);
                    NewSPNEnt itemEnts = new NewSPNEnt(
                            item.getString("SpnId"), item.getString("SPNCode"),
                            item.getString("NamaVendor"), item.getString("CodePo"),
                            item.getString("Qty"),item.getString("Other"),
                            item.getString("Qty"),item.getString("Other"),
                            item.getString("Qty"),item.getString("Other"),
                            item.getString("Qty"),item.getString("Other")
                    );
                    listItems.add(itemEnts);

                    //Toast.makeText(getActivity(), "CATCH s"+item.toString(), Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(), "CATCH s"+jsonListSPNDummy.getJSONObject(i).toString()+" = "+i, Toast.LENGTH_LONG).show();
                }
                if (listItems.size() > 0 & listViewProject != null) {
                    adapter = new AllSPNAdapter(getActivity().getApplicationContext(),listItems);
                    listViewProject.setAdapter(adapter);
                    listViewProject.setTextFilterEnabled(false);
                }
                /*
                if (listItems.size() > 0 & listViewProject != null) {
                    adapter = new NewSPNAdapter(getActivity().getApplicationContext(), listItems);
                    listViewProject.setAdapter(adapter);
                    listViewProject.setTextFilterEnabled(false);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    } */


}
