package com.totalbp.spnapps.interfaces;

import com.totalbp.spnapps.model.PoEnt;

import java.util.ArrayList;

/**
 * Created by 140030 on 09/07/2017.
 */

public interface ItemLoadCompleteSPNCallback {
    void onComplete(ArrayList<PoEnt> result);
}
