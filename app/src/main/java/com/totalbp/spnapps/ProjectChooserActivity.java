package com.totalbp.spnapps;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.totalbp.spnapps.adapter.SortAdapter;
import com.totalbp.spnapps.controller.AppController;
import com.totalbp.spnapps.config.SessionManager;
import com.totalbp.spnapps.interfaces.VolleyCallback;
import com.totalbp.spnapps.model.ProjectEnt;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ProjectChooserActivity extends AppCompatActivity {
    SearchView searchView;
    public ArrayList<ProjectEnt> listItems = new ArrayList<>();
    public SortAdapter adapter;
    private ListView listViewProject;
    private AppController controller;
    private SessionManager session;
    ProgressDialog pDialog;
    ArrayList<HashMap<String, String>> projectList;
    private static final int send_data_to_project_items  = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_chooser);
        controller = new AppController();
        session = new SessionManager(getApplicationContext());

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        listViewProject = (ListView) findViewById(R.id.listViewProjectSearchOnType);
        SetListView("");
        listViewProject.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view.findViewById(R.id.tvNamaVendor);
                TextView textViewVal1 = (TextView) view.findViewById(R.id.tvKodeSPN);
                TextView textViewVal2 = (TextView) view.findViewById(R.id.tvKodeSPN);

                //Toast.makeText(getApplicationContext(), "Kode Proyek"+textView.getText().toString(), Toast.LENGTH_LONG).show();
                //Toast.makeText(getApplicationContext(), "Kode Proyek"+textViewVal2.getText().toString(), Toast.LENGTH_LONG).show();

                Bundle sendBundle = new Bundle();
                sendBundle.putString("projectname", textView.getText().toString());
                sendBundle.putString("kodeProyek",textViewVal2.getText().toString());
                Intent intent = new Intent(getApplicationContext(), MainActivity.class );
                intent.putExtras(sendBundle);
                setResult(send_data_to_project_items,intent);
                finish();
            }
        });

    }

    public void SetListView(String wherecond){

        listItems.clear();
        String whereCond = "";
        if (!wherecond.equals("")){
            whereCond = wherecond;
        }
        controller.InqGeneral(getApplicationContext(),"GetUserProyek", "NIK",
                session.isNikUserLoggedIn(),"wherecond",whereCond,"","", new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    ProjectEnt itemEnts = new ProjectEnt(
                                            item.getString("Nama_Proyek"),item.getString("Kd_Proyek"),"1"
                                            );
                                    listItems.add(itemEnts);
                                    //Toast.makeText(getApplicationContext(), "WOI "+item.toString(), Toast.LENGTH_LONG).show();
                                    //Log.d("EZRA", item.toString());
                                }

                                if (listItems.size() > 0 & listViewProject != null) {
                                    adapter = new SortAdapter(getApplicationContext(),listItems);
                                    listViewProject.setAdapter(adapter);
                                    listViewProject.setTextFilterEnabled(false);
                                }
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search_item, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        //*** setOnQueryTextFocusChangeListener ***
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchQuery) {
                adapter.filter(searchQuery.toString().trim());
                listViewProject.invalidate();
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        });
        return true;

        //untuk ganti menu itemnya harus pake inflater
        /*
        getMenuInflater().inflate(R.menu.search_item, menu);

        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        MenuItemCompat.expandActionView(searchMenuItem);
        searchView.setIconifiedByDefault(false);

        searchView.setQueryHint("type project name");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                SetListView(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    //listView.clearTextFilter();
                    SetListView("");
                } else {
                    SetListView(newText);
                    //listView.setFilterText(newText); supaya tidak ada popup text
                }
                return true;
            }
        });
        */
       // return super.onCreateOptionsMenu(menu);

    }



    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
