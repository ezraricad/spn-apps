package com.totalbp.spnapps;

import android.Manifest;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;
import com.totalbp.spnapps.activity.FilterAllSPNActivity;
import com.totalbp.spnapps.activity.FilterUpcomingActivity;
import com.totalbp.spnapps.activity.ListPOActivity;
import com.totalbp.spnapps.activity.SPNDetail;
import com.totalbp.spnapps.activity.SortingAllSPNActivity;
import com.totalbp.spnapps.activity.SortingUpcomingActivity;
import com.totalbp.spnapps.config.SessionManager;
import com.totalbp.spnapps.controller.AppController;
import com.totalbp.spnapps.fragments.FragmentAllSPN;
import com.totalbp.spnapps.fragments.FragmentUpcomingOrders;
import com.totalbp.spnapps.interfaces.VolleyCallback;
import com.totalbp.spnapps.model.IPPrinter;
import com.totalbp.spnapps.model.UserPrivilegeEnt;
import com.totalbp.spnapps.receiver.ConnectivityReceiver;
import com.totalbp.spnapps.utils.MProgressDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.paperdb.Paper;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission_group.CAMERA;
import static com.totalbp.spnapps.R.id.imageView;
import static com.totalbp.spnapps.config.AppConfig.urlProfileFromTBP;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ConnectivityReceiver.ConnectivityReceiverListener{

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Dialog dialog;
    private BottomNavigationView bottomNavigation;
    private View navHeader;
    private int mSelectedItem;
    private long lastBackPressTime = 0;
    private Toast toast;
    private TextView selectProject, projectSelected, tvUserNameSideBar, tvEmailSideBar;
    private static final int request_data_from_project_items  = 99;
    private static final int request_data_filter_upcoming_activity  = 98;
    private static final int request_data_filter_allspn_activity  = 97;
    private static final int request_data_sorting_allspn_activity  = 96;
    private static final int request_data_sorting_upcoming_activity = 95;

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE  = 99;
    private SessionManager session;
    public  Bundle extras;
    private FloatingActionButton fab,fab1,fab2;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;
    private Boolean isFabOpen = false;
    private TextView tvLabelNewMo, tvLabelFiler;
    private ImageView imgProfile;
    List<String> ipprinter =  new ArrayList<>();
    List<String> ipprinterSet =  new ArrayList<>();
    private Dialog dialogIpSetting;

    private TextView dITittle;
    private EditText dIComment;
    private Button dIBtnCancel, dIBtnSubmit;
    JSONObject item;
    private AppController controller;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Paper.init(getApplicationContext());
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        session = new SessionManager(getApplicationContext());
        controller = new AppController();

        ipprinter.clear();
        ipprinter.add("192.168.118.1");
        if(Paper.book().exist("printer")) {
            Log.d("LogTest", "exist");
        }
        else
        {
            Paper.book().write("printer", ipprinter);
            String tes = Paper.book().read("printer").toString();
            Log.d("LogTest", tes);
        }

        selectProject = (TextView) findViewById(R.id.select_project);
        projectSelected = (TextView) findViewById(R.id.project_selected);
        dialog = new Dialog(MainActivity.this);

        Intent intent = getIntent();
        extras = intent.getExtras();
        if(extras!=null){
            if(extras.getString("nik")!=null)
            {

                session.setKodeProyek(extras.getString("projectid"));
                session.setNamaProyek(extras.getString("projectname"));

                session.createLoginSession(true,extras.getString("nik"),extras.getString("email"), "role", extras.getString("token"), extras.getString("nama"));
                Log.d("login_session_mm","Kode Proyek : "+extras.getString("projectid")+", Nama Proyek " +extras.getString("projectname") + ", Nik :" + extras.getString("nik")+", Email : "+extras.getString("email")+ ", role"+ extras.getString("token")+ ", Nama : "+extras.getString("nama"));


//                session.setCanView(extras.getString("toapprove"));
//                session.setCanEdit(extras.getString("todelete"));
//                session.setCanInsert(extras.getString("toedit"));
//                session.setCanDelete(extras.getString("toinsert"));
//                session.setCanApprove(extras.getString("toview"));
                session.setUrlConfig(extras.getString("url"));

                CheckCanUserPreviledge();

                if(!session.getCanView().toString().equals("1"))
                {
                    Toast.makeText(getApplicationContext(),"You dont have Privilege to View!", Toast.LENGTH_SHORT).show();
                    this.finish();
                }


            }
//            Toast.makeText(getApplicationContext(),extras.getString("nik"),Toast.LENGTH_SHORT).show();
        }
        else{
            Intent inent = new Intent("com.totalbp.cis.main_action");
            PackageManager pm = getPackageManager();
            List<ResolveInfo> resolveInfos = pm.queryIntentActivities(inent, PackageManager.GET_RESOLVED_FILTER);
            if(resolveInfos.isEmpty()) {
                Toast.makeText(getApplicationContext(),"Application Not Installed", Toast.LENGTH_SHORT).show();
                Log.i("NoneResolved", "No Activities");
                new AlertDialog.Builder(this)
                        .setTitle("Warning")
                        .setMessage("Please using CIS launcher to use the apps")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                finish();
                            }}).show();
            } else {
                Log.i("Resolved!", "There are activities" + resolveInfos);
                startActivity(inent);
                this.finish();
            }
        }

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navHeader = navigationView.getHeaderView(0);

        tvUserNameSideBar = (TextView) navHeader.findViewById(R.id.tv_UsernameSideBar);
        tvEmailSideBar = (TextView) navHeader.findViewById(R.id.tv_EmailSideBar);
        imgProfile = (ImageView) navHeader.findViewById(R.id.imageView);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        bottomNavigation = (BottomNavigationView)findViewById(R.id.bottom_navigation);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (!session.getKodeProyek().equals("")) {
            //setupViewPager2(viewPager);
            projectSelected.setText(session.getNamaProyek());
        }
        //else {
            setupViewPager(viewPager);
        //}

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


        tvUserNameSideBar.setText(session.getUserName());
        tvEmailSideBar.setText(session.getUserEmail());


        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);

        tvLabelNewMo = (TextView) findViewById(R.id.tvLabelNewMo);
        tvLabelFiler = (TextView) findViewById(R.id.tvLabelFiler);

        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_backward);


        //Dialog untuk input text
        dialogIpSetting = new Dialog(MainActivity.this);
        dialogIpSetting.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogIpSetting.setContentView(R.layout.general_dialog_ipprinter);
        dialogIpSetting.setCanceledOnTouchOutside(false);
        dITittle = (TextView) dialogIpSetting.findViewById(R.id.dITittle);
        dIComment = (EditText) dialogIpSetting.findViewById(R.id.dIComment);
        dIBtnCancel = (Button) dialogIpSetting.findViewById(R.id.dIBtnCancel);
        dIBtnSubmit = (Button) dialogIpSetting.findViewById(R.id.dIBtnSubmit);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animateFAB();
            }
        });

        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tabLayout.getSelectedTabPosition() == 0)
                {
                    Intent intent = new Intent(MainActivity.this, FilterUpcomingActivity.class);
                    startActivityForResult(intent, request_data_filter_upcoming_activity);
                }
                else
                {
                    Intent intent = new Intent(MainActivity.this, FilterAllSPNActivity.class);
                    startActivityForResult(intent, request_data_filter_allspn_activity);
                }
            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tabLayout.getSelectedTabPosition() == 0)
                {
                    Intent intent = new Intent(MainActivity.this, SortingUpcomingActivity.class);
                    startActivityForResult(intent, request_data_sorting_upcoming_activity);
                }
                else
                {
                    Intent intent = new Intent(MainActivity.this, SortingAllSPNActivity.class);
                    startActivityForResult(intent, request_data_sorting_allspn_activity);
                }
            }
        });

        Glide.with(getApplicationContext()).load(session.getUrlConfig()+urlProfileFromTBP+session.getUserDetails().get("nik")+".jpg")
                .asBitmap()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(imgProfile) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        imgProfile.setImageDrawable(circularBitmapDrawable);
                    }
                });


        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectFragment(item);
                return true;
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.ACCESS_WIFI_STATE)
                    != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.CHANGE_WIFI_STATE)
                    != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE)
                    != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.CHANGE_NETWORK_STATE)
                    != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.WRITE_SETTINGS)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.ACCESS_WIFI_STATE,
                                Manifest.permission.CHANGE_WIFI_STATE,
                                Manifest.permission.ACCESS_NETWORK_STATE,
                                Manifest.permission.CHANGE_NETWORK_STATE,
                                Manifest.permission.WRITE_SETTINGS},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        }

        //checkConnection();
    }

    public void changeTab() {
        tabLayout.getTabAt(1).select();
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;

            setupViewPager(viewPager);

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;


        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.fab), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        CoordinatorLayout.LayoutParams params =(CoordinatorLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);
        snackbar.show();

    }

    public void animateFAB(){

        if(isFabOpen){

            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            tvLabelNewMo.startAnimation(fab_close);
            tvLabelFiler.startAnimation(fab_close);
            fab1.setClickable(false);
            //fab2.setClickable(false);
            isFabOpen = false;
            Log.d("Raj", "close");

        } else {

            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            tvLabelNewMo.startAnimation(fab_open);
            tvLabelFiler.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            isFabOpen = true;
            Log.d("Raj","open");

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        AppController.getInstance().setConnectivityListener(this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if( requestCode == request_data_from_project_items ) {
            if (data != null){
                projectSelected.setText(data.getExtras().getString("projectname"));
                session.setKodeProyek(data.getExtras().getString("kodeProyek"));
                session.setNamaProyek(data.getExtras().getString("projectname"));

               //Toast.makeText(getApplicationContext(), "Sampai sini Kode2 Proyek"+session.getKodeProyek(), Toast.LENGTH_LONG).show();

                setupViewPager(viewPager);
            }
        }
        else if(requestCode == request_data_filter_upcoming_activity){
            if (data != null){

                String kodeVendor  = data.getExtras().getString("kodeVendor");
                String dateRange  = data.getExtras().getString("dateRange");

                List<Fragment> allFragments = getSupportFragmentManager().getFragments();
                if (allFragments != null) {
                    for (Fragment fragment : allFragments) {
                        if(fragment.getClass() == FragmentUpcomingOrders.class) {
                            FragmentUpcomingOrders f1 = (FragmentUpcomingOrders) fragment;
                            f1.updateDataFromActivity("", kodeVendor, dateRange, "");
                        }
                    }
                }
            }
        }
        else if(requestCode == request_data_filter_allspn_activity){
            if (data != null){

                tabLayout.getTabAt(1).select();
                String kodeVendor = data.getExtras().getString("kodeVendor");
                String kodeSpn  = data.getExtras().getString("kodeSpn");
                String status  = data.getExtras().getString("status");
                String startDate  = data.getExtras().getString("startDate");
                String endDate  = data.getExtras().getString("endDate");

                //Toast.makeText(getApplicationContext(), "Sampai sini Kode Proyek"+kodeVendor+kodeSpn+status+startDate+endDate, Toast.LENGTH_LONG).show();
                List<Fragment> allFragments = getSupportFragmentManager().getFragments();
                if (allFragments != null) {
                    for (Fragment fragment : allFragments) {
                        if(fragment.getClass() == FragmentAllSPN.class) {
                            Log.d("EZRA", "FILTER");
                            FragmentAllSPN f1 = (FragmentAllSPN) fragment;
                            f1.updateDataFromActivity(kodeSpn, kodeVendor, startDate, endDate, status, "filter");
                        }
                    }
                }
            }
        }
        else if(requestCode == request_data_sorting_allspn_activity){
            if (data != null){

                tabLayout.getTabAt(1).select();
                String Value = data.getExtras().getString("Value");

                //Toast.makeText(getApplicationContext(), "Sampai sini Kode Proyek"+Value, Toast.LENGTH_LONG).show();
                List<Fragment> allFragments = getSupportFragmentManager().getFragments();
                if (allFragments != null) {
                    for (Fragment fragment : allFragments) {
                        if(fragment.getClass() == FragmentAllSPN.class) {
                            Log.d("EZRA", "SORT");
                            FragmentAllSPN f1 = (FragmentAllSPN) fragment;
                            f1.updateDataFromActivity("", "", "", "", Value, "sort");
                        }
                    }
                }
            }
        }
        else if(requestCode == request_data_sorting_upcoming_activity){
            if (data != null){

                String Value = data.getExtras().getString("Value");

                //Toast.makeText(getApplicationContext(), "Sampai sini Kode Proyek"+Value, Toast.LENGTH_LONG).show();
                List<Fragment> allFragments = getSupportFragmentManager().getFragments();
                if (allFragments != null) {
                    for (Fragment fragment : allFragments) {
                        if(fragment.getClass() == FragmentUpcomingOrders.class) {
                            Log.d("EZRA", "SORT");
                            FragmentUpcomingOrders f1 = (FragmentUpcomingOrders) fragment;
                            f1.updateDataFromActivitySort("", "", Value, "");
                        }
                    }
                }
            }
        }

    }

    public void CheckCanUserPreviledge(){

        Gson gsonHeader = new Gson();
        UserPrivilegeEnt userPrivilegeEnt = new UserPrivilegeEnt();
        userPrivilegeEnt.setUserID(session.isNikUserLoggedIn());
        userPrivilegeEnt.setKodeProyek(session.getKodeProyek());
        userPrivilegeEnt.setApp_ID("PC.2008.10");
        userPrivilegeEnt.setForm_ID("PC.02.13");

        String jsonString = gsonHeader.toJson(userPrivilegeEnt);
        Log.d("LogParamInput",jsonString.toString());

        controller.getUserPrivilege(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                //MProgressDialog.dismissProgressDialog();
                try {
                    Log.d("LogPreviledge", result.toString());
                    //swipeRefreshLayout.setRefreshing(false);
                    if (result.length() > 0) {

                        for (int i = 0; i < result.length(); i++) {

                            item = result.getJSONObject(i);

                            session.setCanView(item.getString("To_View"));
                            session.setCanEdit(item.getString("To_Edit"));
                            session.setCanInsert(item.getString("To_Insert"));
                            session.setCanDelete(item.getString("To_Delete"));
                            session.setCanApprove(item.getString("To_Approve"));

                            Log.d("LOG.USERPRIVILEGE","To Approve : "+item.getString("To_Approve")+ " To Delete :" +item.getString("To_Delete")+ " To Edit :"+ item.getString("To_Edit")+ " To Insert :" + item.getString("To_Insert")+ " To View : "+ item.getString("To_View"));

                        }


                        //Toast.makeText(getApplicationContext(), "Can view HSE : " + session.getviewhse() + " , Can view Quality : " + session.getviewquality() + " , Can Approve : " + session.getcanapprovencf(), Toast.LENGTH_LONG).show();
                        //adapterNotification.notifyDataSetChanged();
                        //swipeRefreshLayout.setRefreshing(false);
                    }
                }catch (JSONException e){
                    Toast.makeText(getApplicationContext(), "ERROR CheckCanViewQuality", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onSave(String output) {

            }
        });
    }

    public boolean checkPermission() {

        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), RECEIVE_SMS);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), READ_SMS);
        int ThirdPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int FourPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int FivePermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ThirdPermissionResult == PackageManager.PERMISSION_GRANTED &&
                FourPermissionResult == PackageManager.PERMISSION_GRANTED &&
                FivePermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getApplicationContext().getAssets().open("list_items.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void selectFragment(MenuItem item) {
        Fragment frag = null;
        // init corresponding fragment
        switch (item.getItemId()) {
            case R.id.action_filter:


                /*
                dialog.setContentView(R.layout.popup_sort);
                dialog.setTitle("Sort Options");
                dialog.setCancelable(true);
                // there are a lot of settings, for dialog, check them all out!
                // set up radiobutton
                RadioButton rd1 = (RadioButton) dialog.findViewById(R.id.rd1);
                RadioButton rd2 = (RadioButton) dialog.findViewById(R.id.rd2);

                Button submitSort = (Button) dialog.findViewById(R.id.btnSubmitSort);

                rd1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(), "Ascending", Toast.LENGTH_LONG).show();
                    }
                });

                rd2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(), "Descending", Toast.LENGTH_LONG).show();
                    }
                });

                submitSort.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Ok", Toast.LENGTH_LONG).show();
                    }
                });
                // now that the dialog is set up, it's time to show it
                dialog.show();
                */

                break;
            case R.id.action_sort:

                break;
        }
        // update selected item
        mSelectedItem = item.getItemId();

        // uncheck the other items.
        for (int i = 0; i< bottomNavigation.getMenu().size(); i++) {
            MenuItem menuItem = bottomNavigation.getMenu().getItem(i);
            menuItem.setChecked(menuItem.getItemId() == item.getItemId());
        }

//        updateToolbarText(item.getTitle());

        if (frag != null) {
            //FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            //ft.replace(R.id.fragmentContainer, frag, frag.getTag());
            //ft.commit();

        }
    }

    /*fungsi percobaan untuk membuat project blank di layout fragment
    private void selectFragmentCustom(String value) {
        // update the main content by replacing fragments
        if (session.getKodeProyek().equals("") || projectName.getText().equals(getString(R.string.select_project_hint))){
            value = "blank";
        }
        Fragment fragment = null;
        switch (value)
        {
            case "blank":
                fragment = new ProjectBlankFragment();
                break;

        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment).commit();

        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }
    */


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentUpcomingOrders(), "Upcoming Delivery");
        adapter.addFragment(new FragmentAllSPN(), "All SPN");

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public void rmFragment() {
            mFragmentList.clear();
            mFragmentTitleList.clear();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = this.getSupportFragmentManager();
//        getFragmentManager().popBackStack();
//        Toast.makeText(getApplicationContext(),"pressed",Toast.LENGTH_SHORT).show();
//        setDrawerState(true);
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//        FragmentManager mgr = getSupportFragmentManager();
//
//        if (mgr.getBackStackEntryCount() > 0)
//            mgr.popBackStack();
//        else
//            setDrawerState(true);
//            super.onBackPressed();
        if (isTaskRoot() && fm.getBackStackEntryCount()==0){
            if (this.lastBackPressTime < System.currentTimeMillis() - 4000) {
                toast = Toast.makeText(this, "Press back again to close this app", Toast.LENGTH_SHORT);
                toast.show();
                this.lastBackPressTime = System.currentTimeMillis();
            } else {
                if (toast != null) {
                    toast.cancel();
                }
                super.onBackPressed();
            }
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            dITittle.setText("Ip Address Printer");

            if(Paper.book().exist("printer")) {
                String ip1 = Paper.book().read("printer").toString();
                String ip2 = ip1.replace("[","");
                String ipfinal = ip2.replace("]","");
                Log.d("LogTest", ipfinal);
                dIComment.setText(ipfinal);
            }

            dIBtnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!dIComment.getText().toString().equals("")) {

                        String ip = dIComment.getText().toString();
                        ipprinterSet.clear();
                        ipprinterSet.add(ip);
                        if(Paper.book().exist("printer")) {
                            Log.d("LogTest", "exist");
                            Paper.book().delete("printer");
                            Paper.book().write("printer", ipprinterSet);
                            String tes2 = Paper.book().read("printer").toString();
                            Log.d("LogTest", tes2);
                        }
                        dialogIpSetting.dismiss();
                    }
                    else
                    {
                        Toast toast = Toast.makeText(MainActivity.this,"Ip Required!", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        dIComment.setText("");
                        dIComment.requestFocus();
                    }
                }
            });

            dIBtnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogIpSetting.dismiss();
                }
            });

            dialogIpSetting.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void logoutUser() {
        session.RemoveSession();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        //Fragment fragment = null;
        //Class fragmentClass = null;
//        if (id == R.id.nav_logout) {
//            logoutUser();
//            //fragmentClass = FragmentOne.class;
//        }
        /*
        else if (id == R.id.nav_gallery) {
            //session.logoutUser();

            //fragmentClass = FragmentTwo.class;
        } else if (id == R.id.nav_slideshow) {
            //fragmentClass = FragmentOne.class;
        } else if (id == R.id.nav_manage) {
            //fragmentClass = FragmentTwo.class;
        } else if (id == R.id.nav_share) {
            //fragmentClass = FragmentOne.class;
        } else if (id == R.id.nav_send) {
            //fragmentClass = FragmentTwo.class;
        }
        */
        //try {
//            fragment = (Fragment) fragmentClass.newInstance();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        /*nonaktifkan fragment layer dari nav menu
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        */
       // DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        //drawer.closeDrawer(GravityCompat.START);
        return false;
    }


}
