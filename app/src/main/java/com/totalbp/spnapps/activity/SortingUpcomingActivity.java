package com.totalbp.spnapps.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.totalbp.spnapps.MainActivity;
import com.totalbp.spnapps.R;
import com.totalbp.spnapps.controller.AppController;
import com.totalbp.spnapps.receiver.ConnectivityReceiver;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Ezra.R on 23/08/2017.
 */

public class SortingUpcomingActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener{
    private RadioGroup rgNamaVendor,rgTglDelivery,rgItemMaterial;
    private RadioButton rbNamaVendor1,rbNamaVendor2, rbTglDelivery1, rbTglDelivery2, rbItemMaterial1, rbItemMaterial2;
    public String Value = "";
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);

    private static final int request_data_sorting_upcoming_activity = 95;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sort_upcoming);

        rgNamaVendor = (RadioGroup)findViewById(R.id.rgNamaVendor);
        rgTglDelivery = (RadioGroup)findViewById(R.id.rgTglDelivery);
        rgItemMaterial = (RadioGroup)findViewById(R.id.rgItemMaterial);

        rbNamaVendor1 = (RadioButton)findViewById(R.id.rbNamaVendor1);
        rbNamaVendor2 = (RadioButton)findViewById(R.id.rbNamaVendor2);
        rbTglDelivery1 = (RadioButton)findViewById(R.id.rbTglDelivery1);
        rbTglDelivery2 = (RadioButton)findViewById(R.id.rbTglDelivery2);
        rbItemMaterial1 = (RadioButton)findViewById(R.id.rbItemMaterial1);
        rbItemMaterial2 = (RadioButton)findViewById(R.id.rbItemMaterial2);

        getSupportActionBar().setTitle("Upcoming Data Filter");

        rbNamaVendor1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Value = "ascnamavendor";

                Bundle sendBundle = new Bundle();

                sendBundle.putString("Value", Value);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtras(sendBundle);
                setResult(request_data_sorting_upcoming_activity, intent);
                finish();

            }
        });

        rbNamaVendor2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Value = "descnamavendor";

                Bundle sendBundle = new Bundle();

                sendBundle.putString("Value", Value);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtras(sendBundle);
                setResult(request_data_sorting_upcoming_activity, intent);
                finish();

            }
        });

        rbTglDelivery1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Value = "asctgldelivery";

                Bundle sendBundle = new Bundle();

                sendBundle.putString("Value", Value);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtras(sendBundle);
                setResult(request_data_sorting_upcoming_activity, intent);
                finish();

            }
        });

        rbTglDelivery2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Value = "desctgldelivery";

                Bundle sendBundle = new Bundle();

                sendBundle.putString("Value", Value);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtras(sendBundle);
                setResult(request_data_sorting_upcoming_activity, intent);
                finish();

            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        AppController.getInstance().setConnectivityListener(this);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;


        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.rbNamaVendor1), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        CoordinatorLayout.LayoutParams params =(CoordinatorLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);
        snackbar.show();

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
}
