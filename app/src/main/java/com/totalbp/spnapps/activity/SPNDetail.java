package com.totalbp.spnapps.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brother.ptouch.sdk.LabelInfo;
import com.brother.ptouch.sdk.Printer;
import com.brother.ptouch.sdk.PrinterInfo;
import com.brother.ptouch.sdk.PrinterStatus;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.squareup.picasso.Picasso;
import com.totalbp.spnapps.LoginActivity;
import com.totalbp.spnapps.MainActivity;
import com.totalbp.spnapps.R;
import com.totalbp.spnapps.config.AppConfig;
import com.totalbp.spnapps.config.SessionManager;
import com.totalbp.spnapps.controller.AppController;
import com.totalbp.spnapps.interfaces.VolleyCallback;
import com.totalbp.spnapps.model.AllItemsEnt;
import com.totalbp.spnapps.model.AllItemsSpnDetilEnt;
import com.totalbp.spnapps.model.ApprovalEnt;
import com.totalbp.spnapps.model.HeadItemsEntity;
import com.totalbp.spnapps.model.SPNDetil;
import com.totalbp.spnapps.model.SPNHeader;
import com.totalbp.spnapps.model.SPNInsertItemQtyReceived;
import com.totalbp.spnapps.model.SPNInsertItemQtyReceivedDetil;
import com.totalbp.spnapps.receiver.ConnectivityReceiver;
import com.totalbp.spnapps.utils.MProgressDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;
import io.paperdb.Paper;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;


public class SPNDetail extends AppCompatActivity  {
    SearchView searchView;
    private ListView listViewProject;
    private AppController controller;
    private SessionManager session;
    ArrayList<HashMap<String, String>> projectList;
    private static final int send_data_to_list_poitems  = 98;
    public ArrayList<AllItemsSpnDetilEnt> listItems = new ArrayList<>();

    public ArrayList<SPNDetil> listSPNDetil = new ArrayList<>();

    public ArrayList<HeadItemsEntity> listHeadItems = new ArrayList<>();
    RecyclerView.Adapter recyclerViewAdapter;
    RecyclerView recyclerView;
    JSONArray jsonListSPNDummy = null;
    JSONObject item;
    String urlPost;
    private Button btAddItemQtyAnotherPo, btnSubmit;
    private static final int request_data_from  = 90;
    String kodespn, id,approvalnumber, statusapproval, norevisi, kodevendor, namavendor, InitialQtyReceive;
    private SectionedRecyclerViewAdapter sectionAdapter;
    private Dialog dialog, dialog2, dialogInputText, dialogprint, dialogDeleteSPNDetil;

    private Button btnAnotherPo, btnSubmitSPN;
    private static final int request_data_from_listpoactivity2  = 94;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
   // private ArrayList<AllItemsSpnDetilEnt> listBeforePostSpnDetil;
    //private EditText edQtyReceive;
    public ArrayList<SPNInsertItemQtyReceivedDetil> listBeforePostSpnDetil = new ArrayList<>();
    Date currentTime = Calendar.getInstance().getTime();
    String today, idpo;
    public Bitmap b;
    private FloatingActionButton fbAddAnotherItem;

    private static final int request_data_fromallsn  = 93;

    private Handler mHandler = new Handler();
    //Dialog input declare

    private LinearLayout llPosting, llApproval, llSuratJalan, llAddItem, llRevisi;
    private TextView dInputTitle, tvQtyBeforeDialog, tvUnit1Dialog, tvUnit2Dialog, dITittle, lblMaxQty, dInputTitlePrint, dNamaVendor,  dNoPO, dNoDO, dTglPenerima, dPenerima, dSpnNumber, dContentSPNDel, dTittleSPNDel;
    private EditText edQtyReceive, etNoSuratJalan, etKeterangan, dIComment, etReason, etDNoKendaraan;
    private Button btnDialogInputCancel, btnDialogInputSubmit,buttonDialogDown,
            buttonDialogUp, btnApprove, btnReject,btnPosting, btnUpdate, dIBtnCancel, dIBtnSubmit, btnRevisi,  dBtnPrint, dBtnPrintClose, dBtnCancelSPNDel, dBtnSubmitSPNDel;
    private ImageView imageView2;
    List<String> listPo = new ArrayList<String>();
    List<String> listPoMark = new ArrayList<String>();
    List<String> listKodePo = new ArrayList<String>();
    public String m_Text = "";

    private ImageView ivContentDialogPrint, ivContentDialogPrint2, ivSample;
    private LinearLayout llDialogContent;
    ArrayList<Bitmap> bitmapTemp = new ArrayList<Bitmap>();
    WifiManager wifiManagerBase;
    WifiInfo wifiInfo;
    WifiConfiguration wifiConfig = new WifiConfiguration();
    int networkId1;
    public String ssid, password;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spn_detail);

        session = new SessionManager(getApplicationContext());
        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if(bd != null)
        {
            kodespn = (String) bd.get("kodespn");
            id = (String) bd.get("id");
            approvalnumber = (String) bd.get("approvalnumber");
            statusapproval = (String) bd.get("statusapproval");
            norevisi = (String) bd.get("norevisi");
            kodevendor = (String) bd.get("kodevendor");
            namavendor = (String) bd.get("namavendor");

            Log.d("HOHO","kodespn"+kodespn);
            Log.d("HOHO","id"+id);
            Log.d("HOHO","norevisi"+norevisi);
            Log.d("HOHO","approvalnumber"+approvalnumber);
            Log.d("HOHO","statusapproval"+statusapproval);

            if(!session.getCanEdit().toString().equals("1"))
            {
                Toast.makeText(getApplicationContext(),"You dont have Privilege to Edit!", Toast.LENGTH_SHORT).show();
                this.finish();
            }
        }
        //btnAnotherPo = (Button) findViewById(R.id.btnAnotherPo);
        //btnSubmitSPN = (Button) findViewById(R.id.btnSubmitSPN);
        etNoSuratJalan = (EditText) findViewById(R.id.etNoSuratJalan);
        etKeterangan = (EditText) findViewById(R.id.etKeterangan);
        etReason = (EditText) findViewById(R.id.etReason);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnApprove = (Button) findViewById(R.id.btnApprove);
        btnReject = (Button) findViewById(R.id.btnReject);
        btnPosting = (Button) findViewById(R.id.btnPosting);
        btnRevisi = (Button) findViewById(R.id.btnRevisi);
        llPosting = (LinearLayout) findViewById(R.id.llPosting);
        llApproval = (LinearLayout) findViewById(R.id.llApproval);
        llSuratJalan = (LinearLayout) findViewById(R.id.llSuratJalan);
        llAddItem = (LinearLayout) findViewById(R.id.llAddItem);
        llRevisi = (LinearLayout) findViewById(R.id.llRevisi);
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        fbAddAnotherItem = (FloatingActionButton) findViewById(R.id.fbAddAnotherItem);

        session = new SessionManager(getApplicationContext());

        ActionBar actionBar = getSupportActionBar();
        setTitle(namavendor);
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        actionBar.setSubtitle(kodespn);
        controller = new AppController();

        int timeInMills = new Random().nextInt((7000 - 3000) + 1) + 3000;
        //sectionAdapter.notifyDataSetChanged();
        Calendar calendar = new GregorianCalendar();
        today = dateFormat.format(calendar.getTime());

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        sectionAdapter = new SectionedRecyclerViewAdapter();
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                int failed = new Random().nextInt((3 - 1) + 1) + 1;
//
//                if (failed == 1) {
//                    //section.setState(Section.State.FAILED);
//                }
//                else {
                    SetPoDataInitialData(kodespn);
//                }
//
//                sectionAdapter.notifyDataSetChanged();
//            }
//        }, timeInMills);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(sectionAdapter);

        //Dialog untuk barcode
        dialog2 = new Dialog(SPNDetail.this);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(R.layout.suratjalan_dialog);

        TextView dTittle = (TextView) dialog2.findViewById(R.id.dTittleSrtJalan);

        EditText dEtSuratJalan = (EditText) dialog2.findViewById(R.id.etSuratJalan);
        EditText dEtKeterangan = (EditText) dialog2.findViewById(R.id.etKeterangan);
        Button dBtnCancelSrtJalan = (Button) dialog2.findViewById(R.id.dBtnCancelSrtJalan);
        Button dBtnSubmitSrtJalan = (Button) dialog2.findViewById(R.id.dBtnSubmitSrtJalan);

        //Dialog untuk input text
        dialogInputText = new Dialog(SPNDetail.this);
        dialogInputText.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogInputText.setContentView(R.layout.general_dialog_inputtext);
        dialogInputText.setCanceledOnTouchOutside(false);
        dITittle = (TextView) dialogInputText.findViewById(R.id.dITittle);
        dIComment = (EditText) dialogInputText.findViewById(R.id.dIComment);
        dIBtnCancel = (Button) dialogInputText.findViewById(R.id.dIBtnCancel);
        dIBtnSubmit = (Button) dialogInputText.findViewById(R.id.dIBtnSubmit);

        //Dialog untuk input
        dialog = new Dialog(SPNDetail.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.general_dialog);
        dialog.setCanceledOnTouchOutside(false);

        dInputTitle = (TextView) dialog.findViewById(R.id.dTittle);
        tvQtyBeforeDialog = (TextView) dialog.findViewById(R.id.dQtyBefore);
        edQtyReceive = (EditText) dialog.findViewById(R.id.dQtyReceive);
        etDNoKendaraan = (EditText) dialog.findViewById(R.id.etDNoKendaraan);
        tvUnit1Dialog = (TextView) dialog.findViewById(R.id.dUnit1);
        tvUnit2Dialog = (TextView) dialog.findViewById(R.id.dUnit2);
        btnDialogInputCancel = (Button) dialog.findViewById(R.id.dBtnCancel);
        btnDialogInputSubmit = (Button) dialog.findViewById(R.id.dBtnSubmit);
        buttonDialogDown = (Button) dialog.findViewById(R.id.buttonDialogDown);
        buttonDialogUp = (Button) dialog.findViewById(R.id.buttonDialogUp);
        lblMaxQty = (TextView) dialog.findViewById(R.id.lblMaxQty);


        //Dialog untuk print
        dialogprint = new Dialog(SPNDetail.this);
        dialogprint.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogprint.setContentView(R.layout.general_dialog_print);
        dialogprint.setCanceledOnTouchOutside(false);

        dInputTitlePrint = (TextView) dialogprint.findViewById(R.id.dPrintTittle);
        ivContentDialogPrint = (ImageView) dialogprint.findViewById(R.id.ivPrintContent);
        ivContentDialogPrint2 = (ImageView) dialogprint.findViewById(R.id.ivPrintContent2);
        ivSample = (ImageView) dialogprint.findViewById(R.id.ivSample);
        llDialogContent = (LinearLayout) dialogprint.findViewById(R.id.llDialogContent);
        dNamaVendor = (TextView) dialogprint.findViewById(R.id.dNamaVendor);
        dNoPO = (TextView) dialogprint.findViewById(R.id.dNoPo);
        dNoDO = (TextView) dialogprint.findViewById(R.id.dNoDo);
        dTglPenerima = (TextView) dialogprint.findViewById(R.id.dTglPenerima);
        dPenerima = (TextView) dialogprint.findViewById(R.id.dPenerima);
        dSpnNumber = (TextView) dialogprint.findViewById(R.id.dSpnNumber);
        dBtnPrint = (Button) dialogprint.findViewById(R.id.dBtnPrint);
        dBtnPrintClose = (Button) dialogprint.findViewById(R.id.dBtnPrintClose);

        //Dialog untuk delete spn detil
        dialogDeleteSPNDetil = new Dialog(SPNDetail.this);
        dialogDeleteSPNDetil.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDeleteSPNDetil.setContentView(R.layout.general_dialog_fix3);
        dialogDeleteSPNDetil.setCanceledOnTouchOutside(false);
        dTittleSPNDel = (TextView) dialogDeleteSPNDetil.findViewById(R.id.dTittleSPNDel);
        dContentSPNDel = (TextView) dialogDeleteSPNDetil.findViewById(R.id.dContentSPNDel);
        dBtnCancelSPNDel = (Button) dialogDeleteSPNDetil.findViewById(R.id.dBtnCancelSPNDel);
        dBtnSubmitSPNDel = (Button) dialogDeleteSPNDetil.findViewById(R.id.dBtnSubmitSPNDel);

        fbAddAnotherItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SPNDetail.this,ListPOActivity2.class);
                //listPo.add();
                //intent.putExtra("id", id);
                intent.putExtra("idpo", (listPo.toString()));
                intent.putExtra("activitytype", "spndetil");
                intent.putExtra("namavendor", namavendor);
                startActivityForResult(intent, request_data_from_listpoactivity2);
            }
        });

        //Tentukan semua button action yang tampil di screen
        CheckAction(statusapproval);

//        wifiManagerBase = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
//        networkId1 = wifiManagerBase.getConnectionInfo().getNetworkId();
//        wifiManagerBase.enableNetwork(networkId1, true);
//        wifiManagerBase.saveConfiguration();
//        Log.d("wifiManagerBase1", String.valueOf(networkId1));

//        wifiInfo = wifiManagerBase.getConnectionInfo();
//        ssid = wifiInfo.getSSID();
//        password = wifiInfo.getNetworkId();
//



//        btnSubmitSPN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                postSPNFinal(etNoSuratJalan.getText().toString(), etKeterangan.getText().toString());
//
//            }
//        });

        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dITittle.setText("Reason");

                dIBtnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!dIComment.getText().toString().equals("")) {
                            onApprove(kodespn, approvalnumber, "0", dIComment.getText().toString());
                        }
                        else
                        {
                            Toast toast = Toast.makeText(SPNDetail.this,"Reason Required!", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            dIComment.setText("");
                            dIComment.requestFocus();
                        }
                    }
                });

                dIBtnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogInputText.dismiss();
                    }
                });

                dialogInputText.show();

            }
        });

        btnApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onApprove(kodespn, approvalnumber, "1", "");
            }
        });

        //Bisa digunakan apablia transaksi belum di posting
        btnPosting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPosting(id, kodespn, etNoSuratJalan.getText().toString(), etKeterangan.getText().toString(), norevisi);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postSPNFinalUpdate(etNoSuratJalan.getText().toString(), etKeterangan.getText().toString(),id, norevisi);
                //postSPNFinalRevisi(etNoSuratJalan.getText().toString(), etKeterangan.getText().toString(),id, norevisi);
            }
        });

        btnRevisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //postSPNFinalUpdate(etNoSuratJalan.getText().toString(), etKeterangan.getText().toString(),id, norevisi);
                postSPNFinalRevisi(etNoSuratJalan.getText().toString(), etKeterangan.getText().toString(),id, norevisi);
            }
        });

    }

    public void CheckAction(String statusrequest)
    {
        //Jika Bukan Not Posted
        if(!approvalnumber.equals(""))
        {
            Log.d("LogCheckAction"," : "+approvalnumber);
              if(!approvalnumber.equals("null"))
            {
                if(statusrequest.equals("4"))
                {
                    llAddItem.setVisibility(View.VISIBLE);
                    isCanPosting(id, kodespn, "", norevisi);
                    //IsCanApprove(kodespn, approvalnumber, id);
                }
                else if(statusrequest.equals("1"))
                {
                    llAddItem.setVisibility(View.INVISIBLE);
                    //isCanPosting(id, kodespn, "");
                }
                else if(statusrequest.equals("2"))
                {
                    llAddItem.setVisibility(View.INVISIBLE);
                    //isCanPosting(id, kodespn, "");
                }
                else if(statusrequest.equals("9"))
                {
                    llAddItem.setVisibility(View.VISIBLE);
                    isCanPosting(id, kodespn, "revisiactive", norevisi);
                    etReason.setVisibility(View.VISIBLE);
                }
                else if(statusrequest.equals("3"))
                {
                    llAddItem.setVisibility(View.INVISIBLE);
                }
            }
            else
              {
                  llAddItem.setVisibility(View.VISIBLE);
                  isCanPosting(id, kodespn, "", norevisi);
              }
        }
        else //jika transaksi belum diposting maka show tombol add another item dan tanyakan apakah user bisa posting
        {
            llAddItem.setVisibility(View.VISIBLE);
            isCanPosting(id, kodespn, "", norevisi);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        //AppController.getInstance().setConnectivityListener(this);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;


        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.fab), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);
        snackbar.show();

    }

//    @Override
//    public void onNetworkConnectionChanged(boolean isConnected) {
//        showSnack(isConnected);
//    }


    private class NewsSection extends StatelessSection {

        final static int INDEX0 = 0;
        final static int INDEX1 = 1;

        final int topic;

        String title;

        ArrayList<AllItemsSpnDetilEnt> listItems = new ArrayList<>();
        int imgPlaceholderResId;
        //boolean expanded = true;

        NewsSection(int topic, String kodePo, String idtdpo, String kodespn, String transactiontype) {
            super(new SectionParameters.Builder(R.layout.fragment_list_item_po_row)
                    .headerResourceId(R.layout.fragment_list_item_po_row_header)
                    //.footerResourceId(R.layout.fragment_upcoming_orders_row_footer)
                    .build());

            this.topic = topic;

            switch (topic) {
                case INDEX0:
                    this.title = kodePo;
                    this.listItems = SetPoData(idtdpo, kodespn);
                    this.imgPlaceholderResId = R.drawable.ic_spn;
                    break;
                case INDEX1:
                    this.title = kodePo;
                    if(transactiontype.equals("PO"))
                    {
                        this.listItems = SetPoDataAdditional(idtdpo, kodespn);
                    }
                    else if(transactiontype.equals("Kontrak"))
                    {
                        this.listItems = SetKontrakDataAdditional(idtdpo, kodespn);
                    }
                    else if(transactiontype.equals("Kontrak PO")) {
                        this.listItems = SetPoDataAdditional(idtdpo, kodespn);
                    }
                    else {
                        this.listItems =  SetNPODataAdditional(idtdpo, kodespn);
                    }
                    this.imgPlaceholderResId = R.drawable.ic_spn;
                    break;
            }
        }

        private List<String> getNews(int arrayResource) {
            return new ArrayList<>(Arrays.asList(getResources().getStringArray(arrayResource)));
        }


        public ArrayList SetPoData(String idthpo, String kodespn){
            swipeRefreshLayout.setRefreshing(true);
            Log.d("TesSpn","here3 :"+idthpo);
            session = new SessionManager(getApplicationContext());
            String date = null;
            String sortBy = "";


            controller.InqGeneralPagingFullEzra(getApplicationContext(),"GetSPNDetailBasedPOMobile",
                    "@KodeProyek",session.getKodeProyek(),
                    "@currentpage","1",
                    "@pagesize","10",
                    "@sortby","",
                    "@wherecond"," AND th.ID = '"+idthpo+"' AND tdspn.Kode_SPN = '"+kodespn+"' ",
                    //"@wherecond","AND CONVERT(VARCHAR,tdDR.TanggalRencanaKirim,105) = '"+tglDipilih+"' AND th.ID = '"+number+"' ",
                   // "@wherecond"," AND th.ID = '"+number+"' ",
                    "@nik",session.isNikUserLoggedIn(),
                    "@formid","PC.02.13",
                    "@zonaid","",
                    new VolleyCallback() {
                @Override
                public void onSave(String output) {

                }

                @Override
                public void onSuccess(JSONArray result) {
                    try {
                        swipeRefreshLayout.setRefreshing(false);
                        if (result.length() > 0) {
                            for (int i = 0; i < result.length(); i++) {
                                JSONObject item = result.getJSONObject(i);

                                AllItemsSpnDetilEnt itemEnts = new AllItemsSpnDetilEnt(item.getString("path"),
                                       "ID",
                                        item.getString("Deskripsi"),
                                        item.getString("ordered_stock"),
                                        item.getString("received_stock"),
                                        item.getString("current_stock"),
                                        item.getString("unit"),
                                        item.getString("specification"),
                                        item.getString("KeteranganTdSpn"),
                                        item.getString("ID_Th_PO"),
                                        item.getString("Kode_Material"),
                                        item.getString("Kd_Anak_Kd_Material"),
                                        item.getString("keteranganitem"),
                                        item.getString("Unit1"),
                                        item.getString("Unit2"),
                                        item.getString("Volume1"),
                                        item.getString("Volume2"),
                                        item.getString("Harga_Satuan_Org1"),
                                        item.getString("Harga_Satuan_Org2"),
                                        item.getString("Harga_Satuan_Std1"),
                                        item.getString("Harga_Satuan_Std2"),
                                        item.getString("Panjang_Material"),
                                        item.getString("Koefisien_Konversi"),
                                        item.getString("DeskripsiMerk"),
                                        item.getString("qtyAlreadyReceived"),
                                        item.getString("VolumeRencanaKirim"),
                                        item.getString("IdTdPo"),
                                        item.getString("TanggalRencanaKirimFormatEnglish"),
                                        item.getString("SpnDetilID"),
                                        item.getString("Volume1Spn"),
                                        item.getString("NO_Revisi"),
                                        item.getString("TransactionType"),
                                        item.getString("Surat_Jalan"),
                                        item.getString("No_Kendaraan"),
                                        item.getString("KeteranganTdSpn"),
                                        item.getString("Kode_Lokasi"),
                                        item.getString("No_Urut"),
                                        item.getString("spesifikasi")

                                );

                                SPNInsertItemQtyReceivedDetil spnInsertItemQtyReceivedDetil = new SPNInsertItemQtyReceivedDetil();
                                spnInsertItemQtyReceivedDetil.setID(item.getString("SpnDetilID"));
                                spnInsertItemQtyReceivedDetil.setTipe_Transaksi( item.getString("TransactionType"));
                                spnInsertItemQtyReceivedDetil.setKode_Transaksi(item.getString("ID_Th_PO"));
                                spnInsertItemQtyReceivedDetil.setNo_Urut(item.getString("No_Urut"));
                                spnInsertItemQtyReceivedDetil.setKode_Aktivitas("0");
                                spnInsertItemQtyReceivedDetil.setKode_Material(item.getString("Kode_Material"));
                                spnInsertItemQtyReceivedDetil.setKd_Anak_Kd_Material(item.getString("Kd_Anak_Kd_Material"));
                                spnInsertItemQtyReceivedDetil.setKeterangan(item.getString("KeteranganTdSpn"));
                                spnInsertItemQtyReceivedDetil.setUnit1(item.getString("Unit1"));
                                spnInsertItemQtyReceivedDetil.setUnit2(item.getString("Unit2"));
                                spnInsertItemQtyReceivedDetil.setVolume1(item.getString("Volume1Spn"));
                                spnInsertItemQtyReceivedDetil.setVolume2(item.getString("Volume1Spn"));
                                spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org1(item.getString("Harga_Satuan_Org1"));
                                spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org2(item.getString("Harga_Satuan_Org2"));
                                spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std1(item.getString("Harga_Satuan_Std1"));
                                spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std2(item.getString("Harga_Satuan_Std2"));
                                spnInsertItemQtyReceivedDetil.setPanjang_Material(item.getString("Panjang_Material"));
                                spnInsertItemQtyReceivedDetil.setKoefisien_Konversi(item.getString("Koefisien_Konversi"));
                                spnInsertItemQtyReceivedDetil.setKode_Lokasi(item.getString("Kode_Lokasi"));
                                spnInsertItemQtyReceivedDetil.setSurat_Jalan(item.getString("Surat_Jalan"));
                                spnInsertItemQtyReceivedDetil.setNo_Kendaraan(item.getString("No_Kendaraan"));
                                spnInsertItemQtyReceivedDetil.setDeskripsi(item.getString("Deskripsi"));
                                spnInsertItemQtyReceivedDetil.setID_Transaksi_Detail(item.getString("IdTdPo"));
                                spnInsertItemQtyReceivedDetil.setIDTdSpn(item.getString("SpnDetilID"));
                                listBeforePostSpnDetil.add(spnInsertItemQtyReceivedDetil);
                                listItems.add(itemEnts);
                            }
                            Log.d("SPNDETIL", "ITEM : "+result.toString());
                            sectionAdapter.notifyDataSetChanged();
                        }
                    }catch (JSONException e){
                        Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                    }
                }
            });
            return listItems;
        }

        public ArrayList SetPoDataAdditional(String number, String tglDipilih){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());
            String date = null;
            String sortBy = "";


            controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedPo",
                    "@KodeProyek",session.getKodeProyek(),
                    "@currentpage","1",
                    "@pagesize","10",
                    "@sortby","",
                    "@wherecond"," AND th.ID = '"+number+"' ",
                    // "@wherecond"," AND th.ID = '"+number+"' ",
                    "@nik",session.isNikUserLoggedIn(),
                    "@formid","PC.02.13",
                    "@zonaid","",
                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        AllItemsSpnDetilEnt itemEnts = new AllItemsSpnDetilEnt(item.getString("path"),
                                                "ID",
                                                item.getString("Deskripsi"),
                                                item.getString("ordered_stock"),
                                                item.getString("received_stock"),
                                                item.getString("current_stock"),
                                                item.getString("unit"),
                                                item.getString("specification"),
                                                item.getString("Keterangan"),
                                                item.getString("ID_Th_PO"),
                                                item.getString("Kode_Material"),
                                                item.getString("Kd_Anak_Kd_Material"),
                                                item.getString("keteranganitem"),
                                                item.getString("Unit1"),
                                                item.getString("Unit2"),
                                                item.getString("Volume1"),
                                                item.getString("Volume2"),
                                                item.getString("Harga_Satuan_Org1"),
                                                item.getString("Harga_Satuan_Org2"),
                                                item.getString("Harga_Satuan_Std1"),
                                                item.getString("Harga_Satuan_Std2"),
                                                item.getString("Panjang_Material"),
                                                item.getString("Koefisien_Konversi"),
                                                item.getString("DeskripsiMerk"),
                                                item.getString("qtyAlreadyReceived"),
                                                item.getString("VolumeRencanaKirim"),
                                                item.getString("IdTdPo"),
                                                item.getString("TanggalRencanaKirimFormatEnglish"),
                                                item.getString("IdTdPo"),
                                                "0",
                                                "",
                                                "PO",
                                                "",
                                                "",
                                                "",
                                                "",
                                                "",
                                                item.getString("spesifikasi")

                                        );

                                        SPNInsertItemQtyReceivedDetil spnInsertItemQtyReceivedDetil = new SPNInsertItemQtyReceivedDetil();
                                        spnInsertItemQtyReceivedDetil.setID("0");
                                        spnInsertItemQtyReceivedDetil.setTipe_Transaksi("PO");
                                        spnInsertItemQtyReceivedDetil.setKode_Transaksi(item.getString("ID_Th_PO"));
                                        spnInsertItemQtyReceivedDetil.setNo_Urut("0");
                                        spnInsertItemQtyReceivedDetil.setKode_Aktivitas("0");
                                        spnInsertItemQtyReceivedDetil.setKode_Material(item.getString("Kode_Material"));
                                        spnInsertItemQtyReceivedDetil.setKd_Anak_Kd_Material(item.getString("Kd_Anak_Kd_Material"));
                                        spnInsertItemQtyReceivedDetil.setKeterangan(item.getString("keteranganitem"));
                                        spnInsertItemQtyReceivedDetil.setUnit1(item.getString("Unit1"));
                                        spnInsertItemQtyReceivedDetil.setUnit2(item.getString("Unit2"));
                                        spnInsertItemQtyReceivedDetil.setVolume1(item.getString("Volume1"));
                                        spnInsertItemQtyReceivedDetil.setVolume2(item.getString("Volume2"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org1(item.getString("Harga_Satuan_Org1"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org2(item.getString("Harga_Satuan_Org2"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std1(item.getString("Harga_Satuan_Std1"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std2(item.getString("Harga_Satuan_Std2"));
                                        spnInsertItemQtyReceivedDetil.setPanjang_Material(item.getString("Panjang_Material"));
                                        spnInsertItemQtyReceivedDetil.setKoefisien_Konversi(item.getString("Koefisien_Konversi"));
                                        spnInsertItemQtyReceivedDetil.setKode_Lokasi("");
                                        spnInsertItemQtyReceivedDetil.setSurat_Jalan("");
                                        spnInsertItemQtyReceivedDetil.setNo_Kendaraan("");
                                        spnInsertItemQtyReceivedDetil.setDeskripsi(item.getString("Deskripsi"));
                                        spnInsertItemQtyReceivedDetil.setID_Transaksi_Detail(item.getString("IdTdPo"));
                                        spnInsertItemQtyReceivedDetil.setIDTdSpn(item.getString("IdTdPo"));
                                        listBeforePostSpnDetil.add(spnInsertItemQtyReceivedDetil);
                                        listItems.add(itemEnts);
                                        Log.d("LogSpn","InitialData2BasedPo"+result.toString());
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                }
                            }catch (JSONException e){
                                Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            return listItems;
        }

        public ArrayList SetKontrakDataAdditional(String number, String tglDipilih){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());
            String date = null;
            String sortBy = "";


            controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedKontrak_Mobile",
                    "@KodeProyek",session.getKodeProyek(),
                    "@currentpage","1",
                    "@pagesize","10",
                    "@sortby","",
                    "@wherecond"," AND th.ID = '"+number+"' ",
                    // "@wherecond"," AND th.ID = '"+number+"' ",
                    "@nik",session.isNikUserLoggedIn(),
                    "@formid","PC.02.13",
                    "@zonaid","",
                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        AllItemsSpnDetilEnt itemEnts = new AllItemsSpnDetilEnt(item.getString("path"),
                                                "ID",
                                                item.getString("Deskripsi"),
                                                item.getString("ordered_stock"),
                                                item.getString("received_stock"),
                                                item.getString("current_stock"),
                                                item.getString("unit"),
                                                item.getString("specification"),
                                                item.getString("Keterangan"),
                                                item.getString("ID_Th_PO"),
                                                item.getString("Kode_Material"),
                                                item.getString("Kd_Anak_Kd_Material"),
                                                item.getString("keteranganitem"),
                                                item.getString("Unit1"),
                                                item.getString("Unit2"),
                                                item.getString("Volume1"),
                                                item.getString("Volume2"),
                                                item.getString("Harga_Satuan_Org1"),
                                                item.getString("Harga_Satuan_Org2"),
                                                item.getString("Harga_Satuan_Std1"),
                                                item.getString("Harga_Satuan_Std2"),
                                                item.getString("Panjang_Material"),
                                                item.getString("Koefisien_Konversi"),
                                                item.getString("DeskripsiMerk"),
                                                item.getString("qtyAlreadyReceived"),
                                                item.getString("VolumeRencanaKirim"),
                                                item.getString("IdTdPo"),
                                                item.getString("TanggalRencanaKirimFormatEnglish"),
                                                item.getString("IdTdPo"),
                                                "0",
                                                "",
                                                "Kontrak",
                                                "",
                                                "",
                                                "",
                                                "",
                                                "",
                                                item.getString("spesifikasi")

                                        );

                                        SPNInsertItemQtyReceivedDetil spnInsertItemQtyReceivedDetil = new SPNInsertItemQtyReceivedDetil();
                                        spnInsertItemQtyReceivedDetil.setID("0");
                                        spnInsertItemQtyReceivedDetil.setTipe_Transaksi("Kontrak");
                                        spnInsertItemQtyReceivedDetil.setKode_Transaksi(item.getString("ID_Th_PO"));
                                        spnInsertItemQtyReceivedDetil.setNo_Urut("0");
                                        spnInsertItemQtyReceivedDetil.setKode_Aktivitas("0");
                                        spnInsertItemQtyReceivedDetil.setKode_Material(item.getString("Kode_Material"));
                                        spnInsertItemQtyReceivedDetil.setKd_Anak_Kd_Material(item.getString("Kd_Anak_Kd_Material"));
                                        spnInsertItemQtyReceivedDetil.setKeterangan(item.getString("keteranganitem"));
                                        spnInsertItemQtyReceivedDetil.setUnit1(item.getString("Unit1"));
                                        spnInsertItemQtyReceivedDetil.setUnit2(item.getString("Unit2"));
                                        spnInsertItemQtyReceivedDetil.setVolume1(item.getString("Volume1"));
                                        spnInsertItemQtyReceivedDetil.setVolume2(item.getString("Volume2"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org1(item.getString("Harga_Satuan_Org1"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org2(item.getString("Harga_Satuan_Org2"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std1(item.getString("Harga_Satuan_Std1"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std2(item.getString("Harga_Satuan_Std2"));
                                        spnInsertItemQtyReceivedDetil.setPanjang_Material(item.getString("Panjang_Material"));
                                        spnInsertItemQtyReceivedDetil.setKoefisien_Konversi(item.getString("Koefisien_Konversi"));
                                        spnInsertItemQtyReceivedDetil.setKode_Lokasi("");
                                        spnInsertItemQtyReceivedDetil.setSurat_Jalan("");
                                        spnInsertItemQtyReceivedDetil.setNo_Kendaraan("");
                                        spnInsertItemQtyReceivedDetil.setDeskripsi(item.getString("Deskripsi"));
                                        spnInsertItemQtyReceivedDetil.setID_Transaksi_Detail(item.getString("IdTdPo"));
                                        spnInsertItemQtyReceivedDetil.setIDTdSpn(item.getString("IdTdPo"));
                                        listBeforePostSpnDetil.add(spnInsertItemQtyReceivedDetil);

                                        listItems.add(itemEnts);
                                        Log.d("LogSpn","InitialData2BasedKontrak"+result.toString());
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                }
                            }catch (JSONException e){
                                Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            return listItems;
        }

        public ArrayList SetKontrakDataNoDr(String number, String tglDipilih){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());
            String date = null;
            String sortBy = "";

            //JIKA TGL DIPILIH KOSONG ALIAS DARI ADD ANOTHER PO/LISTPOACTIVITY2 MAKA TDK ADA PARAM TANGGAL
            if(tglDipilih.equals(""))
            {
                tglDipilih = "";
            }
            else
            {
                tglDipilih = "AND CONVERT(VARCHAR,tdDR.TanggalRencanaKirim,105) = '"+tglDipilih+"' ";
            }
            controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedKontrakWithoutDr_Mobile",
                    "@KodeProyek",session.getKodeProyek(),
                    "@currentpage","1",
                    "@pagesize","10",
                    "@sortby","",
                    "@wherecond"," AND th.ID = '"+number+"' ",
                    // "@wherecond"," AND th.ID = '"+number+"' ",
                    "@nik",session.isNikUserLoggedIn(),
                    "@formid","PC.02.13",
                    "@zonaid","",
                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {

                                        JSONObject item = result.getJSONObject(i);

                                        AllItemsSpnDetilEnt itemEnts = new AllItemsSpnDetilEnt(item.getString("path"),
                                                "ID",
                                                item.getString("Deskripsi"),
                                                item.getString("ordered_stock"),
                                                item.getString("received_stock"),
                                                item.getString("current_stock"),
                                                item.getString("unit"),
                                                item.getString("specification"),
                                                item.getString("Keterangan"),
                                                item.getString("ID_Th_PO"),
                                                item.getString("Kode_Material"),
                                                item.getString("Kd_Anak_Kd_Material"),
                                                item.getString("keteranganitem"),
                                                item.getString("Unit1"),
                                                item.getString("Unit2"),
                                                item.getString("Volume1"),
                                                item.getString("Volume2"),
                                                item.getString("Harga_Satuan_Org1"),
                                                item.getString("Harga_Satuan_Org2"),
                                                item.getString("Harga_Satuan_Std1"),
                                                item.getString("Harga_Satuan_Std2"),
                                                item.getString("Panjang_Material"),
                                                item.getString("Koefisien_Konversi"),
                                                item.getString("DeskripsiMerk"),
                                                item.getString("qtyAlreadyReceived"),
                                                item.getString("VolumeRencanaKirim"),
                                                item.getString("IdTdPo"),
                                                item.getString("TanggalRencanaKirimFormatEnglish"),
                                                item.getString("IdTdPo"),
                                                "0",
                                                "",
                                                "NPO",
                                                "",
                                                "",
                                                "",
                                                "",
                                                "",
                                                item.getString("spesifikasi")

                                        );

                                        SPNInsertItemQtyReceivedDetil spnInsertItemQtyReceivedDetil = new SPNInsertItemQtyReceivedDetil();
                                        spnInsertItemQtyReceivedDetil.setID("0");
                                        spnInsertItemQtyReceivedDetil.setTipe_Transaksi("NPO");
                                        spnInsertItemQtyReceivedDetil.setKode_Transaksi(item.getString("ID_Th_PO"));
                                        spnInsertItemQtyReceivedDetil.setNo_Urut("0");
                                        spnInsertItemQtyReceivedDetil.setKode_Aktivitas("0");
                                        spnInsertItemQtyReceivedDetil.setKode_Material(item.getString("Kode_Material"));
                                        spnInsertItemQtyReceivedDetil.setKd_Anak_Kd_Material(item.getString("Kd_Anak_Kd_Material"));
                                        spnInsertItemQtyReceivedDetil.setKeterangan(item.getString("keteranganitem"));
                                        spnInsertItemQtyReceivedDetil.setUnit1(item.getString("Unit1"));
                                        spnInsertItemQtyReceivedDetil.setUnit2(item.getString("Unit2"));
                                        spnInsertItemQtyReceivedDetil.setVolume1(item.getString("Volume1"));
                                        spnInsertItemQtyReceivedDetil.setVolume2(item.getString("Volume2"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org1(item.getString("Harga_Satuan_Org1"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org2(item.getString("Harga_Satuan_Org2"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std1(item.getString("Harga_Satuan_Std1"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std2(item.getString("Harga_Satuan_Std2"));
                                        spnInsertItemQtyReceivedDetil.setPanjang_Material(item.getString("Panjang_Material"));
                                        spnInsertItemQtyReceivedDetil.setKoefisien_Konversi(item.getString("Koefisien_Konversi"));
                                        spnInsertItemQtyReceivedDetil.setKode_Lokasi("");
                                        spnInsertItemQtyReceivedDetil.setSurat_Jalan("");
                                        spnInsertItemQtyReceivedDetil.setNo_Kendaraan("");
                                        spnInsertItemQtyReceivedDetil.setDeskripsi(item.getString("Deskripsi"));
                                        spnInsertItemQtyReceivedDetil.setID_Transaksi_Detail(item.getString("IdTdPo"));
                                        spnInsertItemQtyReceivedDetil.setIDTdSpn(item.getString("IdTdPo"));
                                        listBeforePostSpnDetil.add(spnInsertItemQtyReceivedDetil);

                                        listItems.add(itemEnts);
                                        Log.d("LogSpn","InitialData2BasedKontrak"+result.toString());

                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                }
                            }catch (JSONException e){
                                Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            return listItems;
        }

        public ArrayList SetNPODataAdditional(String number, String tglDipilih){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());
            String date = null;
            String sortBy = "";


            controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedNpo_Mobile",
                    "@KodeProyek",session.getKodeProyek(),
                    "@currentpage","1",
                    "@pagesize","10",
                    "@sortby","",
                    "@wherecond"," AND th.ID = '"+number+"' ",
                    // "@wherecond"," AND th.ID = '"+number+"' ",
                    "@nik",session.isNikUserLoggedIn(),
                    "@formid","PC.02.13",
                    "@zonaid","",
                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        AllItemsSpnDetilEnt itemEnts = new AllItemsSpnDetilEnt(item.getString("path"),
                                                "ID",
                                                item.getString("Deskripsi"),
                                                item.getString("ordered_stock"),
                                                item.getString("received_stock"),
                                                item.getString("current_stock"),
                                                item.getString("unit"),
                                                item.getString("specification"),
                                                item.getString("Keterangan"),
                                                item.getString("ID_Th_PO"),
                                                item.getString("Kode_Material"),
                                                item.getString("Kd_Anak_Kd_Material"),
                                                item.getString("keteranganitem"),
                                                item.getString("Unit1"),
                                                item.getString("Unit2"),
                                                item.getString("Volume1"),
                                                item.getString("Volume2"),
                                                item.getString("Harga_Satuan_Org1"),
                                                item.getString("Harga_Satuan_Org2"),
                                                item.getString("Harga_Satuan_Std1"),
                                                item.getString("Harga_Satuan_Std2"),
                                                item.getString("Panjang_Material"),
                                                item.getString("Koefisien_Konversi"),
                                                item.getString("DeskripsiMerk"),
                                                item.getString("qtyAlreadyReceived"),
                                                item.getString("VolumeRencanaKirim"),
                                                item.getString("IdTdPo"),
                                                item.getString("TanggalRencanaKirimFormatEnglish"),
                                                item.getString("IdTdPo"),
                                                "0",
                                                "",
                                                "NPO",
                                                "",
                                                "",
                                                "",
                                                "",
                                                "",
                                                item.getString("spesifikasi")

                                        );

                                        SPNInsertItemQtyReceivedDetil spnInsertItemQtyReceivedDetil = new SPNInsertItemQtyReceivedDetil();
                                        spnInsertItemQtyReceivedDetil.setID("0");
                                        spnInsertItemQtyReceivedDetil.setTipe_Transaksi("NPO");
                                        spnInsertItemQtyReceivedDetil.setKode_Transaksi(item.getString("ID_Th_PO"));
                                        spnInsertItemQtyReceivedDetil.setNo_Urut("0");
                                        spnInsertItemQtyReceivedDetil.setKode_Aktivitas("0");
                                        spnInsertItemQtyReceivedDetil.setKode_Material(item.getString("Kode_Material"));
                                        spnInsertItemQtyReceivedDetil.setKd_Anak_Kd_Material(item.getString("Kd_Anak_Kd_Material"));
                                        spnInsertItemQtyReceivedDetil.setKeterangan(item.getString("keteranganitem"));
                                        spnInsertItemQtyReceivedDetil.setUnit1(item.getString("Unit1"));
                                        spnInsertItemQtyReceivedDetil.setUnit2(item.getString("Unit2"));
                                        spnInsertItemQtyReceivedDetil.setVolume1(item.getString("Volume1"));
                                        spnInsertItemQtyReceivedDetil.setVolume2(item.getString("Volume2"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org1(item.getString("Harga_Satuan_Org1"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org2(item.getString("Harga_Satuan_Org2"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std1(item.getString("Harga_Satuan_Std1"));
                                        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std2(item.getString("Harga_Satuan_Std2"));
                                        spnInsertItemQtyReceivedDetil.setPanjang_Material(item.getString("Panjang_Material"));
                                        spnInsertItemQtyReceivedDetil.setKoefisien_Konversi(item.getString("Koefisien_Konversi"));
                                        spnInsertItemQtyReceivedDetil.setKode_Lokasi("");
                                        spnInsertItemQtyReceivedDetil.setSurat_Jalan("");
                                        spnInsertItemQtyReceivedDetil.setNo_Kendaraan("");
                                        spnInsertItemQtyReceivedDetil.setDeskripsi(item.getString("Deskripsi"));
                                        spnInsertItemQtyReceivedDetil.setID_Transaksi_Detail(item.getString("IdTdPo"));
                                        spnInsertItemQtyReceivedDetil.setIDTdSpn(item.getString("IdTdPo"));
                                        listBeforePostSpnDetil.add(spnInsertItemQtyReceivedDetil);

                                        listItems.add(itemEnts);
                                        Log.d("LogSpn","InitialData2BasedKontrak"+result.toString());
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                }
                            }catch (JSONException e){
                                Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            return listItems;
        }

        @Override
        public int getContentItemsTotal() {
            //return expanded? listItems.size() : 0;
            return listItems.size();
        }

        @Override
        public RecyclerView.ViewHolder getItemViewHolder(View view) {
            return new ItemViewHolder(view);
        }

        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
            final ItemViewHolder itemHolder = (ItemViewHolder) holder;

            //dialog = new Dialog(ListPOActivity.this);

            itemHolder.tvListItemName.setText(listItems.get(position).getDescription()+" ("+listItems.get(position).getSpesifikasi()+")");
            itemHolder.tvListItemBrand.setText(listItems.get(position).getMerk());
            itemHolder.tvListQtyPo.setText(listItems.get(position).getVolume1());
            itemHolder.tvListQtyReceived.setText(listItems.get(position).getCurrent_stock());
            itemHolder.tvQtyRcvNotRejected.setText(listItems.get(position).getCurrent_stock());
            itemHolder.txtNoPlat.setText(listItems.get(position).getNo_Kendaraan());

            itemHolder.txtKodeSPN.setText(kodespn);
            itemHolder.txtNoRevisi.setText(listItems.get(position).getNO_Revisi());
            itemHolder.txtNoUrut.setText(listItems.get(position).getNo_Urut());
            itemHolder.tvQtyDR.setText(listItems.get(position).getVolumeRencanaKirim());
            if(listItems.get(position).getVolumeRencanaKirim().toString().equals(""))
            {
                itemHolder.tvQtyDR.setVisibility(View.GONE);
                itemHolder.tvLabelRcnTerima.setVisibility(View.GONE);
            }



            Picasso.with(getApplicationContext())
                    .load(session.getUrlConfig()+AppConfig.URL_IMAGE_PREFIX+listItems.get(position).getImageUrlPath())
                    .placeholder(R.drawable.ic_insert_photo_black_48dp)
                    .error(R.drawable.ic_insert_photo_black_48dp)
                    .into(itemHolder.imgItem);

            //Jika image url kosong maka gambar default menjadi gray
            if(listItems.get(position).getImageUrlPath().equals("null")) {
                itemHolder.imgItem.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
            }

            itemHolder.tvListUnit.setText(listItems.get(position).getUnit1());
            itemHolder.tvUnit2.setText(listItems.get(position).getUnit2());
            itemHolder.tvListQtyInput.setText(listItems.get(position).getVolume1Spn());
            InitialQtyReceive = listItems.get(position).getVolume1Spn();

            Log.d("LogSPNDetilId",listItems.get(position).getSpnDetilID());

            //if(position == 0){
//                itemHolder.llRowPo.setBackgroundColor(Color.parseColor("#20000000"));
//            }

            itemHolder.imgItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(getApplicationContext(), itemHolder.imgItem.getResources().toString(), Toast.LENGTH_LONG).show();
                }
            });

            itemHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dInputTitle.setText(listItems.get(position).getDescription()+" ("+listItems.get(position).getSpesifikasi()+")");
                    tvUnit1Dialog.setText(itemHolder.tvUnit2.getText().toString());
                    tvUnit2Dialog.setText(itemHolder.tvListUnit.getText().toString());
                    double qtyMax = Double.parseDouble(itemHolder.tvListQtyPo.getText().toString()) - Double.parseDouble(itemHolder.tvQtyRcvNotRejected.getText().toString());
                    //tvQtyBeforeDialog.setText(String.valueOf(qtyMax));
                    tvQtyBeforeDialog.setText(itemHolder.tvListQtyPo.getText().toString());
                    edQtyReceive.setText(itemHolder.tvListQtyInput.getText());
                    etDNoKendaraan.setText(itemHolder.txtNoPlat.getText());
                    lblMaxQty.setText("PO Qty");

                    double count1 = Double.parseDouble(itemHolder.tvListQtyPo.getText().toString()) - qtyMax - Double.parseDouble(InitialQtyReceive);
                    Log.d("LogSPNDetil1po",itemHolder.tvListQtyPo.getText().toString());
                    Log.d("LogSPNDetil1max",String.valueOf(qtyMax));
                    Log.d("LogSPNDetil1QtyRec",itemHolder.tvListQtyInput.getText().toString());

                    buttonDialogUp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(!edQtyReceive.getText().toString().equals(""))
                            {
                                if(edQtyReceive.getText().toString().equals(tvQtyBeforeDialog.getText().toString()))
                                {
                                    edQtyReceive.setText(tvQtyBeforeDialog.getText().toString());
                                }
                                else {
                                    float value = Float.parseFloat(edQtyReceive.getText().toString());
                                    float resultvalue = value + 1;
                                    edQtyReceive.setText(String.valueOf(resultvalue));
                                }
                            }
                            else
                            {
                                edQtyReceive.setText("0");
                            }
                        }
                    });

                    buttonDialogDown.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(!edQtyReceive.getText().toString().equals(""))
                            {
                                if(edQtyReceive.getText().toString().equals("1.0"))
                                {
                                    edQtyReceive.setText("1.0");
                                }
                                else
                                {
                                    float value = Float.parseFloat(edQtyReceive.getText().toString());
                                    float resultvalue = value-1;
                                    edQtyReceive.setText(String.valueOf(resultvalue));
                                }
                            }
                            else
                            {
                                edQtyReceive.setText("0");
                            }
                        }
                    });

                    btnDialogInputCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    btnDialogInputSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //JIKA QTY RECEIVED ADALAH ANGKA
                            if(edQtyReceive.getText().toString().matches("\\d+(?:\\.\\d+)?") && !edQtyReceive.getText().toString().equals("0.0") && !edQtyReceive.getText().toString().equals("0"))
                            {

                                if(!etDNoKendaraan.getText().toString().equals(""))
                                {
                                    double qtyReceived = Double.parseDouble(edQtyReceive.getText().toString());
                                    Log.d("LogSPNDetil1qtyrec",String.valueOf(qtyReceived));
                                    Log.d("LogSPNDetil1count1",String.valueOf(count1));

                                    //PO - Received terakhir =  limit edit
                                    double limitInput = Double.parseDouble(itemHolder.tvListQtyPo.getText().toString()) - count1;
                                    //QTY Receive tidak boleh lebih besar dengan qty received sblmnya + yg mau di received
                                    if( (qtyReceived+count1) <= Double.parseDouble(itemHolder.tvListQtyPo.getText().toString()))
                                    {
                                        for(SPNInsertItemQtyReceivedDetil spndetil:listBeforePostSpnDetil) {
                                            if(spndetil.getIDTdSpn().equals(listItems.get(position).getSpnDetilID())) {
                                                Log.d("LogSPNDetilId","Match");
                                                spndetil.setVolume1(edQtyReceive.getText().toString());
                                                spndetil.setVolume2(edQtyReceive.getText().toString());
                                                spndetil.setNo_Kendaraan(etDNoKendaraan.getText().toString());
        //                                        spndetil.setName("ChangedJuned");
        //                                        spndetil.setAddress("Hola-lulu");
                                            }
                                        }

                                        for(SPNInsertItemQtyReceivedDetil spndetil:listBeforePostSpnDetil) {
                                            //System.out.println(spndetil);
                                            Log.d("LogSPNDetilId",spndetil.getVolume1());
                                        }

                                        itemHolder.llRowPo.setBackgroundColor(Color.parseColor("#605f87c7"));
                                    //preparePostSPN();
                                        Gson gsonDetail = new Gson();
                                        String jsonString = gsonDetail.toJson(listBeforePostSpnDetil);
                                        session.setQtyReceivedArrayListSPN(jsonString.toString());
                                    //Toast.makeText(SPNDetail.this, "Qty Input Success!"+ jsonString.toString(), Toast.LENGTH_LONG).show();
                                        dialog.dismiss();
                                        itemHolder.tvListQtyInput.setText(edQtyReceive.getText().toString());
                                        itemHolder.txtNoPlat.setText(etDNoKendaraan.getText().toString());
                                    }
                                    else
                                    {
                                        Toast.makeText(SPNDetail.this, "Qty Do Receive must <= Qty Received Limit("+limitInput+")", Toast.LENGTH_LONG).show();
                                        edQtyReceive.requestFocus();
                                    }
                                }
                                else
                                {
                                    Toast.makeText(SPNDetail.this, "No Plat Required!", Toast.LENGTH_LONG).show();
                                    etDNoKendaraan.requestFocus();
                                }
                            }
                            else
                            {
                                Toast.makeText(SPNDetail.this, "Qty Receive must Number, Not Empty and > 0!", Toast.LENGTH_LONG).show();
                                edQtyReceive.requestFocus();
                            }
                        }
                    });

//                    session.setItemInfo(listItems.get(position).getMaterialName(),
//                            listItems.get(position).getDescription(),
//                            listItems.get(position).getImageUrlPath(),
//                            listItems.get(position).getCurrent_stock(),
//                            listItems.get(position).getOrdered_stock(),
//                            listItems.get(position).getReceived(),
//                            listItems.get(position).getSpec(),
//                            listItems.get(position).getUnit(),
//                            listItems.get(position).getKeterangan(),
//
//                            listItems.get(position).getKode_transaksi(),
//                            listItems.get(position).getKode_material(),
//                            listItems.get(position).getKodeanak_kodematerial(),
//                            listItems.get(position).getKeteranganpo(),
//                            listItems.get(position).getUnit1(),
//                            listItems.get(position).getUnit2(),
//                            listItems.get(position).getVolume1(),
//                            listItems.get(position).getVolume2(),
//                            listItems.get(position).getHarga_satuan_org1(),
//                            listItems.get(position).getHarga_satuan_org2(),
//                            listItems.get(position).getHarga_satuan_std1(),
//                            listItems.get(position).getHarga_satuan_std2(),
//                            listItems.get(position).getPanjang_material(),
//                            listItems.get(position).getKoefisien_konversi(),
//                            listItems.get(position).getIdTdPo()
//                    );

                    dialog.show();
                }
            });


            itemHolder.rootView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    //Jika tidak ada akses Delete
                    if(!session.getCanDelete().toString().equals("1"))
                    {
                        Toast.makeText(getApplicationContext(),"You dont have Privilege to Delete!", Toast.LENGTH_SHORT).show();
                    }
                    else {

                        if ((statusapproval.equals("4")) || (statusapproval.equals("9"))) {

                            itemHolder.llRowPo.setBackgroundColor(Color.parseColor("#60c75e5e"));

                            String namaitem = itemHolder.tvListItemName.getText().toString();
                            String kodespn = itemHolder.txtKodeSPN.getText().toString();
                            String norevisi = itemHolder.txtNoRevisi.getText().toString();
                            String nourut = itemHolder.txtNoUrut.getText().toString();
//                    Toast.makeText(SPNDetail.this, "Long Click "+namaitem+kodespn+norevisi+nourut, Toast.LENGTH_LONG).show();

                            dTittleSPNDel.setText("Delete SPN Detail");
                            dContentSPNDel.setText("" + namaitem + " ?");

                            dBtnSubmitSPNDel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //postSPNFinalRevisi(etNoSuratJalan.getText().toString(), etKeterangan.getText().toString(),id, norevisi);
                                    postDeleteSpnDetil(kodespn, norevisi, nourut);
                                    listItems.remove(position);
                                    sectionAdapter.notifyItemRemoved(position);

                                }
                            });

                            dBtnCancelSPNDel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    itemHolder.llRowPo.setBackgroundColor(Color.parseColor("#ffffff"));
                                    dialogDeleteSPNDetil.dismiss();
                                }
                            });

                            dialogDeleteSPNDetil.show();
                        } else {
                            Toast toast = Toast.makeText(SPNDetail.this, "Approval Status not allow to delete data!", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    }

                    return true;// returning true instead of false, works for me
                }
            });

        }

        @Override
        public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
            return new HeaderViewHolder(view);
        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;

            //if(headerHolder.tvTitle.getText().equals(tgldipilih)) {
                headerHolder.tvTitle.setText(title);

            headerHolder.tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(getApplicationContext(), "bisaa", Toast.LENGTH_SHORT).show();

                    WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
                    Display display = windowManager.getDefaultDisplay();
                    int displayWidth = display.getWidth();
                    int displayHeight = display.getHeight();
                    //TextView tvSelectedFiles = (TextView) findViewById(R.id.tvSelectedFiles);
//
//                    int[] location = new int[2];
//                    headerHolder.tvTitle.getLocationOnScreen(location);
//
//                    int height = displayHeight - location[1]
//                            - headerHolder.tvTitle.getHeight();
                    //b = fileToBitmap("/storage/8765-4321/ezra.jpg", 50, 15);

                    imageView2.buildDrawingCache();
                    b = imageView2.getDrawingCache();
                    //bmap = BitmapFactory.decodeResource(getResources(), R.drawable.ezra);

                    //Print();
                    //printTemplateSample(b);
//                    myPrinter = new Printer();
//                    info = new PrinterInfo();
//                    mLabelInfo = new LabelInfo();
//
//                    info = myPrinter.getPrinterInfo();
//                    Log.i("printt", "info= " + info);
//                    try{
//                       // info = myPrinter.getPrinterInfo();
//
//                        info.printerModel = PrinterInfo.Model.PT_P750W;
//                        info.ipAddress="192.168.118.1";
//                        info.macAddress="40:49:0F:89:F4:F8";
//                        info.port=PrinterInfo.Port.NET;
//                        info.paperSize = PrinterInfo.PaperSize.CUSTOM;
//                        info.printMode=PrinterInfo.PrintMode.ORIGINAL;
//                        info.numberOfCopies = 1;
//
//                        mLabelInfo.labelNameIndex = 5;
//                        mLabelInfo.isAutoCut = true;
//                        mLabelInfo.isEndCut = true;
//                        mLabelInfo.isHalfCut = false;
//                        mLabelInfo.isSpecialTape = false;
//
//                        myPrinter.setPrinterInfo(info);
//                        myPrinter.setLabelInfo(mLabelInfo);
//
//                        Log.i("printt", "info2= " + info);
//                        Log.i("printt", "mLabelInfo= " + mLabelInfo);
//                        print2();
//
//                    }catch(Exception e){
//                        e.printStackTrace();
//                        Log.i("printt", "startPTTPrint3= " + e);
//                    }
                }
            });
        }

        @Override
        public RecyclerView.ViewHolder getFooterViewHolder(View view) {
            return new FooterViewHolder(view);
        }

        @Override
        public void onBindFooterViewHolder(RecyclerView.ViewHolder holder) {
            FooterViewHolder footerHolder = (FooterViewHolder) holder;

            footerHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(), String.format("Clicked on footer of Section %s", title), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public static Bitmap fileToBitmap(String filePath, int width, int length) {

        final long imageView01Resolution = width * length;

        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(filePath, options);

        int imgSize = options.outWidth * options.outHeight;

        if (imgSize < imageView01Resolution) {
            options.inSampleSize = 1;
        } else if (imgSize < imageView01Resolution * 2 * 2) {
            options.inSampleSize = 2;
        } else {
            options.inSampleSize = 4;
        }

        float resizeScaleWidth;
        float resizeScaleHeight;
        Matrix matrix = new Matrix();
        resizeScaleWidth = (float) width / options.outWidth;
        resizeScaleHeight = (float) length / options.outHeight;
        float scale = Math.min(resizeScaleWidth, resizeScaleHeight);

        options.inJustDecodeBounds = false;

        Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

        if (bitmap != null && scale < 1.0) {
            matrix.postScale(scale * options.inSampleSize, scale
                    * options.inSampleSize);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true);
        }
        return bitmap;

    }

    Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, 1000, 1000, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 1000, 0, 0, w, h);
        return bitmap;
    }


    public void PreparePrint(){
//        dInputTitlePrint.setText("Print QrCode");
//        dNamaVendor.setText(session.getNamaVendor());
//        dNoPO.setText(kodepo);
//        dNoDO.setText(etNoSuratJalan.getText());
//        dTglPenerima.setText(today);
//        dPenerima.setText(session.getUserName());
//        dSpnNumber.setText(kodespn);

        //Jika array isi
    if(!listKodePo.isEmpty()) {
        Bitmap bitmap = null;
        try {
            bitmap = encodeAsBitmap(kodespn);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        ivContentDialogPrint.setImageBitmap(bitmap);
        ivContentDialogPrint2.setImageBitmap(bitmap);
        ivContentDialogPrint.layout(0, 0, 816/6, 816/6);
        ivContentDialogPrint.getLayoutParams().height = 816/3;
        ivContentDialogPrint.getLayoutParams().width = 816/3;
        ivContentDialogPrint.requestLayout();
        Log.d("listprintWidht4", String.valueOf(ivContentDialogPrint.getWidth()));
        Log.d("listprintheight4", String.valueOf(ivContentDialogPrint.getHeight()));

        for (int i = 0; i < listPo.size(); i++) {
            if (i % 3 == 0) {

                //Print Linear Layout Content
                llDialogContent.setDrawingCacheEnabled(true);
                llDialogContent.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

                Log.d("listprintWidht", String.valueOf(llDialogContent.getMeasuredWidth()));
                Log.d("listprintheight", String.valueOf(llDialogContent.getMeasuredHeight()));
                llDialogContent.layout(0, 0, 816, 236);
                Log.d("listprintWidht2", String.valueOf(llDialogContent.getWidth()));
                Log.d("listprintheight2", String.valueOf(llDialogContent.getHeight()));
                Log.d("listprintWidht3", String.valueOf(llDialogContent.getMeasuredWidth()));
                Log.d("listprintheight3", String.valueOf(llDialogContent.getMeasuredHeight()));
                llDialogContent.buildDrawingCache(true);
                bitmapTemp.add(Bitmap.createBitmap(llDialogContent.getDrawingCache()));
                llDialogContent.setDrawingCacheEnabled(false); // clear drawing cache
                Canvas newCanvas;
//                                if(bitmapTemp.isEmpty()) {
//                                    newCanvas = new Canvas(bitmapTemp.get(0));
//                                }
//                                else
//                                {
                newCanvas = new Canvas(bitmapTemp.get(bitmapTemp.size() - 1));
//                                }
                Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                paint.setColor(Color.BLACK);
                paint.setTextSize(28);
                //paint.setTypeface(Typeface.create("Roboto", Typeface.BOLD));

                Typeface tf = Typeface.createFromAsset(getAssets(),"Roboto-Black.ttf");
                paint.setTypeface(tf);

                newCanvas.drawText(namavendor, 816/3, 55, paint);
                newCanvas.drawText("No SPN", 816/3, 85, paint);
                newCanvas.drawText("Kode DO", 816/3, 115, paint);
                newCanvas.drawText("No PO", 816/3, 145, paint);
                newCanvas.drawText("Tgl.Penerima", 816/3, 205, paint);
                newCanvas.drawText("Penerima", 816/3, 235, paint);

                newCanvas.drawText(": " + kodespn, ((816/3)+175), 85, paint);
                newCanvas.drawText(": " + etNoSuratJalan.getText().toString(), ((816/3)+175), 115, paint);
                int k = 0;
                for (int j = 0; j < 3; j++) {
                    if (j + i < listKodePo.size()) {
                        Log.d("listPo2", listKodePo.get(j + i));
                        newCanvas.drawText(": " + listKodePo.get(j + i), ((816/3)+175), 145 + k, paint);
                        k = k + 25;
                    }
                }

                newCanvas.drawText(": " + today, ((816/3)+175), 205, paint);
                newCanvas.drawText(": " + session.getUserName(), ((816/3)+175), 235, paint);
            }
        }

        //Percobaan Print ImageView
//                        ivContentDialogPrint.buildDrawingCache();
//                        Bitmap bb = ivContentDialogPrint.getDrawingCache();

        ivSample.setImageBitmap(bitmapTemp.get(0));
        dBtnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrintQrCode();
                //bitmapTemp.clear();
                dialogprint.dismiss();

                int timeInMills = new Random().nextInt((7000 - 3000) + 1) + 3000;

                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                        int networkId = wifiManager.getConnectionInfo().getNetworkId();
                        wifiManager.enableNetwork(networkId, true);
                        //wifiManager.removeNetwork(networkId);
                        //wifiManager.saveConfiguration();
                       // wifiManager.disconnect();

                        Log.d("wifiManagerBase2", String.valueOf(networkId));
//                        wifiManagerBase.reconnect();
                    }
                }, 2000);


            }
        });

        dBtnPrintClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogprint.dismiss();
//                Intent intent = new Intent();
//                intent.putExtra("", "");
//                setResult(request_data_from, intent);
                //finish();
            }
        });
        dialogprint.show();
    }
    else
    {
        Toast.makeText(getApplicationContext(), "SPN Data not Found!", Toast.LENGTH_SHORT).show();
    }

    }


    public void PrintQrCode(){

        Printer myPrinter = new Printer();
        PrinterInfo myPrinterInfo = new PrinterInfo();
        PrinterStatus myPrinterStatus = new PrinterStatus();

        try{
            // Retrieve printer informations
            myPrinterInfo = myPrinter.getPrinterInfo();

            // Set printer informations
            myPrinterInfo.printerModel = PrinterInfo.Model.PT_P750W;
            myPrinterInfo.port=PrinterInfo.Port.NET;
            myPrinterInfo.printMode=PrinterInfo.PrintMode.FIT_TO_PAGE;
            myPrinterInfo.paperSize = PrinterInfo.PaperSize.CUSTOM;
            myPrinterInfo.orientation = PrinterInfo.Orientation.LANDSCAPE;
            myPrinterInfo.numberOfCopies = 1;

            if(Paper.book().exist("printer")) {
                String ip1 = Paper.book().read("printer").toString();
                String ip2 = ip1.replace("[","");
                String ipfinal = ip2.replace("]","");
                Log.d("LogTest", ipfinal);
                myPrinterInfo.ipAddress=ipfinal;
            }
            //myPrinterInfo.macAddress="40:49:0F:89:F4:F8"; //hidden for security reasons

            LabelInfo mLabelInfo = new LabelInfo();
            mLabelInfo.labelNameIndex = 5;
            mLabelInfo.isAutoCut = true;
            mLabelInfo.isEndCut = true;
            mLabelInfo.isHalfCut = false;
            mLabelInfo.isSpecialTape = false;
            myPrinter.setPrinterInfo(myPrinterInfo);
            myPrinter.setLabelInfo(mLabelInfo);

            try{
                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try  {
                            myPrinter.startCommunication();
                            for(int l = 0; l < bitmapTemp.size(); l++) {
                                if(l < bitmapTemp.size()) {
                                    myPrinter.printImage(bitmapTemp.get(l));
                                }
                            }
                            myPrinter.endCommunication();
                            //Log.i("printlog", "info" + printerStatus.errorCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.i("printlog", "info= " + e);
                        }
                    }
                });
                thread.start();

            }catch(Exception e){
                Log.i("printlog", "info2" + e);
            }

        }catch(Exception e){
            Log.i("printlog", "info3" + e);
        }

    }

    /** Launch the thread to print */
    public void print2() {

        PrinterThread printTread = new PrinterThread();
        printTread.start();
    }

    protected class PrinterThread extends Thread {
        @Override
        public void run() {

            // set info. for printing
            //setPrinterInfo();

            //print
//            status = new PrinterStatus();
            //Call startCommunication proir to Printimage or other print functions to open a socket connection  and keep the number connections to one
//            myPrinter.startCommunication();
            //Call Print Image to print a Bitmap
//            status = myPrinter.printImage(bmap);
            // if error, stop print next files
//            myPrinter.endCommunication();

//            if (status.errorCode != PrinterInfo.ErrorCode.ERROR_NONE) {
//                Log.i("printt", "sukses"+status.errorCode);
//            }
//            Log.i("printt0", "startPTTPrint1= " + myPrinter.startCommunication());
//            Log.i("printt1", "startPTTPrint2"+status.errorCode);
//            Log.i("printt2", "errorCode-11 = " + status.errorCode);
//            Log.i("printt5", "labelNameIndex " + mLabelInfo.labelNameIndex);
//            Log.i("printt6", "printers " + myPrinter.getNetPrinters("PT_P750W"));
//            Log.i("printtLabel-id", myPrinter.getPrinterStatus().labelId + "");
//            Log.i("printt7", "startPTTPrint1= " + info.printerModel);
//            Log.i("printt8", "startPTTPrint1= " + info.macAddress);
            //When done printing call End Communication to close the socket connection


        }
    }

    public ArrayList SetPoDataInitialData(String kodespn){
        swipeRefreshLayout.setRefreshing(true);
        session = new SessionManager(getApplicationContext());
        String date = null;
        String sortBy = "";


        controller.InqGeneralPagingFullEzra(getApplicationContext(),"GETSPNDETIL_PO_Inq_Mobile",
                "@KodeProyek",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","50",
                "@sortby","",
                "@wherecond"," AND tdspn.Kode_SPN = '"+kodespn+"' ",
                "@nik",session.isNikUserLoggedIn(),
                "@formid","PC.02.13",
                "@zonaid","",
        new VolleyCallback() {
            @Override
            public void onSave(String output) {

            }

            @Override
            public void onSuccess(JSONArray result)
            {
                try {
                    swipeRefreshLayout.setRefreshing(false);
                    if (result.length() > 0) {
                        Log.d("TesSpn","TesSpn"+result.toString());
                        for (int i = 0; i < result.length(); i++)
                        {
                            JSONObject item = result.getJSONObject(i);

//                            AllItemsSpnDetilEnt itemEnts = new AllItemsSpnDetilEnt(item.getString("path"),
//                                    item.getString("ID"),
//                                    item.getString("Deskripsi"),
//                                    item.getString("ordered_stock"),
//                                    item.getString("received_stock"),
//                                    item.getString("current_stock"),
//                                    item.getString("unit"),
//                                    item.getString("specification"),
//                                    item.getString("Keterangan"),
//
//                                    item.getString("ID_Th_PO"),
//                                    item.getString("Kode_Material"),
//                                    item.getString("Kd_Anak_Kd_Material"),
//                                    item.getString("keteranganitem"),
//                                    item.getString("Unit1"),
//                                    item.getString("Unit2"),
//                                    item.getString("Volume1"),
//                                    item.getString("Volume2"),
//                                    item.getString("Harga_Satuan_Org1"),
//                                    item.getString("Harga_Satuan_Org2"),
//                                    item.getString("Harga_Satuan_Std1"),
//                                    item.getString("Harga_Satuan_Std2"),
//                                    item.getString("Panjang_Material"),
//                                    item.getString("Koefisien_Konversi"),
//                                    item.getString("DeskripsiMerk"),
//                                    item.getString("qtyAlreadyReceived"),
//                                    item.getString("VolumeRencanaKirim"),
//                                    item.getString("IdTdPo"),
//                                    item.getString("TanggalRencanaKirimFormatEnglish")
//
//                            );
//                            listItems.add(itemEnts);
                            etNoSuratJalan.setText(item.getString("Surat_Jalan"));
                            etKeterangan.setText(item.getString("KeteranganSpn"));
                            etReason.setText(item.getString("Reason"));
                            listPo.add(item.getString("ID"));
                            listPoMark.add(item.getString("Kode_PO"));

                            if(!listKodePo.contains(item.getString("Kode_PO"))){
                                listKodePo.add(item.getString("Kode_PO"));
                            }

                            sectionAdapter.addSection(new NewsSection(NewsSection.INDEX0, item.getString("Kode_PO"), item.getString("ID"), item.getString("Kode_SPN"), ""));
                            sectionAdapter.notifyDataSetChanged();
                            Log.d("TesSpn","here"+result.toString());

                        }
                    }
                }
                catch (JSONException e){
                    Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                }
            }
        });
        return listItems;
    }

    private void preparePostSPN(){
        Gson gsonDetail = new Gson();
        SPNInsertItemQtyReceivedDetil spnInsertItemQtyReceivedDetil = new SPNInsertItemQtyReceivedDetil();
        spnInsertItemQtyReceivedDetil.setID("0");
        spnInsertItemQtyReceivedDetil.setTipe_Transaksi("PO");
        spnInsertItemQtyReceivedDetil.setKode_Transaksi(session.getIdPO());
        spnInsertItemQtyReceivedDetil.setNo_Urut("0");
        spnInsertItemQtyReceivedDetil.setKode_Aktivitas("0");
        spnInsertItemQtyReceivedDetil.setKode_Material(session.getItemKodeMaterial());
        spnInsertItemQtyReceivedDetil.setKd_Anak_Kd_Material(session.getItemKodeAnakKodeMaterial());
        spnInsertItemQtyReceivedDetil.setKeterangan(session.getItemKeteranganPo());
        spnInsertItemQtyReceivedDetil.setUnit1(session.getItemUnit1());
        spnInsertItemQtyReceivedDetil.setUnit2(session.getItemUnit2());
        spnInsertItemQtyReceivedDetil.setVolume1(edQtyReceive.getText().toString());
        spnInsertItemQtyReceivedDetil.setVolume2(edQtyReceive.getText().toString());
        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org1(session.getItemHargaSatuanOrg1());
        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org2(session.getItemHargaSatuanOrg2());
        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std1(session.getItemHargaSatuanStd1());
        spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std2(session.getItemHargaSatuanStd2());
        spnInsertItemQtyReceivedDetil.setPanjang_Material(session.getItemPanjangMaterial());
        spnInsertItemQtyReceivedDetil.setKoefisien_Konversi(session.getItemKoefisienKonversi());
        spnInsertItemQtyReceivedDetil.setKode_Lokasi("");
        spnInsertItemQtyReceivedDetil.setSurat_Jalan("");
        spnInsertItemQtyReceivedDetil.setNo_Kendaraan("");
        spnInsertItemQtyReceivedDetil.setDeskripsi(session.getItemDesc());
        spnInsertItemQtyReceivedDetil.setID_Transaksi_Detail(session.getKodeTransaksiPoDetil());
        String jsonString = gsonDetail.toJson(spnInsertItemQtyReceivedDetil);
        listBeforePostSpnDetil.add(spnInsertItemQtyReceivedDetil);

    }

    public void postSPNFinalUpdate(String nosuratjalan, String keterangan, String idspn, String norevisi){
        MProgressDialog.showProgressDialog(SPNDetail.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader1 = new Gson();
        ArrayList<SPNInsertItemQtyReceivedDetil> list = gsonHeader1.fromJson(session.getQtyReceivedArrayListSPN().toString(), new TypeToken<ArrayList<SPNInsertItemQtyReceivedDetil>>() {}.getType());

        Gson gsonHeader = new Gson();
        SPNInsertItemQtyReceived spnInsertItemQtyReceived = new SPNInsertItemQtyReceived();
        spnInsertItemQtyReceived.setID(idspn);
        spnInsertItemQtyReceived.setKode_SPN("0");
        spnInsertItemQtyReceived.setNo_Revisi(norevisi);
        spnInsertItemQtyReceived.setKodeProyek(session.getKodeProyek());
        spnInsertItemQtyReceived.setTanggal_SPN(today);
        spnInsertItemQtyReceived.setKode_Zona(session.getKodeZona());
        spnInsertItemQtyReceived.setKode_Vendor(kodevendor);
        spnInsertItemQtyReceived.setSurat_Jalan(nosuratjalan);
        spnInsertItemQtyReceived.setKeterangan(keterangan);
        spnInsertItemQtyReceived.setPengubah(session.isNikUserLoggedIn());
        spnInsertItemQtyReceived.setTokenID(session.getUserToken());
        spnInsertItemQtyReceived.setModeReq("Update");
        spnInsertItemQtyReceived.setStatus_SPN("true");
        spnInsertItemQtyReceived.setFormid("PC.02.13");
        spnInsertItemQtyReceived.setSPNDetail(listBeforePostSpnDetil);
        String jsonString = gsonHeader.toJson(spnInsertItemQtyReceived);
        Log.d("FinalInputSuccess",jsonString.toString());

        session.setReceivedItemDateTime(currentTime.toString());

        controller.SaveGeneralObject(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                try {

                    JSONObject obj = new JSONObject(output);
                    Log.d("LogSPNRes",obj.toString());

                    if(obj.getString("IsSucceed").equals("true"))
                    {
                        MProgressDialog.dismissProgressDialog();
                        Toast.makeText(getApplicationContext(),"Update SPN Success!",Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        MProgressDialog.dismissProgressDialog();
                        Toast.makeText(getApplicationContext(),"Update SPN Fail: "+obj.getString("Message"),Toast.LENGTH_SHORT).show();
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    public void postSPNFinalRevisi(String nosuratjalan, String keterangan, String idspn, String norevisi){

        MProgressDialog.showProgressDialog(SPNDetail.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        for(SPNInsertItemQtyReceivedDetil spndetil:listBeforePostSpnDetil) {
                spndetil.setID("0");
        }
        Gson gsonHeader = new Gson();
        SPNInsertItemQtyReceived spnInsertItemQtyReceived = new SPNInsertItemQtyReceived();
        spnInsertItemQtyReceived.setID(idspn);
        spnInsertItemQtyReceived.setKode_SPN("0");
        int norev = Integer.parseInt(norevisi)+1;
        spnInsertItemQtyReceived.setNo_Revisi(String.valueOf(norev));
        spnInsertItemQtyReceived.setKodeProyek(session.getKodeProyek());
        spnInsertItemQtyReceived.setTanggal_SPN(today);
        spnInsertItemQtyReceived.setKode_Zona(session.getKodeZona());
        spnInsertItemQtyReceived.setKode_Vendor(kodevendor);
        spnInsertItemQtyReceived.setSurat_Jalan(nosuratjalan);
        spnInsertItemQtyReceived.setKeterangan(keterangan);
        spnInsertItemQtyReceived.setPengubah(session.isNikUserLoggedIn());
        spnInsertItemQtyReceived.setTokenID(session.getUserToken());
        spnInsertItemQtyReceived.setModeReq("Update");
        spnInsertItemQtyReceived.setStatus_SPN("true");
        spnInsertItemQtyReceived.setFormid("PC.02.13");
        spnInsertItemQtyReceived.setSPNDetail(listBeforePostSpnDetil);
        String jsonString = gsonHeader.toJson(spnInsertItemQtyReceived);
        Log.d("FinalInputSuccess",jsonString.toString());

        session.setReceivedItemDateTime(currentTime.toString());

        controller.SaveGeneralObject(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {
                Log.d("asddd","ss"+output);

                try {

                    JSONObject obj = new JSONObject(output);
                    Log.d("LogSPNRes",obj.toString());

                    if(obj.getString("IsSucceed").equals("true"))
                    {
                        MProgressDialog.dismissProgressDialog();
                        Toast.makeText(getApplicationContext(),"Revisi SPN Success!",Toast.LENGTH_SHORT).show();
                        finish();

                    }
                    else
                    {
                        MProgressDialog.dismissProgressDialog();
                        Toast.makeText(getApplicationContext(),"Revisi SPN Fail: "+obj.getString("Message"),Toast.LENGTH_SHORT).show();
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        });

    }

    public void postDeleteSpnDetil(String kodespn, String norevisi, String nourut){

        MProgressDialog.showProgressDialog(SPNDetail.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        SPNDetil spnDetil = new SPNDetil();
        spnDetil.setKode_SPN(kodespn);
        spnDetil.setNO_Revisi(norevisi);
        spnDetil.setNo_Urut(nourut);
        listSPNDetil.add(spnDetil);

        Gson gsonDetil = new Gson();
        SPNHeader spnHeader = new SPNHeader();
        spnHeader.setPengubah(session.isNikUserLoggedIn());
        spnHeader.setTokenID(session.getUserToken());
        spnHeader.setSPNDetail(listSPNDetil);

        String jsonString2 = gsonDetil.toJson(spnHeader);
        Log.d("FinalDeleteParam",jsonString2.toString());

        controller.DeleteGeneralObject(getApplicationContext(),jsonString2,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                dialogDeleteSPNDetil.dismiss();
                Log.d("LogSPN","Deleted"+output);
                MProgressDialog.dismissProgressDialog();
                Toast.makeText(getApplicationContext(),"Delete SPN Detail Success!",Toast.LENGTH_SHORT).show();
            }
        });

    }

    //Jika user dapat posting maka tampilkan tombol btn Posting
    private void isCanPosting(String idspn, String kodespn, String actionkhusus, String norevisi) {

        MProgressDialog.showProgressDialog(SPNDetail.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.isNikUserLoggedIn());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("PC.02.13");
        approvalEnt.setReference_id(kodespn+"-"+norevisi);
        approvalEnt.setMode("is_can_posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
        //approvalEnt.setApproval_Name("Approval SPN Proyek");
        approvalEnt.setTokenID(session.getUserToken());
        approvalEnt.setIs_Mobile("1");
        approvalEnt.setReason("");

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

                if(jsonObject.get("IsCanPosting").getAsString().equals("true"))
                {
                    if(actionkhusus.equals("revisiactive")) {
                        llRevisi.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        llPosting.setVisibility(View.VISIBLE);
                    }

                }
                else
                {
                    Toast toast = Toast.makeText(SPNDetail.this,jsonObject.get("Message").getAsString()+"!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

            }
        });
    }

    private void onPosting(String idspn, String kodespn, String nosuratjalan,String keterangan, String norevisi) {

        MProgressDialog.showProgressDialog(SPNDetail.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.isNikUserLoggedIn());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("PC.02.13");
        approvalEnt.setReference_id(kodespn+"-"+norevisi);
        approvalEnt.setMode("posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
        //approvalEnt.setApproval_Name("Approval SPN Proyek");
        approvalEnt.setTokenID(session.getUserToken());
        approvalEnt.setIs_Mobile("1");
        approvalEnt.setReason("");

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {


                Log.d("OmOm2", output);
                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

//                if(jsonObject.get("IsCanApprove").getAsString().equals("true"))
//                {
//                    Log.d("OmOm3", output+kodespn);
                    updateRequestMO(jsonObject.get("Approval_Number").getAsString(), idspn, kodespn, nosuratjalan, keterangan, norevisi);
//                }
//                else
//                {
                    Toast toast = Toast.makeText(SPNDetail.this,"Your Posting Success!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    Intent intent = new Intent();
                    intent.putExtra("", "");
                    setResult(request_data_fromallsn, intent);
                    finish();
//                }

            }
        });
    }

    private void IsCanApprove(String kodematerialout, String ApprovalNumber, String IdMo) {

        MProgressDialog.showProgressDialog(SPNDetail.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.isNikUserLoggedIn());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("PC.02.13");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("");
        //approvalEnt.setApproval_Name("Approval SPN Proyek");
        //approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number(ApprovalNumber);
        approvalEnt.setTokenID(session.getUserToken());
        approvalEnt.setIs_Mobile("1");
        approvalEnt.setReason("");

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {
                Log.d("Saapp", output);
                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

                if(jsonObject.get("IsCanApprove").getAsString().equals("true"))
                {
                    llApproval.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void onApprove(String kodematerialout, String ApprovalNumber, String IsApprove, String Reason) {

        MProgressDialog.showProgressDialog(SPNDetail.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.isNikUserLoggedIn());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("PC.02.13");
        approvalEnt.setReference_id(kodematerialout+"-"+norevisi);
        approvalEnt.setMode("");
        //approvalEnt.setApproval_Name("Approval SPN Proyek");
        approvalEnt.setIs_approve(IsApprove);
        approvalEnt.setApproval_number(ApprovalNumber);
        approvalEnt.setTokenID(session.getUserToken());
        approvalEnt.setIs_Mobile("1");
        approvalEnt.setReason(Reason);

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                //JIKA UNTUK APPROVAL MAKA TOASNYA APPROVAL SUCCESS
                if(IsApprove.equals("1")) {
                    Toast toast = Toast.makeText(SPNDetail.this, "Approve Transaction Success!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                else
                {
                    Toast toast = Toast.makeText(SPNDetail.this, "Reject Transaction Success!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

                Intent intent = new Intent();
                intent.putExtra("", "");
                setResult(request_data_fromallsn, intent);
                finish();

            }
        });
    }

    private void updateRequestMO(String ApprovalNumber, String idspn, String kodespn, String nosuratjalan, String keterangan, String norevisi) {
        Log.d("OmOm4", ApprovalNumber);
        MProgressDialog.showProgressDialog(SPNDetail.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();

        SPNInsertItemQtyReceived spnInsertItemQtyReceived = new SPNInsertItemQtyReceived();
        spnInsertItemQtyReceived.setID(idspn);
        spnInsertItemQtyReceived.setKode_SPN(kodespn);
        spnInsertItemQtyReceived.setNo_Revisi(norevisi);
        spnInsertItemQtyReceived.setKodeProyek(session.getKodeProyek());
        spnInsertItemQtyReceived.setTanggal_SPN(today);
        spnInsertItemQtyReceived.setKode_Zona(session.getKodeZona());
        spnInsertItemQtyReceived.setKode_Vendor(kodevendor);
        spnInsertItemQtyReceived.setSurat_Jalan(nosuratjalan);
        spnInsertItemQtyReceived.setKeterangan(keterangan);
        spnInsertItemQtyReceived.setPengubah(session.isNikUserLoggedIn());
        spnInsertItemQtyReceived.setTokenID(session.getUserToken());
        spnInsertItemQtyReceived.setModeReq("posting");
//        spnInsertItemQtyReceived.setSPNDetail(null);
        spnInsertItemQtyReceived.setApproval_Number(ApprovalNumber);
        spnInsertItemQtyReceived.setStatus_SPN("true");
        spnInsertItemQtyReceived.setFormid("PC.02.13");
        String jsonString = gsonHeader.toJson(spnInsertItemQtyReceived);
        Log.d("FinalInputSuccess",jsonString.toString());

//        CommonRequestMoFormEnt commonRequestMoFormEnt = new CommonRequestMoFormEnt();
//        commonRequestMoFormEnt.setID(IdMo);
//        commonRequestMoFormEnt.setKode_MO(KodeMaterialOut);
//        commonRequestMoFormEnt.setKode_Proyek(session.getKodeProyek());
//        commonRequestMoFormEnt.setKode_Item("");
//        commonRequestMoFormEnt.setVolume("1");
//        commonRequestMoFormEnt.setUnit("");
//        commonRequestMoFormEnt.setWork_Code("");
//        commonRequestMoFormEnt.setKode_Tower("");
//        commonRequestMoFormEnt.setKode_Lantai("");
//        commonRequestMoFormEnt.setKode_Zona("");
//        commonRequestMoFormEnt.setCourierPhoto("");
//        commonRequestMoFormEnt.setRefHeaderSPR("0");
//        commonRequestMoFormEnt.setRefDetailSPR("0");
//        commonRequestMoFormEnt.setStatusRequest("1");
//        commonRequestMoFormEnt.setApprovalNo(ApprovalNumber);
//        commonRequestMoFormEnt.setStatusAktif("True");
//        commonRequestMoFormEnt.setPembuat(session.getUserName());
//        commonRequestMoFormEnt.setNama_Pembuat(session.getUserName());
//        commonRequestMoFormEnt.setWaktuBuat(today);
//        commonRequestMoFormEnt.setPengubah(session.getKeyNik());
//        commonRequestMoFormEnt.setNama_Pengubah(session.getUserName());
//        commonRequestMoFormEnt.setWaktuUbah(today);
//        commonRequestMoFormEnt.setComment("");
//        commonRequestMoFormEnt.setTokenID(session.getUserToken());
//        //commonRequestMoFormEnt.setModeReq("POSTING");
//        commonRequestMoFormEnt.setFormId("MA.MM.01");

       // String jsonString = gsonHeader.toJson(commonRequestMoFormEnt);

        controller.SaveGeneralObject(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {
                Log.d("OmOm5", output);
                Toast toast = Toast.makeText(SPNDetail.this,kodespn+"SPN Posting Success!", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                Intent intent = new Intent();
                intent.putExtra("", "");
                setResult(request_data_fromallsn, intent);
                finish();

            }
        });
    }


    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvTitle;
        private final View rootView;
        private final ImageView imgArrow;

        HeaderViewHolder(View view) {
            super(view);
            rootView = view;
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            imgArrow = (ImageView) view.findViewById(R.id.imgArrow);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {

        private final View rootView;

        FooterViewHolder(View view) {
            super(view);

            rootView = view;
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        private final View rootView;
        private final ImageView imgItem;
        private final TextView tvListItemName;
        private final TextView tvListItemBrand;
        private final TextView tvListQtyPo;
        private final TextView tvListQtyReceived;
        private final TextView tvListQtyInput;
        private final TextView tvListUnit;
        private final TextView tvUnit2;
        private final TextView tvQtyRcvNotRejected;
        private final TextView txtNoPlat;

        private final TextView txtKodeSPN;
        private final TextView txtNoRevisi;
        private final TextView txtNoUrut;
        private final TextView tvQtyDR;
        private final TextView tvLabelRcnTerima;

        private final RelativeLayout llRowPo;

        ItemViewHolder(View view) {
            super(view);

            rootView = view;
            imgItem = (ImageView) view.findViewById(R.id.imgItem);
            tvListItemName = (TextView) view.findViewById(R.id.tvNamaVendor);
            tvListItemBrand = (TextView) view.findViewById(R.id.tvKodeSPN);
            tvListQtyPo = (TextView) view.findViewById(R.id.tvListQtyPo);
            tvListQtyReceived = (TextView) view.findViewById(R.id.tvQtyReceived);
            tvListQtyInput = (TextView) view.findViewById(R.id.tvQtyInput);
            tvListUnit = (TextView) view.findViewById(R.id.tvUnit);
            tvUnit2 = (TextView) view.findViewById(R.id.tvUnit2);
            tvQtyRcvNotRejected = (TextView) view.findViewById(R.id.tvQtyRcvNotRejected);
            txtNoPlat = (TextView) view.findViewById(R.id.txtNoPlat);

            txtKodeSPN = (TextView) view.findViewById(R.id.tvKodeSPNReal);
            txtNoRevisi = (TextView) view.findViewById(R.id.tvNoRevisi);
            txtNoUrut = (TextView) view.findViewById(R.id.tvNoUrut);
            llRowPo = (RelativeLayout) view.findViewById(R.id.rootView);
            tvQtyDR = (TextView) view.findViewById(R.id.tvQtyDR);
            tvLabelRcnTerima = (TextView) view.findViewById(R.id.tvLabelRcnTerima);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.printoptions, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_print) {
            //if(!statusapproval.equals("4")) {


            PreparePrint();
            //}
            //else
//            {
//                Toast toast = Toast.makeText(SPNDetail.this, "Transaction have not Posted!", Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//            }
            return true;
        }
        else if(id == R.id.action_wifi)
        {
            Intent intent = new Intent(SPNDetail.this, CustomWifiListAcivity.class);
            startActivity(intent);
        }
        else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if( requestCode == request_data_from_listpoactivity2 ) {
            if (data != null){


                String kodeporesult, idporesult, tgldipilihresult, transactiontype;
                kodeporesult = data.getExtras().getString("kodepo");
                idporesult = data.getExtras().getString("idpo");
                tgldipilihresult = data.getExtras().getString("tgldipilih");
                transactiontype = data.getExtras().getString("transactiontype");
//                Log.d("woio", "kodeporesult"+kodeporesult);
//                Log.d("woio", "idporesult"+idporesult);
//                Log.d("woio", "tgldipilihresult"+tgldipilihresult);

//                sectionAdapter.addSection(new NewsSection(NewsSection.INDEX0, kodeporesult, idporesult, tgldipilihresult));
//                sectionAdapter.notifyDataSetChanged();
                sectionAdapter.addSection(new NewsSection(NewsSection.INDEX1, kodeporesult, idporesult, tgldipilihresult, transactiontype));
                //sectionAdapter.addSection(new NewsSection(NewsSection.INDEX0, kodeporesult, idporesult, tgldipilihresult));
                sectionAdapter.notifyDataSetChanged();
                listPo.add(idporesult);
                listPoMark.add(kodeporesult);

                if(!listKodePo.contains(kodeporesult)){
                    listKodePo.add(kodeporesult);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {

        //Bundle sendBundle = new Bundle();
        //sendBundle.putString("projectname", "po tes");
        //Intent intent = new Intent(getApplicationContext(), ListPOActivity.class );
        //intent.putExtras(sendBundle);
        //setResult(request_data_from,intent);

        this.finish();
    }



}
