package com.totalbp.spnapps.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.totalbp.spnapps.MainActivity;
import com.totalbp.spnapps.R;
import com.totalbp.spnapps.controller.AppController;
import com.totalbp.spnapps.receiver.ConnectivityReceiver;

/**
 * Created by Ezra.R on 23/08/2017.
 */

public class SortingAllSPNActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private RadioGroup rgKodeSPN,rgTglSpn,rgNamaVendor;
    private RadioButton rbKodeSpn1,rbKodeSpn2, rbTglSpn1, rbTglSpn2, rbNamaVendor1, rbNamaVendor2;
    public String Value = "";


    private static final int request_data_sorting_allspn_activity  = 96;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sort_spn);

        rgKodeSPN = (RadioGroup)findViewById(R.id.rgKodeSPN);
        rgTglSpn = (RadioGroup)findViewById(R.id.rgTglSpn);
        rgNamaVendor = (RadioGroup)findViewById(R.id.rgNamaVendor);

        rbKodeSpn1 = (RadioButton)findViewById(R.id.rbKodeSpn1);
        rbKodeSpn2 = (RadioButton)findViewById(R.id.rbKodeSpn2);
        rbTglSpn1 = (RadioButton)findViewById(R.id.rbTglSpn1);
        rbTglSpn2 = (RadioButton)findViewById(R.id.rbTglSpn2);
        rbNamaVendor1 = (RadioButton)findViewById(R.id.rbNamaVendor1);
        rbNamaVendor2 = (RadioButton)findViewById(R.id.rbNamaVendor2);

        getSupportActionBar().setTitle("All SPN Data Filter");

        rbKodeSpn1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // get selected radio button from radioGroup
//                int selectedId = rgKodeSPN.getCheckedRadioButtonId();
                // find the radiobutton by returned id
//                rbKodeSpn1 = (RadioButton) findViewById(selectedId);
                Value = "asckodespn";

                Bundle sendBundle = new Bundle();

                sendBundle.putString("Value", Value);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtras(sendBundle);
                setResult(request_data_sorting_allspn_activity, intent);
                finish();

            }
        });

        rbKodeSpn2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // get selected radio button from radioGroup
//                int selectedId = rgKodeSPN.getCheckedRadioButtonId();
                // find the radiobutton by returned id
//                rbKodeSpn1 = (RadioButton) findViewById(selectedId);
                Value = "desckodespn";

                Bundle sendBundle = new Bundle();

                sendBundle.putString("Value", Value);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtras(sendBundle);
                setResult(request_data_sorting_allspn_activity, intent);
                finish();
            }
        });

        rbTglSpn1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // get selected radio button from radioGroup
//                int selectedId = rgKodeSPN.getCheckedRadioButtonId();
                // find the radiobutton by returned id
//                rbKodeSpn1 = (RadioButton) findViewById(selectedId);
                Value = "asctglspn";

                Bundle sendBundle = new Bundle();

                sendBundle.putString("Value", Value);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtras(sendBundle);
                setResult(request_data_sorting_allspn_activity, intent);
                finish();
            }
        });

        rbTglSpn2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // get selected radio button from radioGroup
//                int selectedId = rgKodeSPN.getCheckedRadioButtonId();
                // find the radiobutton by returned id
//                rbKodeSpn1 = (RadioButton) findViewById(selectedId);
                Value = "desctglspn";

                Bundle sendBundle = new Bundle();

                sendBundle.putString("Value", Value);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtras(sendBundle);
                setResult(request_data_sorting_allspn_activity, intent);
                finish();
            }
        });

        rbNamaVendor1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // get selected radio button from radioGroup
//                int selectedId = rgKodeSPN.getCheckedRadioButtonId();
                // find the radiobutton by returned id
//                rbKodeSpn1 = (RadioButton) findViewById(selectedId);
                Value = "ascnamavendor";

                Bundle sendBundle = new Bundle();

                sendBundle.putString("Value", Value);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtras(sendBundle);
                setResult(request_data_sorting_allspn_activity, intent);
                finish();
            }
        });

        rbNamaVendor2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // get selected radio button from radioGroup
//                int selectedId = rgKodeSPN.getCheckedRadioButtonId();
                // find the radiobutton by returned id
//                rbKodeSpn1 = (RadioButton) findViewById(selectedId);
                Value = "descnamavendor";

                Bundle sendBundle = new Bundle();

                sendBundle.putString("Value", Value);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtras(sendBundle);
                setResult(request_data_sorting_allspn_activity, intent);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        AppController.getInstance().setConnectivityListener(this);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;


        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.rbKodeSpn1), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        CoordinatorLayout.LayoutParams params =(CoordinatorLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);
        snackbar.show();

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
}
