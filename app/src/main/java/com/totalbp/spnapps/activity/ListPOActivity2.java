package com.totalbp.spnapps.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.totalbp.spnapps.adapter.ListPoActivity2Adapter;
import com.totalbp.spnapps.interfaces.VolleyCallback;
import com.totalbp.spnapps.R;
import com.totalbp.spnapps.config.SessionManager;
import com.totalbp.spnapps.controller.AppController;
import com.totalbp.spnapps.model.HeadItemsEntity;
import com.totalbp.spnapps.model.UpcomingListPoEnt;
import com.totalbp.spnapps.receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class ListPOActivity2 extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener{
    SearchView searchView;
    public ListPoActivity2Adapter adapter;
    private AppController controller;
    private SessionManager session;
    ProgressDialog pDialog;
    ArrayList<HashMap<String, String>> projectList;
    private static final int send_data_to_list_poitems  = 98;
    public ArrayList<UpcomingListPoEnt> list = new ArrayList<>();
    public ArrayList<HeadItemsEntity> listHeadItems = new ArrayList<>();
    RecyclerView.Adapter recyclerViewAdapter;
    RecyclerView recyclerView;
    JSONArray jsonListSPNDummy = null;
    JSONObject item;
    String urlPost;
    private Button btAddItemQtyAnotherPo;
    String today, tomorrowlusa, tomorrow2, idpo, kodepo, tgldipilih, activitytype, namavendor;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private ListView listViewProject;
    private static final int request_data_from  = 92;

    private static final int request_data_from2  = 94;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2_list_po);
        controller = new AppController();
        session = new SessionManager(getApplicationContext());

        Calendar calendar = new GregorianCalendar();
        today = dateFormat.format(calendar.getTime());

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if(bd != null)
        {
            idpo = (String) bd.get("idpo");
            idpo = idpo.replace("[","");
            idpo = idpo.replace("]","");
            kodepo = (String) bd.get("kodepo");
            tgldipilih = (String) bd.get("tgldipilih");
            activitytype = (String) bd.get("activitytype");
            namavendor = (String) bd.get("namavendor");
            Log.d("LogPoActivity2", idpo);
            //Log.d("LogPoActivity2", tgldipilih);

        }
        Log.d("LogPoActivity2f", idpo);
        Log.d("LogPoActivity3f", "sss");
        ActionBar actionBar = getSupportActionBar();
        setTitle(namavendor);
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        listViewProject = (ListView) findViewById(R.id.listViewPoActivity2);

        //if(activitytype.equals("listpoactivity")) {
            //SetUpcomingDataInitial(today);
        //}
        //else
            //{
                SetUpcomingDataFromSPNDetilInitial(today);
        //}
        listViewProject.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                TextView tvListItemName = (TextView) view.findViewById(R.id.tvNamaVendor);
                TextView tvListItemBrand = (TextView) view.findViewById(R.id.tvKodeSPN);
                TextView tvItem = (TextView) view.findViewById(R.id.tvQtyReceived);
                TextView tvIdPo = (TextView) view.findViewById(R.id.tvIdPo);
                ImageView imgItem = (ImageView) view.findViewById(R.id.imgItem);
                TextView tvIDVendor = (TextView) view.findViewById(R.id.tvIDVendor);
                TextView tvTglRencanaKirim = (TextView) view.findViewById(R.id.tvTglRencanaTerima);
                TextView tvTransactionType = (TextView) view.findViewById(R.id.tvTransactionType);


                Intent intent = new Intent();
                intent.putExtra("idpo", tvIdPo.getText().toString());
                intent.putExtra("kodepo", tvListItemBrand.getText().toString());
                intent.putExtra("tgldipilih", tvTglRencanaKirim.getText().toString());
                intent.putExtra("transactiontype", tvTransactionType.getText().toString());
                Log.d("LogPoActivity2", tvIdPo.getText().toString());
                Log.d("LogPoActivity2", tvListItemBrand.getText().toString());
                Log.d("LogPoActivity2", tvTransactionType.getText().toString());

                if(activitytype.equals("listpoactivity"))
                {
                    setResult(request_data_from, intent);
                }
                else
                {
                    setResult(request_data_from2, intent);
                }
                finish();

            }
        });

    }



    public ArrayList SetUpcomingDataInitial(String today){
        session = new SessionManager(getApplication().getApplicationContext());

        controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_PO_Orders_Mobile_Delivery_Request_Item",
                "@KodeProyek",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
//                "@wherecond"," AND mv.ID ='"+session.getIDVendor()+"' AND th.ID NOT IN ("+idpo+")  ",
                "@wherecond","  ",
                "@nik",session.isNikUserLoggedIn(),
                "@formid","PC.02.13",
                "@zonaid","",


                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);

                                    UpcomingListPoEnt itemEnts = new UpcomingListPoEnt(
                                            item.getString("Nama_Vendor"),item.getString("Kode_PO"),item.getString("Total_Item") + " items",item.getString("ID"),item.getString("TanggalRencanaKirim"), item.getString("IDVendor"), item.getString("Tanggal_Rencana_Terima2"), item.getString("TransactionType"),item.getString("NoDeliveryRequest")
                                    );
                                    list.add(itemEnts);
                                }
                            }
                            if (list.size() > 0 & listViewProject != null) {
                                adapter = new ListPoActivity2Adapter(getApplicationContext(),list);
                                listViewProject.setAdapter(adapter);
                                listViewProject.setTextFilterEnabled(false);
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        return list;
    }

    public ArrayList SetUpcomingDataFromSPNDetilInitial(String today){
        session = new SessionManager(getApplication().getApplicationContext());

        controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_PO_Orders_Mobile_Delivery_Request_Item2",
                "@KodeProyek",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","100",
                "@sortby","",
//                "@wherecond"," AND mv.ID ='"+session.getIDVendor()+"' AND th.ID NOT IN ("+idpo+")  ",
                "@wherecond"," AND mv.ID ='"+session.getIDVendor()+"' ",
                "@nik",session.isNikUserLoggedIn(),
                "@formid","PC.02.13",
                "@zonaid","",


                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);

                                    UpcomingListPoEnt itemEnts = new UpcomingListPoEnt(
                                            item.getString("Nama_Vendor"),item.getString("Kode_PO"),item.getString("Total_Item") + " items",item.getString("ID"),item.getString("TanggalRencanaKirim"), item.getString("IDVendor"), item.getString("Tanggal_Rencana_Terima2"), item.getString("TransactionType"),item.getString("NoDeliveryRequest")
                                    );
                                    list.add(itemEnts);
                                }
                            }
                            if (list.size() > 0 & listViewProject != null) {
                                adapter = new ListPoActivity2Adapter(getApplicationContext(),list);
                                listViewProject.setAdapter(adapter);
                                listViewProject.setTextFilterEnabled(false);
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        return list;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search_item, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        //*** setOnQueryTextFocusChangeListener ***
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchQuery) {
                adapter.filter(searchQuery.toString().trim());
                listViewProject.invalidate();
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        });
        return true;


    }



    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        AppController.getInstance().setConnectivityListener(this);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;


        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.listViewPoActivity2), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);
        snackbar.show();

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
}
