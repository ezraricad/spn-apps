package com.totalbp.spnapps.activity;

import android.app.Dialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.brother.ptouch.sdk.LabelInfo;
import com.brother.ptouch.sdk.Printer;
import com.brother.ptouch.sdk.PrinterInfo;
import com.brother.ptouch.sdk.PrinterStatus;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.squareup.picasso.Picasso;
import com.totalbp.spnapps.R;
import com.totalbp.spnapps.config.AppConfig;
import com.totalbp.spnapps.config.SessionManager;
import com.totalbp.spnapps.controller.AppController;
import com.totalbp.spnapps.interfaces.VolleyCallback;
import com.totalbp.spnapps.model.AllItemsEnt;
import com.totalbp.spnapps.model.ApprovalEnt;
import com.totalbp.spnapps.model.HeadItemsEntity;
import com.totalbp.spnapps.model.NewSPNEnt;
import com.totalbp.spnapps.model.SPNInsertItemQtyReceived;
import com.totalbp.spnapps.model.SPNInsertItemQtyReceivedDetil;
import com.totalbp.spnapps.model.SPNInsertItemQtyReceivedDetilTemp;
import com.totalbp.spnapps.receiver.ConnectivityReceiver;
import com.totalbp.spnapps.spndb.DatabaseHandler;
import com.totalbp.spnapps.utils.MProgressDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;
import io.paperdb.Paper;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;
import java.util.stream.Collectors;


public class ListPOActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    SearchView searchView;
    private ListView listViewProject;
    private AppController controller;
    private SessionManager session;
    ArrayList<HashMap<String, String>> projectList;
    private static final int send_data_to_list_poitems  = 98;
    public ArrayList<AllItemsEnt> listItems = new ArrayList<>();
    public ArrayList<HeadItemsEntity> listHeadItems = new ArrayList<>();
    List<String> listPo = new ArrayList<String>();
    List<String> listPoMark = new ArrayList<String>();
    List<String> listItemMark = new ArrayList<String>();
    List<String> listKodePo = new ArrayList<String>();
    RecyclerView.Adapter recyclerViewAdapter;
    RecyclerView recyclerView;
    JSONArray jsonListSPNDummy = null;
    JSONObject item;
    String urlPost;
    private Button btAddItemQtyAnotherPo, btnSubmit;
    private static final int request_data_from  = 91;
    String kodepo, idpo, tgldipilih, transactiontype, nodr, idvendor, namavendor;
    private SectionedRecyclerViewAdapter sectionAdapter;
    private Dialog dialog, dialogprint, dialogApr, dialogConfirmationPosting, dialogcart;
    private Button btnAnotherPo, btnSubmitSPN;
    private static final int request_data_from_listpoactivity2  = 92;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
   // private ArrayList<AllItemsEnt> listBeforePostSpnDetil;
    //private EditText edQtyReceive;
    public ArrayList<SPNInsertItemQtyReceivedDetil> listBeforePostSpnDetil = new ArrayList<>();
    Date currentTime = Calendar.getInstance().getTime();
    String today;


    private Handler mHandler = new Handler();
    //Dialog input declare
    private TextView dInputTitle, dInputTitlePrint,dCartTittle, tvQtyBeforeDialog, tvUnit1Dialog, tvUnit2Dialog, dNamaVendor, dNoPO, dNoDO, dTglPenerima, dPenerima, dSpnNumber, tvItemPreSubmit;
    private EditText edQtyReceive, etNoSuratJalan, etKeterangan, etDNoKendaraan;
    private Button btnDialogInputCancel, btnDialogInputSubmit,buttonDialogDown, buttonDialogUp, dBtnPrint, dBtnPrintClose, dBtnCartClose;
    private ImageView ivContentDialogPrint, ivContentDialogPrint2, ivSample;
    private LinearLayout llDialogContent, llReviewListItem;
    ArrayList<Bitmap> bitmapTemp = new ArrayList<Bitmap>();
    float[] maxQtyNow;
    private String latestvol;
    Boolean isanydata;
    DatabaseHandler db=new DatabaseHandler(this);
    SwipeRefreshLayout swipeRefreshLayout;
    List<String> listHeaderForFooter = new ArrayList<>();

    int sectionOneUp0 = 1;
    int sectionTwoUp0 = 1;
    int sectionThreeUp0 = 1;
    int sectionFourUp0 = 1;
    int sectionOneUp1 = 1;
    int sectionTwoUp1 = 1;
    int sectionThreeUp1 = 1;
    int sectionFourUp1 = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_po_form_final);

        session = new SessionManager(getApplicationContext());
        isanydata = db.checkForTables();
        if(isanydata)
        {
            db.deleteTable();
            Log.d("LogSpnDBDelete", "benar");
        }





        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if(bd != null)
        {
            idpo = (String) bd.get("idpo");

            kodepo = (String) bd.get("kodepo");
            tgldipilih = (String) bd.get("tgldipilih");
            transactiontype = (String) bd.get("transactiontype");
            nodr = (String) bd.get("nodr");
            Log.d("LogSpntransactiontype", transactiontype);
            idvendor = (String) bd.get("idvendor");
            namavendor = (String) bd.get("namavendor");

            //Jika tidak ada akses insert
            if(!session.getCanInsert().toString().equals("1"))
            {
                Toast.makeText(getApplicationContext(),"You dont have Privilege to Insert!", Toast.LENGTH_SHORT).show();
                this.finish();
            }
        }

        btnAnotherPo = (Button) findViewById(R.id.btnAnotherPo);
        btnSubmitSPN = (Button) findViewById(R.id.btnSubmitSPN);
        etNoSuratJalan = (EditText) findViewById(R.id.etNoSuratJalan);
        etKeterangan = (EditText) findViewById(R.id.etKeterangan);
        tvItemPreSubmit = (TextView) findViewById(R.id.tvItemPreSubmit);

        session = new SessionManager(getApplicationContext());

        ActionBar actionBar = getSupportActionBar();
        setTitle(namavendor);
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        controller = new AppController();

        int timeInMills = new Random().nextInt((7000 - 3000) + 1) + 3000;
        //sectionAdapter.notifyDataSetChanged();
        Calendar calendar = new GregorianCalendar();
        today = dateFormat.format(calendar.getTime());

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        sectionAdapter = new SectionedRecyclerViewAdapter();
        sectionAdapter.addSection(new NewsSection(NewsSection.INDEX1, kodepo, idpo, tgldipilih, transactiontype));

//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                int failed = new Random().nextInt((3 - 1) + 1) + 1;
//
//                if (failed == 1) {
//                    //section.setState(Section.State.FAILED);
//                }
//                else {

//                    Log.d("LogSpntransactiontype2", transactiontype);
//                    if(transactiontype.equals("PO")) {
//                        listItems = SetPoDataInitialData(idpo, tgldipilih);
//                    }
//                    else if(transactiontype.equals("KONTRAK")) {
//
//                        listItems = SetPoDataInitialDataKontrak(idpo, tgldipilih);
//                    }
//                    else
//                    {
//                        listItems = SetPoDataInitialDataNpo(idpo, tgldipilih);
//                    }

//                }
//
//                sectionAdapter.notifyDataSetChanged();
//            }
//        }, timeInMills);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(sectionAdapter);

        //Dialog untuk input
        dialog = new Dialog(ListPOActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.general_dialog);
        dialog.setCanceledOnTouchOutside(false);

        dInputTitle = (TextView) dialog.findViewById(R.id.dTittle);
        tvQtyBeforeDialog = (TextView) dialog.findViewById(R.id.dQtyBefore);
        edQtyReceive = (EditText) dialog.findViewById(R.id.dQtyReceive);
        etDNoKendaraan = (EditText) dialog.findViewById(R.id.etDNoKendaraan);
        tvUnit1Dialog = (TextView) dialog.findViewById(R.id.dUnit1);
        tvUnit2Dialog = (TextView) dialog.findViewById(R.id.dUnit2);
        btnDialogInputCancel = (Button) dialog.findViewById(R.id.dBtnCancel);
        btnDialogInputSubmit = (Button) dialog.findViewById(R.id.dBtnSubmit);
        buttonDialogDown = (Button) dialog.findViewById(R.id.buttonDialogDown);
        buttonDialogUp = (Button) dialog.findViewById(R.id.buttonDialogUp);

        //Dialog untuk print
        dialogprint = new Dialog(ListPOActivity.this);
        dialogprint.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogprint.setContentView(R.layout.general_dialog_print);
        dialogprint.setCanceledOnTouchOutside(false);

        dInputTitlePrint = (TextView) dialogprint.findViewById(R.id.dPrintTittle);
        ivContentDialogPrint = (ImageView) dialogprint.findViewById(R.id.ivPrintContent);
        ivContentDialogPrint2 = (ImageView) dialogprint.findViewById(R.id.ivPrintContent2);
        ivSample = (ImageView) dialogprint.findViewById(R.id.ivSample);
        llDialogContent = (LinearLayout) dialogprint.findViewById(R.id.llDialogContent);
        dNamaVendor = (TextView) dialogprint.findViewById(R.id.dNamaVendor);
        dNoPO = (TextView) dialogprint.findViewById(R.id.dNoPo);
        dNoDO = (TextView) dialogprint.findViewById(R.id.dNoDo);
        dTglPenerima = (TextView) dialogprint.findViewById(R.id.dTglPenerima);
        dPenerima = (TextView) dialogprint.findViewById(R.id.dPenerima);
        dSpnNumber = (TextView) dialogprint.findViewById(R.id.dSpnNumber);
        dBtnPrint = (Button) dialogprint.findViewById(R.id.dBtnPrint);
        dBtnPrintClose = (Button) dialogprint.findViewById(R.id.dBtnPrintClose);

        //Dialog untuk display cart (belum digunakan
//        dialogcart = new Dialog(ListPOActivity.this);
//        dialogcart.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialogcart.setContentView(R.layout.general_dialog_cart);
//        dialogcart.setCanceledOnTouchOutside(false);
//        dCartTittle = (TextView) dialogcart.findViewById(R.id.dCartTittle);
//        llReviewListItem = (LinearLayout) dialogcart.findViewById(R.id.llReviewListItem);
//        dBtnCartClose = (Button) dialogcart.findViewById(R.id.dBtnCartClose);

        btnAnotherPo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!listPo.isEmpty())
                {
                    Intent intent = new Intent(ListPOActivity.this, ListPOActivity2.class);
                    intent.putExtra("idpo", listPoMark.toString());
                    intent.putExtra("activitytype", "listpoactivity");
                    intent.putExtra("namavendor", namavendor);
                    startActivityForResult(intent, request_data_from_listpoactivity2);
                }
                else
                {
                    Toast.makeText(ListPOActivity.this, "You don't have input Qty!", Toast.LENGTH_LONG).show();
                }

            }
        });

        btnSubmitSPN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(!etNoSuratJalan.getText().toString().equals("")) {



                    if(!etKeterangan.getText().toString().equals("")) {
                        if(listBeforePostSpnDetil.size() > 0)
                        {
                            postSPNFinal(etNoSuratJalan.getText().toString(), etKeterangan.getText().toString());
                        }
                        else
                        {
                            Toast.makeText(ListPOActivity.this, "No item selected found!", Toast.LENGTH_LONG).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(ListPOActivity.this, "Keterangan Required", Toast.LENGTH_LONG).show();
                        etKeterangan.requestFocus();
                    }
                }
                else
                {
                    Toast.makeText(ListPOActivity.this, "Surat Jalan Required", Toast.LENGTH_LONG).show();
                    etNoSuratJalan.requestFocus();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        //AppController.getInstance().setConnectivityListener(this);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;


        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.etNoSuratJalan), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);
        snackbar.show();


    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

//    @Override
//    public void onNetworkConnectionChanged(boolean isConnected) {
//        showSnack(isConnected);
//    }

    private class NewsSection extends StatelessSection {

        final static int INDEX0 = 0;
        final static int INDEX1 = 1;

        final int topic;

        String title;
        String titleTgl, titleTransNo, titleTypeTrans, titleIndex;

        ArrayList<AllItemsEnt> listItems = new ArrayList<>();

        int imgPlaceholderResId;
        //boolean expanded = true;

        NewsSection(int topic, String kodePo, String idPo, String tglDipilih, String transactiontypeHere) {
            super(new SectionParameters.Builder(R.layout.fragment_list_item_po_row)
                    .headerResourceId(R.layout.fragment_list_item_po_row_header)
                    .footerResourceId(R.layout.fragment_upcoming_orders_row_footer)
                    .build());

            this.topic = topic;


            switch (topic) {
                case INDEX0:
                    this.title = kodePo;
                    this.titleTransNo = idPo;
                    this.titleTgl = tglDipilih;
                    this.titleTypeTrans = transactiontypeHere;
                    this.titleIndex = "0";

                    if(transactiontypeHere.equals("PO")) {
                        this.listItems = SetPoDataBase(idPo, tglDipilih, String.valueOf("1"), String.valueOf("10"));
                        this.imgPlaceholderResId = R.drawable.ic_spn_basedpo;
                    }
                    else if(transactiontypeHere.equals("Kontrak")) {
                        this.listItems = SetKontrakDataBase(idPo, tglDipilih, String.valueOf("1"), String.valueOf("10"));
                        this.imgPlaceholderResId = R.drawable.ic_spn_basedkontrak;
                    }
//                    else if(transactiontypeHere.equals("KONTRAKNODR")) {
//                        this.listItems = SetKontrakDataNoDr(idPo, tglDipilih, String.valueOf("1"), String.valueOf("10"));
//                        this.imgPlaceholderResId = R.drawable.ic_spn_basedkontrak;
//                    }
                    else
                    {
                        this.listItems = SetNpoData(idPo, tglDipilih, String.valueOf("1"), String.valueOf("10"));
                        this.imgPlaceholderResId = R.drawable.ic_spn_basednpo;
                    }
                    break;
                case INDEX1:
                    this.title = kodePo;
                    this.titleTransNo = idPo;
                    this.titleTgl = tglDipilih;
                    this.titleTypeTrans = transactiontypeHere;
                    this.titleIndex = "1";

                    if(transactiontypeHere.equals("PO")) {
                        this.listItems = SetPoDataBase(idPo, tglDipilih, String.valueOf("1"), String.valueOf("10"));
                        this.imgPlaceholderResId = R.drawable.ic_spn_basedpo;
                    }
                    else if(transactiontypeHere.equals("Kontrak")) {
                        this.listItems = SetKontrakDataBase(idPo, tglDipilih, String.valueOf("1"), String.valueOf("10"));
                        this.imgPlaceholderResId = R.drawable.ic_spn_basedkontrak;
                    }
//                    else if(transactiontypeHere.equals("KONTRAKNODR")) {
//                        this.listItems = SetKontrakDataNoDr(idPo, tglDipilih, String.valueOf("1"), String.valueOf("10"));
//                        this.imgPlaceholderResId = R.drawable.ic_spn_basedkontrak;
//                    }
                    else
                    {
                        this.listItems = SetNpoDataBase(idPo, tglDipilih, String.valueOf("1"), String.valueOf("10"));
                        this.imgPlaceholderResId = R.drawable.ic_spn_basednpo;
                    }
                    break;
            }
        }

        private List<String> getNews(int arrayResource) {
            return new ArrayList<>(Arrays.asList(getResources().getStringArray(arrayResource)));
        }


        public ArrayList SetPoData(String number, String tglDipilih, String pageup, String pagedown){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());
            String date = null;
            String sortBy = "";

            //JIKA TGL DIPILIH KOSONG ALIAS DARI ADD ANOTHER PO/LISTPOACTIVITY2 MAKA TDK ADA PARAM TANGGAL
            if(tglDipilih.equals(""))
            {
                tglDipilih = "";
            }
            else
                {
                tglDipilih = "AND CONVERT(VARCHAR,tdDR.TanggalRencanaKirim,105) = '"+tglDipilih+"' ";
            }
            controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedPo",
                    "@KodeProyek",session.getKodeProyek(),
                    "@currentpage",pageup,
                    "@pagesize",pagedown,
                    "@sortby","",
                    "@wherecond"," "+tglDipilih+" AND th.ID = '"+number+"' ",
                   // "@wherecond"," AND th.ID = '"+number+"' ",
                    "@nik",session.isNikUserLoggedIn(),
                    "@formid","PC.02.13",
                    "@zonaid","",
                    new VolleyCallback() {
                @Override
                public void onSave(String output) {

                }

                @Override
                public void onSuccess(JSONArray result) {
                    try {
                        swipeRefreshLayout.setRefreshing(false);
                        if (result.length() > 0) {
                            for (int i = 0; i < result.length(); i++) {
                                JSONObject item = result.getJSONObject(i);

                                AllItemsEnt itemEnts = new AllItemsEnt(item.getString("path"),
                                        item.getString("ID"),
                                        item.getString("Deskripsi"),
                                        item.getString("ordered_stock"),
                                        item.getString("received_stock"),
                                        item.getString("current_stock"),
                                        item.getString("unit"),
                                        item.getString("specification"),
                                        item.getString("Keterangan"),

                                        item.getString("ID_Th_PO"),
                                        item.getString("Kode_Material"),
                                        item.getString("Kd_Anak_Kd_Material"),
                                        item.getString("keteranganitem"),
                                        item.getString("Unit1"),
                                        item.getString("Unit2"),
                                        item.getString("Volume1"),
                                        item.getString("Volume2"),
                                        item.getString("Harga_Satuan_Org1"),
                                        item.getString("Harga_Satuan_Org2"),
                                        item.getString("Harga_Satuan_Std1"),
                                        item.getString("Harga_Satuan_Std2"),
                                        item.getString("Panjang_Material"),
                                        item.getString("Koefisien_Konversi"),
                                        item.getString("DeskripsiMerk"),
                                        item.getString("qtyAlreadyReceived"),
                                        item.getString("VolumeRencanaKirim"),
                                        item.getString("IdTdPo"),
                                        item.getString("TanggalRencanaKirimFormatEnglish"),
                                        item.getString("Kode_PO"),
                                        item.getString("TransactionType"),
                                        item.getString("spesifikasi")

                                );
                                listItems.add(itemEnts);
                                Log.d("LogSpn","InitialData2BasedPo"+result.toString());
                            }
                            sectionAdapter.notifyDataSetChanged();
                        }
                    }catch (JSONException e){
                        Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                    }
                }
            });
            return listItems;
        }

        public ArrayList SetPoDataBase(String number, String tglDipilih, String pageup, String pagedown){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());
            String date = null;
            String sortBy = "";

            //JIKA TGL DIPILIH KOSONG ALIAS DARI ADD ANOTHER PO/LISTPOACTIVITY2 MAKA TDK ADA PARAM TANGGAL
            if(tglDipilih.equals(""))
            {
                tglDipilih = "";
            }
            else if(tglDipilih.equals("null"))
            {
                tglDipilih = "";
            }
            else
            {
                tglDipilih = "AND CONVERT(VARCHAR,tdDR.TanggalRencanaKirim,105) = '"+tglDipilih+"' ";
            }
            controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedPo1",
                    "@KodeProyek",session.getKodeProyek(),
                    "@currentpage",pageup,
                    "@pagesize",pagedown,
                    "@sortby","",
                    "@wherecond"," "+tglDipilih+" AND th.ID = '"+number+"' ",
                    // "@wherecond"," AND th.ID = '"+number+"' ",
                    "@nik",session.isNikUserLoggedIn(),
                    "@formid","PC.02.13",
                    "@zonaid","",
                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        AllItemsEnt itemEnts = new AllItemsEnt(item.getString("path"),
                                                item.getString("ID"),
                                                item.getString("Deskripsi"),
                                                item.getString("ordered_stock"),
                                                item.getString("received_stock"),
                                                item.getString("current_stock"),
                                                item.getString("unit"),
                                                item.getString("specification"),
                                                item.getString("Keterangan"),

                                                item.getString("ID_Th_PO"),
                                                item.getString("Kode_Material"),
                                                item.getString("Kd_Anak_Kd_Material"),
                                                item.getString("keteranganitem"),
                                                item.getString("Unit1"),
                                                item.getString("Unit2"),
                                                item.getString("Volume1"),
                                                item.getString("Volume2"),
                                                item.getString("Harga_Satuan_Org1"),
                                                item.getString("Harga_Satuan_Org2"),
                                                item.getString("Harga_Satuan_Std1"),
                                                item.getString("Harga_Satuan_Std2"),
                                                item.getString("Panjang_Material"),
                                                item.getString("Koefisien_Konversi"),
                                                item.getString("DeskripsiMerk"),
                                                item.getString("qtyAlreadyReceived"),
                                                item.getString("VolumeRencanaKirim"),
                                                item.getString("IdTdPo"),
                                                item.getString("TanggalRencanaKirimFormatEnglish"),
                                                item.getString("Kode_PO"),
                                                item.getString("TransactionType"),
                                                item.getString("spesifikasi")

                                        );
                                        listItems.add(itemEnts);
                                        Log.d("c","InitialData2BasedPo"+result.toString());
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                }
                            }catch (JSONException e){
                                Toast.makeText(getApplicationContext(), "CATCH "+e.toString(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            return listItems;
        }

        public ArrayList SetKontrakData(String number, String tglDipilih, String pageup, String pagedown){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());
            String date = null;
            String sortBy = "";

            //JIKA TGL DIPILIH KOSONG ALIAS DARI ADD ANOTHER PO/LISTPOACTIVITY2 MAKA TDK ADA PARAM TANGGAL
            if(tglDipilih.equals(""))
            {
                tglDipilih = "";
            }
            else
            {
                tglDipilih = "AND CONVERT(VARCHAR,tdDR.TanggalRencanaKirim,105) = '"+tglDipilih+"' ";
            }
            controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedKontrak_Mobile",
                    "@KodeProyek",session.getKodeProyek(),
                    "@currentpage",pageup,
                    "@pagesize",pagedown,
                    "@sortby","",
                    "@wherecond"," "+tglDipilih+" AND th.ID = '"+number+"' ",
                    // "@wherecond"," AND th.ID = '"+number+"' ",
                    "@nik",session.isNikUserLoggedIn(),
                    "@formid","PC.02.13",
                    "@zonaid","",
                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        AllItemsEnt itemEnts = new AllItemsEnt(item.getString("path"),
                                                item.getString("ID"),
                                                item.getString("Deskripsi"),
                                                item.getString("ordered_stock"),
                                                item.getString("received_stock"),
                                                item.getString("current_stock"),
                                                item.getString("unit"),
                                                item.getString("specification"),
                                                item.getString("Keterangan"),

                                                item.getString("ID_Th_PO"),
                                                item.getString("Kode_Material"),
                                                item.getString("Kd_Anak_Kd_Material"),
                                                item.getString("keteranganitem"),
                                                item.getString("Unit1"),
                                                item.getString("Unit2"),
                                                item.getString("Volume1"),
                                                item.getString("Volume2"),
                                                item.getString("Harga_Satuan_Org1"),
                                                item.getString("Harga_Satuan_Org2"),
                                                item.getString("Harga_Satuan_Std1"),
                                                item.getString("Harga_Satuan_Std2"),
                                                item.getString("Panjang_Material"),
                                                item.getString("Koefisien_Konversi"),
                                                item.getString("DeskripsiMerk"),
                                                item.getString("qtyAlreadyReceived"),
                                                item.getString("VolumeRencanaKirim"),
                                                item.getString("IdTdPo"),
                                                item.getString("TanggalRencanaKirimFormatEnglish"),
                                                item.getString("Kode_PO"),
                                                item.getString("TransactionType"),
                                                item.getString("spesifikasi")

                                        );
                                        listItems.add(itemEnts);
                                        Log.d("LogSpn","InitialData2BasedKontrak"+result.toString());
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                }
                            }catch (JSONException e){
                                Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            return listItems;
        }

        public ArrayList SetKontrakDataNoDr(String number, String tglDipilih, String pageup, String pagedown){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());
            String date = null;
            String sortBy = "";

            //JIKA TGL DIPILIH KOSONG ALIAS DARI ADD ANOTHER PO/LISTPOACTIVITY2 MAKA TDK ADA PARAM TANGGAL
            if(tglDipilih.equals(""))
            {
                tglDipilih = "";
            }
            else
            {
                tglDipilih = "AND CONVERT(VARCHAR,tdDR.TanggalRencanaKirim,105) = '"+tglDipilih+"' ";
            }
            controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedKontrakWithoutDr_Mobile",
                    "@KodeProyek",session.getKodeProyek(),
                    "@currentpage",pageup,
                    "@pagesize",pagedown,
                    "@sortby","",
                    "@wherecond"," AND th.ID = '"+number+"' ",
                    // "@wherecond"," AND th.ID = '"+number+"' ",
                    "@nik",session.isNikUserLoggedIn(),
                    "@formid","PC.02.13",
                    "@zonaid","",
                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        AllItemsEnt itemEnts = new AllItemsEnt(item.getString("path"),
                                                item.getString("ID"),
                                                item.getString("Deskripsi"),
                                                item.getString("ordered_stock"),
                                                item.getString("received_stock"),
                                                item.getString("current_stock"),
                                                item.getString("unit"),
                                                item.getString("specification"),
                                                item.getString("Keterangan"),

                                                item.getString("ID_Th_PO"),
                                                item.getString("Kode_Material"),
                                                item.getString("Kd_Anak_Kd_Material"),
                                                item.getString("keteranganitem"),
                                                item.getString("Unit1"),
                                                item.getString("Unit2"),
                                                item.getString("Volume1"),
                                                item.getString("Volume2"),
                                                item.getString("Harga_Satuan_Org1"),
                                                item.getString("Harga_Satuan_Org2"),
                                                item.getString("Harga_Satuan_Std1"),
                                                item.getString("Harga_Satuan_Std2"),
                                                item.getString("Panjang_Material"),
                                                item.getString("Koefisien_Konversi"),
                                                item.getString("DeskripsiMerk"),
                                                item.getString("qtyAlreadyReceived"),
                                                item.getString("VolumeRencanaKirim"),
                                                item.getString("IdTdPo"),
                                                item.getString("TanggalRencanaKirimFormatEnglish"),
                                                item.getString("Kode_PO"),
                                                item.getString("TransactionType"),
                                                item.getString("spesifikasi")

                                        );
                                        listItems.add(itemEnts);
                                        Log.d("LogSpn","InitialData2BasedKontrakNoDr"+result.toString());
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                }
                            }catch (JSONException e){
                                Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            return listItems;
        }

        public ArrayList SetKontrakDataBase(String number, String tglDipilih, String pageup, String pagedown){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());
            String date = null;
            String sortBy = "";

            //JIKA TGL DIPILIH KOSONG ALIAS DARI ADD ANOTHER PO/LISTPOACTIVITY2 MAKA TDK ADA PARAM TANGGAL
            if(tglDipilih.equals(""))
            {
                tglDipilih = "";
            }
            else if(tglDipilih.equals("null"))
            {
                tglDipilih = "";
            }
            else
            {
                tglDipilih = "AND CONVERT(VARCHAR,tdDR.TanggalRencanaKirim,105) = '"+tglDipilih+"' ";
            }
            controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedKontrak_Mobile1",
                    "@KodeProyek",session.getKodeProyek(),
                    "@currentpage",pageup,
                    "@pagesize",pagedown,
                    "@sortby","",
                    "@wherecond"," "+tglDipilih+" AND th.ID = '"+number+"' ",
                    // "@wherecond"," AND th.ID = '"+number+"' ",
                    "@nik",session.isNikUserLoggedIn(),
                    "@formid","PC.02.13",
                    "@zonaid","",
                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        AllItemsEnt itemEnts = new AllItemsEnt(item.getString("path"),
                                                item.getString("ID"),
                                                item.getString("Deskripsi"),
                                                item.getString("ordered_stock"),
                                                item.getString("received_stock"),
                                                item.getString("current_stock"),
                                                item.getString("unit"),
                                                item.getString("specification"),
                                                item.getString("Keterangan"),

                                                item.getString("ID_Th_PO"),
                                                item.getString("Kode_Material"),
                                                item.getString("Kd_Anak_Kd_Material"),
                                                item.getString("keteranganitem"),
                                                item.getString("Unit1"),
                                                item.getString("Unit2"),
                                                item.getString("Volume1"),
                                                item.getString("Volume2"),
                                                item.getString("Harga_Satuan_Org1"),
                                                item.getString("Harga_Satuan_Org2"),
                                                item.getString("Harga_Satuan_Std1"),
                                                item.getString("Harga_Satuan_Std2"),
                                                item.getString("Panjang_Material"),
                                                item.getString("Koefisien_Konversi"),
                                                item.getString("DeskripsiMerk"),
                                                item.getString("qtyAlreadyReceived"),
                                                item.getString("VolumeRencanaKirim"),
                                                item.getString("IdTdPo"),
                                                item.getString("TanggalRencanaKirimFormatEnglish"),
                                                item.getString("Kode_PO"),
                                                item.getString("TransactionType"),
                                                item.getString("spesifikasi")

                                        );
                                        listItems.add(itemEnts);
                                        Log.d("LogSpn","InitialData2BasedKontrak"+result.toString());
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                }
                            }catch (JSONException e){
                                Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            return listItems;
        }

        public ArrayList SetNpoData(String number, String tglDipilih, String pageup, String pagedown){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());
            String date = null;
            String sortBy = "";


            controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedNpo_Mobile",
                    "@KodeProyek",session.getKodeProyek(),
                    "@currentpage",pageup,
                    "@pagesize",pagedown,
                    "@sortby","",
                    "@wherecond"," AND th.ID = '"+number+"' ",
                    // "@wherecond"," AND th.ID = '"+number+"' ",
                    "@nik",session.isNikUserLoggedIn(),
                    "@formid","PC.02.13",
                    "@zonaid","",
                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        AllItemsEnt itemEnts = new AllItemsEnt(item.getString("path"),
                                                item.getString("ID"),
                                                item.getString("Deskripsi"),
                                                item.getString("ordered_stock"),
                                                item.getString("received_stock"),
                                                item.getString("current_stock"),
                                                item.getString("unit"),
                                                item.getString("specification"),
                                                item.getString("Keterangan"),

                                                item.getString("ID_Th_PO"),
                                                item.getString("Kode_Material"),
                                                item.getString("Kd_Anak_Kd_Material"),
                                                item.getString("keteranganitem"),
                                                item.getString("Unit1"),
                                                item.getString("Unit2"),
                                                item.getString("Volume1"),
                                                item.getString("Volume2"),
                                                item.getString("Harga_Satuan_Org1"),
                                                item.getString("Harga_Satuan_Org2"),
                                                item.getString("Harga_Satuan_Std1"),
                                                item.getString("Harga_Satuan_Std2"),
                                                item.getString("Panjang_Material"),
                                                item.getString("Koefisien_Konversi"),
                                                item.getString("DeskripsiMerk"),
                                                item.getString("qtyAlreadyReceived"),
                                                item.getString("VolumeRencanaKirim"),
                                                item.getString("IdTdPo"),
                                                item.getString("TanggalRencanaKirimFormatEnglish"),
                                                item.getString("Kode_PO"),
                                                item.getString("TransactionType"),
                                                item.getString("spesifikasi")

                                        );
                                        listItems.add(itemEnts);
                                        Log.d("LogSpn","InitialData2BasedNpo"+result.toString());
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                }
                            }catch (JSONException e){
                                Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            return listItems;
        }

        public ArrayList SetNpoDataBase(String number, String tglDipilih, String pageup, String pagedown){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());
            String date = null;
            String sortBy = "";


            controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedNpo_Mobile1",
                    "@KodeProyek",session.getKodeProyek(),
                    "@currentpage",pageup,
                    "@pagesize",pagedown,
                    "@sortby","",
                    "@wherecond"," AND th.ID = '"+number+"' ",
                    // "@wherecond"," AND th.ID = '"+number+"' ",
                    "@nik",session.isNikUserLoggedIn(),
                    "@formid","PC.02.13",
                    "@zonaid","",
                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        AllItemsEnt itemEnts = new AllItemsEnt(item.getString("path"),
                                                item.getString("ID"),
                                                item.getString("Deskripsi"),
                                                item.getString("ordered_stock"),
                                                item.getString("received_stock"),
                                                item.getString("current_stock"),
                                                item.getString("unit"),
                                                item.getString("specification"),
                                                item.getString("Keterangan"),

                                                item.getString("ID_Th_PO"),
                                                item.getString("Kode_Material"),
                                                item.getString("Kd_Anak_Kd_Material"),
                                                item.getString("keteranganitem"),
                                                item.getString("Unit1"),
                                                item.getString("Unit2"),
                                                item.getString("Volume1"),
                                                item.getString("Volume2"),
                                                item.getString("Harga_Satuan_Org1"),
                                                item.getString("Harga_Satuan_Org2"),
                                                item.getString("Harga_Satuan_Std1"),
                                                item.getString("Harga_Satuan_Std2"),
                                                item.getString("Panjang_Material"),
                                                item.getString("Koefisien_Konversi"),
                                                item.getString("DeskripsiMerk"),
                                                item.getString("qtyAlreadyReceived"),
                                                item.getString("VolumeRencanaKirim"),
                                                item.getString("IdTdPo"),
                                                item.getString("TanggalRencanaKirimFormatEnglish"),
                                                item.getString("Kode_PO"),
                                                item.getString("TransactionType"),
                                                item.getString("spesifikasi")

                                        );
                                        listItems.add(itemEnts);
                                        Log.d("LogSpn","InitialData2BasedNpo"+result.toString());
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                }
                            }catch (JSONException e){
                                Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            return listItems;
        }

        @Override
        public int getContentItemsTotal() {
            //return expanded? listItems.size() : 0;
            return listItems.size();
        }

        @Override
        public RecyclerView.ViewHolder getItemViewHolder(View view) {
            return new ItemViewHolder(view);
        }

        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
            final ItemViewHolder itemHolder = (ItemViewHolder) holder;

            //dialog = new Dialog(ListPOActivity.this);

            String spesifikasi = listItems.get(position).getSpesifikasi().toString();
            if(listItems.get(position).getTransactionType().toString().equals("Kontrak"))
            {
                int speklength = spesifikasi.length();
                spesifikasi = spesifikasi.substring(0,speklength-1);
            }

            itemHolder.tvListItemName.setText(listItems.get(position).getDescription()+" ("+spesifikasi+")");
            if(listItems.get(position).getMerk().equals("")) {
                itemHolder.tvListItemBrand.setText("");
            }
            else
            {
                itemHolder.tvListItemBrand.setText(listItems.get(position).getMerk());
            }
            //String qtypo = listItems.get(position).getVolume1().replaceAll("([0-9])\\.0+([^0-9]|$)", "$1$2");
            itemHolder.tvListQtyPo.setText(listItems.get(position).getVolume1());
            itemHolder.tvListQtyReceived.setText(listItems.get(position).getCurrent_stock());
            itemHolder.tvQtyRcvNotRejected.setText(listItems.get(position).getCurrent_stock());
            itemHolder.txtIdTdPo.setText(listItems.get(position).getIdTdPo());
            itemHolder.txtTransType.setText(listItems.get(position).getTransactionType());
            itemHolder.txtTglDipilih.setText(listItems.get(position).getTanggalRencanaKirimFormatEnglish());
            itemHolder.txtIdThPo.setText(listItems.get(position).getKode_transaksi());
            if(listItems.get(position).getVolumeRencanaKirim().toString().equals(""))
            {
                itemHolder.tvQtyDR.setText("0");
            }
            else if(listItems.get(position).getVolumeRencanaKirim().toString().equals("null"))
            {
                itemHolder.tvQtyDR.setText("0");
            }
            else
            {
                itemHolder.tvQtyDR.setText(listItems.get(position).getVolumeRencanaKirim());
            }

            Log.d("LOGCurrentStock",listItems.get(position).getCurrent_stock());
            Log.d("LOGIdTdTrans",listItems.get(position).getIdTdPo());
            //itemHolder.imgItem.setImageBitmap(listItems.get(position).getImageUrlPath());

            Picasso.with(getApplicationContext())
                    .load(session.getUrlConfig()+AppConfig.URL_IMAGE_PREFIX+listItems.get(position).getImageUrlPath())
                    .placeholder(R.drawable.ic_insert_photo_black_48dp)
                    .error(R.drawable.ic_insert_photo_black_48dp)
                    .into(itemHolder.imgItem);
            //Jika image url kosong maka gambar default menjadi gray
            if(listItems.get(position).getImageUrlPath().equals("null")) {
                itemHolder.imgItem.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
            }

            if(listItems.get(position).getUnit1().equals("")) {
                itemHolder.tvListUnit.setText("");
            }
            else
            {
                itemHolder.tvListUnit.setText(listItems.get(position).getUnit1());
            }

            if(listItems.get(position).getUnit2().equals(""))
            {
                itemHolder.tvUnit2.setText("");
            }
            else {
                itemHolder.tvUnit2.setText(listItems.get(position).getUnit2());
            }

            itemHolder.ivCreate.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.yellow));

            //if(position == 0){
//                itemHolder.llRowPo.setBackgroundColor(Color.parseColor("#20000000"));
//            }

            itemHolder.imgItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // String.valueOf(v.getTag());
                   // Toast.makeText(getApplicationContext(), itemHolder.imgItem.getResources().toString(), Toast.LENGTH_LONG).show();
                }
            });

            String finalSpesifikasi = spesifikasi;
            itemHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("LogSpnAdp",String.valueOf(holder.getAdapterPosition()));

                    //GetIdTdTrans
                    String idtdtrans = itemHolder.txtIdTdPo.getText().toString();
                    String transtype = itemHolder.txtTransType.getText().toString();
                    String tglrencanakirim = itemHolder.txtTglDipilih.getText().toString();
                    String idthtrans = itemHolder.txtIdThPo.getText().toString();

                    //DIALOG INPUT RECEIVED QTY
                    //QTY MAX = QTY PO - QTY SPN BASED PO TSB
                    dInputTitle.setText(listItems.get(position).getDescription().toString()+" ("+ finalSpesifikasi +")");
                    tvUnit1Dialog.setText(itemHolder.tvUnit2.getText().toString());
                    tvUnit2Dialog.setText(itemHolder.tvListUnit.getText().toString());
                    double qtyMax = Double.parseDouble(itemHolder.tvListQtyPo.getText().toString()) - Double.parseDouble(itemHolder.tvQtyRcvNotRejected.getText().toString());
                    tvQtyBeforeDialog.setText(String.valueOf(qtyMax));
                    //jika kolom belum diisi maka tampilkan qty DR di pop up, else tampilkan ql yg sudah diinput sebelumnya
                    if(itemHolder.tvListQtyInput.getText().toString().equals("0"))
                    {

                        //jika dr != null || dr != "" maka tampilkan qtydr, else tampilkan 0
                        if(listItems.get(position).getVolumeRencanaKirim().toString().equals(""))
                        {
                            edQtyReceive.setText("0");
                        }
                        else if(listItems.get(position).getVolumeRencanaKirim().toString().equals("null"))
                        {
                            edQtyReceive.setText("0");
                        }
                        else
                        {
                            edQtyReceive.setText(listItems.get(position).getVolumeRencanaKirim());
                        }
                    }
                    else
                    {
                        edQtyReceive.setText(itemHolder.tvListQtyInput.getText().toString());
                    }
                    buttonDialogUp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(!edQtyReceive.getText().toString().equals(""))
                            {
                                if(edQtyReceive.getText().toString().equals(tvQtyBeforeDialog.getText().toString()))
                                {
                                    edQtyReceive.setText(tvQtyBeforeDialog.getText().toString());
                                }
                                else
                                {
                                    float value = Float.parseFloat(edQtyReceive.getText().toString());
                                    float resultvalue = value + 1;
                                    edQtyReceive.setText(String.valueOf(resultvalue));
                                }
                            }
                            else
                                {
                                edQtyReceive.setText("0");
                            }

                        }
                    });

                    buttonDialogDown.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(!edQtyReceive.getText().toString().equals(""))
                            {
                                if (edQtyReceive.getText().toString().equals("1.0")) {
                                    edQtyReceive.setText("1.0");
                                } else {
                                    float value = Float.parseFloat(edQtyReceive.getText().toString());
                                    float resultvalue = value - 1;
                                    edQtyReceive.setText(String.valueOf(resultvalue));
                                }
                            }
                            else {
                                edQtyReceive.setText("0");
                            }
                        }
                    });

                    btnDialogInputCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    btnDialogInputSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            if(transtype.equals("PO")) {
//                              gtvReadyStockQtyChecker(idthtrans, tglrencanakirim, idtdtrans);

                                session = new SessionManager(getApplicationContext());
                                controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedPo1",
                                        "@KodeProyek",session.getKodeProyek(),
                                        "@currentpage","1",
                                        "@pagesize","10",
                                        "@sortby","",
                                        "@wherecond","AND th.ID = '"+idthtrans+"' AND td.ID = '"+idtdtrans+"' ",
                                        "@nik",session.isNikUserLoggedIn(),
                                        "@formid","PC.02.13",
                                        "@zonaid","",
                                        new VolleyCallback() {
                                            @Override
                                            public void onSave(String output) {

                                            }

                                            @Override
                                            public void onSuccess(JSONArray result) {
                                                try {
                                                    if (result.length() > 0) {

                                                        for (int i = 0; i < result.length(); i++) {
                                                            item = result.getJSONObject(i);
                                                            latestvol = item.getString("current_stock");
                                                        }

                                                        float readystocknow = Float.parseFloat(latestvol);

                                                        //JIKA QTY RECEIVED ADALAH ANGKA
                                                        if(edQtyReceive.getText().toString().matches("\\d+(?:\\.\\d+)?") && !edQtyReceive.getText().toString().equals("0.0") && !edQtyReceive.getText().toString().equals("0"))
                                                        {
                                                            if(!etDNoKendaraan.getText().toString().equals(""))
                                                            {
                                                                float qtyReceived = Float.parseFloat(edQtyReceive.getText().toString());
                                                                float limitInput = Float.parseFloat(itemHolder.tvListQtyPo.getText().toString()) - readystocknow;
//                                                                if (qtyReceived <= qtyMax) {
                                                                if (qtyReceived <= limitInput ) {
                                                                    itemHolder.llRowPo.setBackgroundColor(Color.parseColor("#405f87c7"));

                                                                    preparePostSPN(etNoSuratJalan.getText().toString(), etKeterangan.getText().toString(), etDNoKendaraan.getText().toString(), String.valueOf(holder.getAdapterPosition()));
                                                                    Gson gsonDetail = new Gson();
                                                                    String jsonString = gsonDetail.toJson(listBeforePostSpnDetil);
                                                                    session.setQtyReceivedArrayListSPN(jsonString.toString());
                                                                    Log.d("SPNLogs : ", jsonString.toString());
                                                                    // Toast.makeText(ListPOActivity.this, "Qty Input Success!"+ jsonString.toString(), Toast.LENGTH_LONG).show();
                                                                    dialog.dismiss();
                                                                    itemHolder.tvListQtyInput.setText(edQtyReceive.getText().toString());
                                                                    itemHolder.txtNoPlat.setText(etDNoKendaraan.getText().toString());
                                                                } else {
                                                                    Toast.makeText(ListPOActivity.this, "Qty Do Receive must <= Qty Received Updated Latest ("+limitInput+")", Toast.LENGTH_LONG).show();
                                                                    edQtyReceive.requestFocus();
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Toast.makeText(ListPOActivity.this, "No Plat Required!", Toast.LENGTH_LONG).show();
                                                                etDNoKendaraan.requestFocus();
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Toast.makeText(ListPOActivity.this, "Qty Receive must Number, Not Empty and > 0!", Toast.LENGTH_LONG).show();
                                                            edQtyReceive.requestFocus();
                                                        }

                                                    }
                                                    else
                                                    {
                                                        latestvol = "0";
                                                        Toast.makeText(ListPOActivity.this, "Check Qty Received Latest Fail!", Toast.LENGTH_LONG).show();
                                                    }
                                                    Log.d("EZRAgtvReadyStockQty", latestvol);
                                                }catch (JSONException e){
                                                    Toast.makeText(getApplicationContext(), "CATCH gtvReadyStockQty", Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });

                            }
                            else if(transtype.equals("Kontrak PO")) {
//                              gtvReadyStockQtyChecker(idthtrans, tglrencanakirim, idtdtrans);

                                session = new SessionManager(getApplicationContext());
                                controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedPo1",
                                        "@KodeProyek",session.getKodeProyek(),
                                        "@currentpage","1",
                                        "@pagesize","10",
                                        "@sortby","",
                                        "@wherecond","AND th.ID = '"+idthtrans+"' AND td.ID = '"+idtdtrans+"' ",
                                        "@nik",session.isNikUserLoggedIn(),
                                        "@formid","PC.02.13",
                                        "@zonaid","",
                                        new VolleyCallback() {
                                            @Override
                                            public void onSave(String output) {

                                            }

                                            @Override
                                            public void onSuccess(JSONArray result) {
                                                try {
                                                    if (result.length() > 0) {

                                                        for (int i = 0; i < result.length(); i++) {
                                                            item = result.getJSONObject(i);
                                                            latestvol = item.getString("current_stock");
                                                        }

                                                        float readystocknow = Float.parseFloat(latestvol);

                                                        //JIKA QTY RECEIVED ADALAH ANGKA
                                                        if(edQtyReceive.getText().toString().matches("\\d+(?:\\.\\d+)?") && !edQtyReceive.getText().toString().equals("0.0") && !edQtyReceive.getText().toString().equals("0"))
                                                        {
                                                            if(!etDNoKendaraan.getText().toString().equals(""))
                                                            {
                                                                float qtyReceived = Float.parseFloat(edQtyReceive.getText().toString());
                                                                float limitInput = Float.parseFloat(itemHolder.tvListQtyPo.getText().toString()) - readystocknow;
//                                                                if (qtyReceived <= qtyMax) {
                                                                if (qtyReceived <= limitInput ) {
                                                                    itemHolder.llRowPo.setBackgroundColor(Color.parseColor("#405f87c7"));

                                                                    preparePostSPN(etNoSuratJalan.getText().toString(), etKeterangan.getText().toString(), etDNoKendaraan.getText().toString(), String.valueOf(holder.getAdapterPosition()));
                                                                    Gson gsonDetail = new Gson();
                                                                    String jsonString = gsonDetail.toJson(listBeforePostSpnDetil);
                                                                    session.setQtyReceivedArrayListSPN(jsonString.toString());
                                                                    Log.d("SPNLogs : ", jsonString.toString());
                                                                    // Toast.makeText(ListPOActivity.this, "Qty Input Success!"+ jsonString.toString(), Toast.LENGTH_LONG).show();
                                                                    dialog.dismiss();
                                                                    itemHolder.tvListQtyInput.setText(edQtyReceive.getText().toString());
                                                                    itemHolder.txtNoPlat.setText(etDNoKendaraan.getText().toString());
                                                                } else {
                                                                    Toast.makeText(ListPOActivity.this, "Qty Do Receive must <= Qty Received Updated Latest ("+limitInput+")", Toast.LENGTH_LONG).show();
                                                                    edQtyReceive.requestFocus();
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Toast.makeText(ListPOActivity.this, "No Plat Required!", Toast.LENGTH_LONG).show();
                                                                etDNoKendaraan.requestFocus();
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Toast.makeText(ListPOActivity.this, "Qty Receive must Number, Not Empty and > 0!", Toast.LENGTH_LONG).show();
                                                            edQtyReceive.requestFocus();
                                                        }

                                                    }
                                                    else
                                                    {
                                                        latestvol = "0";
                                                        Toast.makeText(ListPOActivity.this, "Check Qty Received Latest Fail!", Toast.LENGTH_LONG).show();
                                                    }
                                                    Log.d("EZRAgtvReadyStockQty", latestvol);
                                                }catch (JSONException e){
                                                    Toast.makeText(getApplicationContext(), "CATCH gtvReadyStockQty", Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });

                            }
                            else if(transtype.equals("Kontrak")) {

                                session = new SessionManager(getApplicationContext());
                                controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedKontrak_Mobile1",
                                        "@KodeProyek",session.getKodeProyek(),
                                        "@currentpage","1",
                                        "@pagesize","10",
                                        "@sortby","",
                                        "@wherecond"," AND th.ID = '"+idthtrans+"' AND td.ID = '"+idtdtrans+"' ",
                                        "@nik",session.isNikUserLoggedIn(),
                                        "@formid","PC.02.13",
                                        "@zonaid","",
                                        new VolleyCallback() {
                                            @Override
                                            public void onSave(String output) {

                                            }

                                            @Override
                                            public void onSuccess(JSONArray result) {
                                                try {
                                                    if (result.length() > 0)
                                                    {

                                                        for (int i = 0; i < result.length(); i++) {
                                                            item = result.getJSONObject(i);
                                                            latestvol = item.getString("current_stock");
                                                        }

                                                        float readystocknow = Float.parseFloat(latestvol);

                                                        //JIKA QTY RECEIVED ADALAH ANGKA
                                                        if(edQtyReceive.getText().toString().matches("\\d+(?:\\.\\d+)?") && !edQtyReceive.getText().toString().equals("0.0") && !edQtyReceive.getText().toString().equals("0"))
                                                        {
                                                            if(!etDNoKendaraan.getText().toString().equals(""))
                                                            {
                                                                float qtyReceived = Float.parseFloat(edQtyReceive.getText().toString());
                                                                float limitInput = Float.parseFloat(itemHolder.tvListQtyPo.getText().toString()) - readystocknow;
//                                                                if (qtyReceived <= qtyMax) {
                                                                if (qtyReceived <= limitInput ) {
                                                                    itemHolder.llRowPo.setBackgroundColor(Color.parseColor("#405f87c7"));

                                                                    preparePostSPN(etNoSuratJalan.getText().toString(), etKeterangan.getText().toString(), etDNoKendaraan.getText().toString(), String.valueOf(holder.getAdapterPosition()));

                                                                    Gson gsonDetail = new Gson();
                                                                    String jsonString = gsonDetail.toJson(listBeforePostSpnDetil);
                                                                    session.setQtyReceivedArrayListSPN(jsonString.toString());
                                                                    Log.d("SPNLogs : ", jsonString.toString());
                                                                    // Toast.makeText(ListPOActivity.this, "Qty Input Success!"+ jsonString.toString(), Toast.LENGTH_LONG).show();
                                                                    dialog.dismiss();
                                                                    itemHolder.tvListQtyInput.setText(edQtyReceive.getText().toString());
                                                                    itemHolder.txtNoPlat.setText(etDNoKendaraan.getText().toString());
                                                                } else {
                                                                    Toast.makeText(ListPOActivity.this, "Qty Do Receive must <= Qty Received Updated Latest ("+limitInput+")", Toast.LENGTH_LONG).show();
                                                                    edQtyReceive.requestFocus();
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Toast.makeText(ListPOActivity.this, "No Plat Required!", Toast.LENGTH_LONG).show();
                                                                etDNoKendaraan.requestFocus();
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Toast.makeText(ListPOActivity.this, "Qty Receive must Number, Not Empty and > 0!", Toast.LENGTH_LONG).show();
                                                            edQtyReceive.requestFocus();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        latestvol = "0";
                                                        Toast.makeText(ListPOActivity.this, "Check Qty Received Latest Fail!", Toast.LENGTH_LONG).show();
                                                    }
                                                    Log.d("EZRAgtvReadyStockQty", latestvol);
                                                }catch (JSONException e){
                                                    Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });
                            }
//                            else if(transtype.equals("KONTRAKNODR")) {
//
//                                session = new SessionManager(getApplicationContext());
//                                controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedKontrakWithoutDr_Mobile1",
//                                        "@KodeProyek",session.getKodeProyek(),
//                                        "@currentpage","1",
//                                        "@pagesize","10",
//                                        "@sortby","",
//                                        "@wherecond"," AND th.ID = '"+idthtrans+"' AND td.ID = '"+idtdtrans+"' ",
//                                        "@nik",session.isNikUserLoggedIn(),
//                                        "@formid","PC.02.13",
//                                        "@zonaid","",
//                                        new VolleyCallback() {
//                                            @Override
//                                            public void onSave(String output) {
//
//                                            }
//
//                                            @Override
//                                            public void onSuccess(JSONArray result) {
//                                                try {
//                                                    if (result.length() > 0)
//                                                    {
//
//                                                        for (int i = 0; i < result.length(); i++) {
//                                                            item = result.getJSONObject(i);
//                                                            latestvol = item.getString("current_stock");
//                                                        }
//
//                                                        float readystocknow = Float.parseFloat(latestvol);
//
//                                                        //JIKA QTY RECEIVED ADALAH ANGKA
//                                                        if(edQtyReceive.getText().toString().matches("\\d+(?:\\.\\d+)?") && !edQtyReceive.getText().toString().equals("0.0") && !edQtyReceive.getText().toString().equals("0"))
//                                                        {
//                                                            if(!etDNoKendaraan.getText().toString().equals(""))
//                                                            {
//                                                                float qtyReceived = Float.parseFloat(edQtyReceive.getText().toString());
//                                                                float limitInput = Float.parseFloat(itemHolder.tvListQtyPo.getText().toString()) - readystocknow;
////                                                                if (qtyReceived <= qtyMax) {
//                                                                if (qtyReceived <= limitInput ) {
//                                                                    itemHolder.llRowPo.setBackgroundColor(Color.parseColor("#405f87c7"));
//
//                                                                    preparePostSPN(etNoSuratJalan.getText().toString(), etKeterangan.getText().toString(), etDNoKendaraan.getText().toString());
//                                                                    Gson gsonDetail = new Gson();
//                                                                    String jsonString = gsonDetail.toJson(listBeforePostSpnDetil);
//                                                                    session.setQtyReceivedArrayListSPN(jsonString.toString());
//                                                                    Log.d("SPNLogs : ", jsonString.toString());
//                                                                    // Toast.makeText(ListPOActivity.this, "Qty Input Success!"+ jsonString.toString(), Toast.LENGTH_LONG).show();
//                                                                    dialog.dismiss();
//                                                                    itemHolder.tvListQtyInput.setText(edQtyReceive.getText().toString());
//                                                                    itemHolder.txtNoPlat.setText(etDNoKendaraan.getText().toString());
//                                                                } else {
//                                                                    Toast.makeText(ListPOActivity.this, "Qty Do Receive must <= Qty Received Updated Latest ("+limitInput+")", Toast.LENGTH_LONG).show();
//                                                                    edQtyReceive.requestFocus();
//                                                                }
//                                                            }
//                                                            else
//                                                            {
//                                                                Toast.makeText(ListPOActivity.this, "No Plat Required!", Toast.LENGTH_LONG).show();
//                                                                etDNoKendaraan.requestFocus();
//                                                            }
//                                                        }
//                                                        else
//                                                        {
//                                                            Toast.makeText(ListPOActivity.this, "Qty Receive must Number, Not Empty and > 0!", Toast.LENGTH_LONG).show();
//                                                            edQtyReceive.requestFocus();
//                                                        }
//                                                    }
//                                                    else
//                                                    {
//                                                        latestvol = "0";
//                                                        Toast.makeText(ListPOActivity.this, "Check Qty Received Latest Fail!", Toast.LENGTH_LONG).show();
//                                                    }
//                                                    Log.d("EZRAgtvReadyStockQty", latestvol);
//                                                }catch (JSONException e){
//                                                    Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
//                                                }
//                                            }
//                                        });
//                            }
                            else
                            {
                                session = new SessionManager(getApplicationContext());
                                controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedNpo_Mobile1",
                                        "@KodeProyek",session.getKodeProyek(),
                                        "@currentpage","1",
                                        "@pagesize","10",
                                        "@sortby","",
                                        "@wherecond"," AND th.ID = '"+idthtrans+"' AND td.ID = '"+idtdtrans+"' ",
                                        "@nik",session.isNikUserLoggedIn(),
                                        "@formid","PC.02.13",
                                        "@zonaid","",
                                        new VolleyCallback() {
                                            @Override
                                            public void onSave(String output) {

                                            }

                                            @Override
                                            public void onSuccess(JSONArray result) {
                                                try {
                                                    if (result.length() > 0)
                                                    {
                                                        for (int i = 0; i < result.length(); i++) {
                                                            item = result.getJSONObject(i);
                                                            latestvol = item.getString("current_stock");
                                                        }

                                                        float readystocknow = Float.parseFloat(latestvol);

                                                        //JIKA QTY RECEIVED ADALAH ANGKA
                                                        if(edQtyReceive.getText().toString().matches("\\d+(?:\\.\\d+)?") && !edQtyReceive.getText().toString().equals("0.0") && !edQtyReceive.getText().toString().equals("0"))
                                                        {
                                                            if(!etDNoKendaraan.getText().toString().equals(""))
                                                            {
                                                                float qtyReceived = Float.parseFloat(edQtyReceive.getText().toString());
                                                                float limitInput = Float.parseFloat(itemHolder.tvListQtyPo.getText().toString()) - readystocknow;
//                                                                if (qtyReceived <= qtyMax) {
                                                                if (qtyReceived <= limitInput ) {
                                                                    itemHolder.llRowPo.setBackgroundColor(Color.parseColor("#405f87c7"));

                                                                    preparePostSPN(etNoSuratJalan.getText().toString(), etKeterangan.getText().toString(), etDNoKendaraan.getText().toString(), String.valueOf(holder.getAdapterPosition()));
                                                                    Gson gsonDetail = new Gson();
                                                                    String jsonString = gsonDetail.toJson(listBeforePostSpnDetil);
                                                                    session.setQtyReceivedArrayListSPN(jsonString.toString());
                                                                    Log.d("SPNLogs : ", jsonString.toString());
                                                                    // Toast.makeText(ListPOActivity.this, "Qty Input Success!"+ jsonString.toString(), Toast.LENGTH_LONG).show();
                                                                    dialog.dismiss();
                                                                    itemHolder.tvListQtyInput.setText(edQtyReceive.getText().toString());
                                                                    itemHolder.txtNoPlat.setText(etDNoKendaraan.getText().toString());
                                                                } else {
                                                                    Toast.makeText(ListPOActivity.this, "Qty Do Receive must <= Qty Received Updated Latest ("+limitInput+")", Toast.LENGTH_LONG).show();
                                                                    edQtyReceive.requestFocus();
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Toast.makeText(ListPOActivity.this, "No Plat Required!", Toast.LENGTH_LONG).show();
                                                                etDNoKendaraan.requestFocus();
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Toast.makeText(ListPOActivity.this, "Qty Receive must Number, Not Empty and > 0!", Toast.LENGTH_LONG).show();
                                                            edQtyReceive.requestFocus();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        latestvol = "0";
                                                        Toast.makeText(ListPOActivity.this, "Check Qty Received Latest Fail!", Toast.LENGTH_LONG).show();
                                                    }
                                                }catch (JSONException e){
                                                    Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });
                            }

//                            Log.d("LOGIdthtrans : ", idthtrans);
//                            Log.d("LOGIdtdtrans : ", idtdtrans);
                            //Log.d("LOGMaxQtyNow : ", maxQtyNow);
                            //JIKA QTY RECEIVED ADALAH ANGKA
//                            if(edQtyReceive.getText().toString().matches("\\d+(?:\\.\\d+)?") && !edQtyReceive.getText().toString().equals("0.0") && !edQtyReceive.getText().toString().equals("0"))
//                            {
//                                if(!etDNoKendaraan.getText().toString().equals(""))
//                                {
//                                    float qtyReceived = Float.parseFloat(edQtyReceive.getText().toString());
//                                    if (qtyReceived <= qtyMax) {
////                                    if (qtyReceived <= maxQtyNow[0]) {
//                                        itemHolder.llRowPo.setBackgroundColor(Color.parseColor("#405f87c7"));
//
//                                        preparePostSPN(etNoSuratJalan.getText().toString(), etKeterangan.getText().toString(), etDNoKendaraan.getText().toString());
//                                        Gson gsonDetail = new Gson();
//                                        String jsonString = gsonDetail.toJson(listBeforePostSpnDetil);
//                                        session.setQtyReceivedArrayListSPN(jsonString.toString());
//                                        Log.d("SPNLogs : ", jsonString.toString());
//                                        // Toast.makeText(ListPOActivity.this, "Qty Input Success!"+ jsonString.toString(), Toast.LENGTH_LONG).show();
//                                        dialog.dismiss();
//                                        itemHolder.tvListQtyInput.setText(edQtyReceive.getText().toString());
//                                        itemHolder.txtNoPlat.setText(etDNoKendaraan.getText().toString());
//                                    } else {
//                                        Toast.makeText(ListPOActivity.this, "Qty Do Receive must <= Qty Received ("+maxQtyNow[0]+")", Toast.LENGTH_LONG).show();
//                                        edQtyReceive.requestFocus();
//                                    }
//                                }
//                                else
//                                {
//                                    Toast.makeText(ListPOActivity.this, "No Plat Required!", Toast.LENGTH_LONG).show();
//                                    etDNoKendaraan.requestFocus();
//                                }
//                            }
//                            else
//                            {
//                                Toast.makeText(ListPOActivity.this, "Qty Receive must Number, Not Empty and > 0!", Toast.LENGTH_LONG).show();
//                                edQtyReceive.requestFocus();
//                            }
                        }
                    });

                    session.setItemInfo(listItems.get(position).getMaterialName(),
                            listItems.get(position).getDescription(),
                            listItems.get(position).getImageUrlPath(),
                            listItems.get(position).getCurrent_stock(),
                            listItems.get(position).getOrdered_stock(),
                            listItems.get(position).getReceived(),
                            listItems.get(position).getSpec(),
                            listItems.get(position).getUnit(),
                            listItems.get(position).getKeterangan(),

                            listItems.get(position).getKode_transaksi(),
                            listItems.get(position).getKode_material(),
                            listItems.get(position).getKodeanak_kodematerial(),
                            listItems.get(position).getKeteranganpo(),
                            listItems.get(position).getUnit1(),
                            listItems.get(position).getUnit2(),
                            listItems.get(position).getVolume1(),
                            listItems.get(position).getVolume2(),
                            listItems.get(position).getHarga_satuan_org1(),
                            listItems.get(position).getHarga_satuan_org2(),
                            listItems.get(position).getHarga_satuan_std1(),
                            listItems.get(position).getHarga_satuan_std2(),
                            listItems.get(position).getPanjang_material(),
                            listItems.get(position).getKoefisien_konversi(),
                            listItems.get(position).getIdTdPo(),
                            listItems.get(position).getMaterialName(),
                            listItems.get(position).getKodeThTransaction(),
                            listItems.get(position).getTransactionType(),
                            listItems.get(position).getSpesifikasi(),
                            String.valueOf(holder.getAdapterPosition())
                    );

                    dialog.show();
                }
            });

        }

        @Override
        public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
            return new HeaderViewHolder(view);
        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;

            //if(headerHolder.tvTitle.getText().equals(tgldipilih)) {
            headerHolder.tvTitle.setText(title);
            headerHolder.tvIdTrans.setText(titleTransNo);
            headerHolder.tvTgl.setText(titleTgl);
            headerHolder.titleTypeTrans.setText(titleTypeTrans);
            headerHolder.titleIndex.setText(titleIndex);
            //}
            //else
            //{
//                headerHolder.tvTitle.setText(title);
//            }

        }

        @Override
        public RecyclerView.ViewHolder getFooterViewHolder(View view) {
            return new FooterViewHolder(view);
        }

        @Override
        public void onBindFooterViewHolder(RecyclerView.ViewHolder holder) {
            FooterViewHolder footerHolder = (FooterViewHolder) holder;

            footerHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if(titleTgl.equals("null"))
                    {
                        titleTgl = "";
                    }

                    //Toast.makeText(getApplicationContext(), title+titleTransNo+titleTgl, Toast.LENGTH_SHORT).show();

                    if(titleIndex.equals("0")) {
                        if (titleTypeTrans.equals("PO")) {
                            sectionOneUp0 = 1+sectionOneUp0;
                            listItems = SetPoData(titleTransNo, titleTgl, String.valueOf(sectionOneUp0), String.valueOf("10"));
                        } else if (titleTypeTrans.equals("Kontrak")) {
                            sectionTwoUp0 = 1+sectionTwoUp0;
                            listItems = SetKontrakDataBase(titleTransNo, titleTgl, String.valueOf(sectionTwoUp0), String.valueOf("10"));
                        }
//                        else if (titleTypeTrans.equals("KONTRAKNODR")) {
//                            sectionThreeUp0 = 1+sectionThreeUp0;
//                            listItems = SetKontrakDataNoDr(titleTransNo, titleTgl, String.valueOf(sectionThreeUp0), String.valueOf("10"));
//                        }
                        else {
                            sectionFourUp0 = 1+sectionFourUp0;
                            listItems = SetNpoData(titleTransNo, titleTgl, String.valueOf(sectionFourUp0), String.valueOf("10"));
                        }
                    }
                    else
                    {
                        if(titleTypeTrans.equals("PO")) {
                            sectionOneUp1 = 1+sectionOneUp1;
                            listItems = SetPoDataBase(titleTransNo, titleTgl, String.valueOf(sectionOneUp1), String.valueOf("10"));
                        }
                        else if(titleTypeTrans.equals("Kontrak")) {
                            sectionTwoUp1 = 1+sectionTwoUp1;
                            listItems = SetKontrakDataBase(titleTransNo, titleTgl, String.valueOf(sectionTwoUp1), String.valueOf("10"));
                        }
//                        else if(titleTypeTrans.equals("KONTRAKNODR")) {
//
//                            sectionThreeUp1 = 1+sectionThreeUp1;
//                            Log.d("LogPage", String.valueOf(sectionThreeUp1));
//                            listItems = SetKontrakDataNoDr(titleTransNo, titleTgl, String.valueOf(sectionThreeUp1), String.valueOf("10"));
//                        }
                        else
                        {
                            sectionFourUp1 = 1+sectionFourUp1;
                            listItems = SetNpoDataBase(titleTransNo, titleTgl, String.valueOf(sectionFourUp1), String.valueOf("10"));
                        }
                    }
                }
            });
        }
    }

    public ArrayList SetPoDataInitialData(String number, String tglDipilih){
        session = new SessionManager(getApplicationContext());
        String date = null;
        String sortBy = "";

        controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedPo1",
                "@KodeProyek",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond","AND CONVERT(VARCHAR,tdDR.TanggalRencanaKirim,105) = '"+tglDipilih+"' AND th.ID = '"+number+"' ",
                "@nik",session.isNikUserLoggedIn(),
                "@formid","PC.02.13",
                "@zonaid","",
        new VolleyCallback() {
            @Override
            public void onSave(String output) {

            }

            @Override
            public void onSuccess(JSONArray result) {
                try {
                    if (result.length() > 0) {
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject item = result.getJSONObject(i);

                            AllItemsEnt itemEnts = new AllItemsEnt(item.getString("path"),
                                    item.getString("ID"),
                                    item.getString("Deskripsi"),
                                    item.getString("ordered_stock"),
                                    item.getString("received_stock"),
                                    item.getString("current_stock"),
                                    item.getString("unit"),
                                    item.getString("specification"),
                                    item.getString("Keterangan"),

                                    item.getString("ID_Th_PO"),
                                    item.getString("Kode_Material"),
                                    item.getString("Kd_Anak_Kd_Material"),
                                    item.getString("keteranganitem"),
                                    item.getString("Unit1"),
                                    item.getString("Unit2"),
                                    item.getString("Volume1"),
                                    item.getString("Volume2"),
                                    item.getString("Harga_Satuan_Org1"),
                                    item.getString("Harga_Satuan_Org2"),
                                    item.getString("Harga_Satuan_Std1"),
                                    item.getString("Harga_Satuan_Std2"),
                                    item.getString("Panjang_Material"),
                                    item.getString("Koefisien_Konversi"),
                                    item.getString("DeskripsiMerk"),
                                    item.getString("qtyAlreadyReceived"),
                                    item.getString("VolumeRencanaKirim"),
                                    item.getString("IdTdPo"),
                                    item.getString("TanggalRencanaKirimFormatEnglish"),
                                    item.getString("Kode_PO"),
                                    item.getString("TransactionType"),
                                    item.getString("spesifikasi")

                            );
                            Log.d("LogSpn","InitialDataBasedPo"+result.toString());
                            listItems.add(itemEnts);
                        }
                        sectionAdapter.notifyDataSetChanged();
                    }
                }catch (JSONException e){
                    Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                }
            }
        });
        return listItems;
    }

    public String gtvReadyStockQtyChecker(String number, String tglDipilih, String idtdtrans){
        //Log.d("material", material);


        return (latestvol);
    }

    public float[] SetPoDataInitialDataChecker(String number, String tglDipilih, String idtdtrans){
        session = new SessionManager(getApplicationContext());
        String date = null;
        String sortBy = "";
        final float[] paramuse = {0};

        controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedPo1",
                "@KodeProyek",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond","AND tdDR.TanggalRencanaKirim = '"+tglDipilih+"' AND th.ID = '"+number+"' AND td.ID = '"+idtdtrans+"' ",
                "@nik",session.isNikUserLoggedIn(),
                "@formid","PC.02.13",
                "@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {

                                    JSONObject item = result.getJSONObject(0);

                                paramuse[0] = Integer.parseInt(item.getString("current_stock"));

                                    Log.d("LOGPoChecker",result.toString());

                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        return paramuse;
    }

    public float[] SetPoDataInitialDataKontrakChecker(String number, String tglDipilih, String idtdtrans){
        session = new SessionManager(getApplicationContext());
        String date = null;
        String sortBy = "";
        final float[] paramuse = {0};

        controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedKontrak_Mobile1",
                "@KodeProyek",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond","AND tdDR.TanggalRencanaKirim = '"+tglDipilih+"' AND th.ID = '"+number+"' AND td.ID = '"+idtdtrans+"' ",
                "@nik",session.isNikUserLoggedIn(),
                "@formid","PC.02.13",
                "@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {

                                    JSONObject item = result.getJSONObject(0);

                                    paramuse[0] = Integer.parseInt(item.getString("current_stock"));

                                    Log.d("LOGKontrakChecker",result.toString());
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        return paramuse;
    }

    public float[] SetPoDataInitialDataNpoChecker(String number, String idtdtrans){
        session = new SessionManager(getApplicationContext());
        String date = null;
        String sortBy = "";
        final float[] paramuse = {0};

        controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedNpo_Mobile1",
                "@KodeProyek",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond"," AND th.ID = '"+number+"' AND td.ID = '"+idtdtrans+"' ",
                "@nik",session.isNikUserLoggedIn(),
                "@formid","PC.02.13",
                "@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                JSONObject item = result.getJSONObject(0);

                                paramuse[0] = Integer.parseInt(item.getString("current_stock"));

                                Log.d("LOGNpoChecker", String.valueOf(paramuse[0]));
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        return paramuse;
    }

    public ArrayList SetPoDataInitialDataKontrak(String number, String tglDipilih){
        session = new SessionManager(getApplicationContext());
        String date = null;
        String sortBy = "";


        controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedKontrak_Mobile1",
                "@KodeProyek",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond","AND CONVERT(VARCHAR,tdDR.TanggalRencanaKirim,105) = '"+tglDipilih+"' AND th.ID = '"+number+"' ",
                "@nik",session.isNikUserLoggedIn(),
                "@formid","PC.02.13",
                "@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);

                                    AllItemsEnt itemEnts = new AllItemsEnt(item.getString("path"),
                                            item.getString("ID"),
                                            item.getString("Deskripsi"),
                                            item.getString("ordered_stock"),
                                            item.getString("received_stock"),
                                            item.getString("current_stock"),
                                            item.getString("unit"),
                                            item.getString("specification"),
                                            item.getString("Keterangan"),

                                            item.getString("ID_Th_PO"),
                                            item.getString("Kode_Material"),
                                            item.getString("Kd_Anak_Kd_Material"),
                                            "",
                                            item.getString("Unit1"),
                                            item.getString("Unit2"),
                                            item.getString("Volume1"),
                                            item.getString("Volume2"),
                                            item.getString("Harga_Satuan_Org1"),
                                            item.getString("Harga_Satuan_Org2"),
                                            item.getString("Harga_Satuan_Std1"),
                                            item.getString("Harga_Satuan_Std2"),
                                            item.getString("Panjang_Material"),
                                            item.getString("Koefisien_Konversi"),
                                            item.getString("DeskripsiMerk"),
                                            item.getString("qtyAlreadyReceived"),
                                            item.getString("VolumeRencanaKirim"),
                                            item.getString("IdTdPo"),
                                            item.getString("TanggalRencanaKirimFormatEnglish"),
                                            item.getString("Kode_PO"),
                                            item.getString("TransactionType"),
                                            item.getString("spesifikasi")

                                    );

                                    Log.d("LogSpn","InitialDataBasedKontrak"+result.toString());
                                    listItems.add(itemEnts);
                                }
                                sectionAdapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        return listItems;
    }

    public ArrayList SetPoDataInitialDataNpo(String number, String tglDipilih){
        session = new SessionManager(getApplicationContext());
        String date = null;
        String sortBy = "";


        controller.InqGeneralPagingFullEzra(getApplicationContext(),"Get_Upcoming_List_Items_BasedNpo_Mobile1",
                "@KodeProyek",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","25",
                "@sortby","",
                "@wherecond"," AND th.ID = '"+number+"' ",
                "@nik",session.isNikUserLoggedIn(),
                "@formid","PC.02.13",
                "@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);

                                    AllItemsEnt itemEnts = new AllItemsEnt(item.getString("path"),
                                            item.getString("ID"),
                                            item.getString("Deskripsi"),
                                            item.getString("ordered_stock"),
                                            item.getString("received_stock"),
                                            item.getString("current_stock"),
                                            item.getString("unit"),
                                            item.getString("specification"),
                                            item.getString("Keterangan"),

                                            item.getString("ID_Th_PO"),
                                            item.getString("Kode_Material"),
                                            item.getString("Kd_Anak_Kd_Material"),
                                            "",
                                            item.getString("Unit1"),
                                            item.getString("Unit2"),
                                            item.getString("Volume1"),
                                            item.getString("Volume2"),
                                            item.getString("Harga_Satuan_Org1"),
                                            item.getString("Harga_Satuan_Org2"),
                                            item.getString("Harga_Satuan_Std1"),
                                            item.getString("Harga_Satuan_Std2"),
                                            item.getString("Panjang_Material"),
                                            item.getString("Koefisien_Konversi"),
                                            item.getString("DeskripsiMerk"),
                                            item.getString("qtyAlreadyReceived"),
                                            item.getString("VolumeRencanaKirim"),
                                            item.getString("IdTdPo"),
                                            item.getString("TanggalRencanaKirimFormatEnglish"),
                                            item.getString("Kode_PO"),
                                            item.getString("TransactionType"),
                                            item.getString("spesifikasi")

                                    );

                                    Log.d("LogSpn","InitialDataBasedNpo"+result.toString());
                                    listItems.add(itemEnts);
                                }
                                sectionAdapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        return listItems;
    }

    private void preparePostSPN(String nosuratjalan, String noketerangan, String nokendaraan, String position){

        Log.d("LogSpnAdp2",position);
        Gson gsonDetail = new Gson();
        SPNInsertItemQtyReceivedDetil spnInsertItemQtyReceivedDetil = new SPNInsertItemQtyReceivedDetil();
        spnInsertItemQtyReceivedDetil.setID("0");
        spnInsertItemQtyReceivedDetil.setTipe_Transaksi(session.getTransactionType());
        spnInsertItemQtyReceivedDetil.setKode_Transaksi(session.getIdPO());
        spnInsertItemQtyReceivedDetil.setNo_Urut("0");
        spnInsertItemQtyReceivedDetil.setKode_Aktivitas("0");
        spnInsertItemQtyReceivedDetil.setKode_Material(session.getItemKodeMaterial());
        spnInsertItemQtyReceivedDetil.setKd_Anak_Kd_Material(session.getItemKodeAnakKodeMaterial());
        spnInsertItemQtyReceivedDetil.setKeterangan(session.getItemKeteranganPo());
        spnInsertItemQtyReceivedDetil.setUnit1(session.getItemUnit1());
        spnInsertItemQtyReceivedDetil.setUnit2(session.getItemUnit2());
        spnInsertItemQtyReceivedDetil.setVolume1(edQtyReceive.getText().toString());
        spnInsertItemQtyReceivedDetil.setVolume2(edQtyReceive.getText().toString());

        //MENUNGGU MODUL KONTRAK MENGIRIM PANJANG MAT, KOEF KONVERSI
        if(session.getTransactionType().equals("Kontrak")) {
            spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org1(session.getItemHargaSatuanOrg1());
            spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org2(session.getItemHargaSatuanOrg1());
            spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std1(session.getItemHargaSatuanOrg1());
            spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std2(session.getItemHargaSatuanOrg1());
            spnInsertItemQtyReceivedDetil.setPanjang_Material("1");
            spnInsertItemQtyReceivedDetil.setKoefisien_Konversi("1.0");

            String flag = session.getSpesifikasi().toString();
            Log.d("tesezraflag",flag);
            int lengthflag = flag.length()-1;
            Log.d("tesezralength",String.valueOf(lengthflag));
            String finalFlag = flag.substring(lengthflag);
            Log.d("tesezra",finalFlag);
            spnInsertItemQtyReceivedDetil.setFlag_Detail_Kontrak(finalFlag);
        }
        else if(session.getTransactionType().equals("NPO")) {
            spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org1(session.getItemHargaSatuanOrg1());
            spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org2(session.getItemHargaSatuanOrg1());
            spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std1(session.getItemHargaSatuanOrg1());
            spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std2(session.getItemHargaSatuanOrg1());
            spnInsertItemQtyReceivedDetil.setPanjang_Material("1");
            spnInsertItemQtyReceivedDetil.setKoefisien_Konversi("1.0");
        }
        else if(session.getTransactionType().equals("Kontrak PO")) {
            spnInsertItemQtyReceivedDetil.setFlag_Detail_Kontrak("0");
        }
        else
        {
            spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org1(session.getItemHargaSatuanOrg1());
            spnInsertItemQtyReceivedDetil.setHarga_Satuan_Org2(session.getItemHargaSatuanOrg2());
            spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std1(session.getItemHargaSatuanStd1());
            spnInsertItemQtyReceivedDetil.setHarga_Satuan_Std2(session.getItemHargaSatuanStd2());
            spnInsertItemQtyReceivedDetil.setPanjang_Material(session.getItemPanjangMaterial());
            spnInsertItemQtyReceivedDetil.setKoefisien_Konversi(session.getItemKoefisienKonversi());

        }
        spnInsertItemQtyReceivedDetil.setKode_Lokasi("");
        spnInsertItemQtyReceivedDetil.setSurat_Jalan(nosuratjalan);
        spnInsertItemQtyReceivedDetil.setKeterangan(noketerangan);
        spnInsertItemQtyReceivedDetil.setNo_Kendaraan(nokendaraan);
        spnInsertItemQtyReceivedDetil.setDeskripsi(session.getItemDesc());
        spnInsertItemQtyReceivedDetil.setID_Transaksi_Detail(session.getKodeTransaksiPoDetil());
        spnInsertItemQtyReceivedDetil.setSpesifikasi(session.getSpesifikasi());
        spnInsertItemQtyReceivedDetil.setIndexPosition(position);

        String jsonString = gsonDetail.toJson(spnInsertItemQtyReceivedDetil);

        if(listBeforePostSpnDetil.isEmpty())
        {
            listBeforePostSpnDetil.add(spnInsertItemQtyReceivedDetil);
            //untuk cart item data prepost
        }
        else
        {
                //CHECK APAKAH ARRAY COLLECTION EXIST
                //JIKA EXIST MAKA UPDATE ELSE INSERT
                int avail = 0;
                int indexfinal = 0;
                float totalfinal = 0;

                for (int index = 0; index < listBeforePostSpnDetil.size(); index++)
                {
                    if( (listBeforePostSpnDetil.get(index).getID_Transaksi_Detail().equals(session.getKodeTransaksiPoDetil())) && (listBeforePostSpnDetil.get(index).getIndexPosition().equals(session.getIndexPosition())) )
                    {
                        avail = 1;
                        indexfinal = index;
                        Log.d("LogSpnlistt2", listBeforePostSpnDetil.get(index).getID_Transaksi_Detail());
                        Log.d("LogSpnlistt2", session.getKodeTransaksiPoDetil());
                        Log.d("LogSpnlistt2", listBeforePostSpnDetil.get(index).getIndexPosition());
                        Log.d("LogSpnlistt2", session.getIndexPosition());
                    }
//                    else if( (listBeforePostSpnDetil.get(index).getID_Transaksi_Detail().equals(session.getKodeTransaksiPoDetil())) && (!listBeforePostSpnDetil.get(index).getIndexPosition().equals(session.getIndexPosition())) ) {
//                        avail = 2;
//                        indexfinal = index;
//                        Log.d("LogSpnlistt1", listBeforePostSpnDetil.get(index).getID_Transaksi_Detail());
//                        Log.d("LogSpnlistt1", session.getKodeTransaksiPoDetil());
//                        Log.d("LogSpnlistt1", listBeforePostSpnDetil.get(index).getIndexPosition());
//                        Log.d("LogSpnlistt1", session.getIndexPosition());
//                        Log.d("LogSpnlistt1", String.valueOf(index));
//
//                    }
                }

                if(avail == 0)
                {
                    listBeforePostSpnDetil.add(spnInsertItemQtyReceivedDetil);
                }
                else if(avail == 1)
                {
                    listBeforePostSpnDetil.set(indexfinal, spnInsertItemQtyReceivedDetil);
                    Log.d("LogSpnlistt", "SATU");
                }
                else if(avail == 2)
                {
                    listBeforePostSpnDetil.add(indexfinal, spnInsertItemQtyReceivedDetil);
                    Log.d("LogSpnlistt", "DUA");
                }
        }

        //tampilkan list item cart
        DisplayCart();

        //ADD LIST PO UNTUK PRINTER
        listPo.add(session.getKodeTransaksiPo());
        listPoMark.add(session.getKodeTransaksiPo());
        listItemMark.add(session.getItemDesc().toLowerCase());

        if(!listKodePo.contains(session.getKodeThTransaction())){
            listKodePo.add(session.getKodeThTransaction());
        }

        //listKodePo.add(session.getKodeTransaksiPo());
        Log.d("LogSpnlistPo", session.getKodeTransaksiPo());
        Log.d("LogItemMark", listItemMark.toString());

    }

    public void postSPNFinal(String nosuratjalan, String keterangan){


        Gson gsonHeader1 = new Gson();
        ArrayList<SPNInsertItemQtyReceivedDetil> list = gsonHeader1.fromJson(session.getQtyReceivedArrayListSPN().toString(), new TypeToken<ArrayList<SPNInsertItemQtyReceivedDetil>>() {}.getType());

        Gson gsonHeader = new Gson();
        SPNInsertItemQtyReceived spnInsertItemQtyReceived = new SPNInsertItemQtyReceived();
        spnInsertItemQtyReceived.setID("0");
        spnInsertItemQtyReceived.setKode_SPN("0");
        spnInsertItemQtyReceived.setNo_Revisi("0");
        spnInsertItemQtyReceived.setKodeProyek(session.getKodeProyek());
        spnInsertItemQtyReceived.setTanggal_SPN(today);
        spnInsertItemQtyReceived.setKode_Zona(session.getKodeZona());
        spnInsertItemQtyReceived.setKode_Vendor(idvendor);
        spnInsertItemQtyReceived.setSurat_Jalan(nosuratjalan);
        spnInsertItemQtyReceived.setKeterangan(keterangan);
        spnInsertItemQtyReceived.setPengubah(session.isNikUserLoggedIn());
        spnInsertItemQtyReceived.setTokenID(session.getUserToken());
        spnInsertItemQtyReceived.setModeReq("SPN");
        spnInsertItemQtyReceived.setStatus_SPN("true");
        spnInsertItemQtyReceived.setFormid("PC.02.13");
        spnInsertItemQtyReceived.setSPNDetail(list);
        String jsonString = gsonHeader.toJson(spnInsertItemQtyReceived);
        Log.d("postSPNFinal",jsonString.toString());

        session.setReceivedItemDateTime(currentTime.toString());

        controller.SaveGeneralObject(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                try {
                    JSONObject obj = new JSONObject(output);
                    Log.d("postSPNFinalParam",obj.toString());
                    //session set to " "
                    session.setQtyReceivedArrayListSPN("");
                    if(obj.getString("IsSucceed").equals("true")) {

                        String output2 = obj.getString("VarOutput");
                        String[] split = output2.split("#");
                        String firstString = split[0];
                        String firstSecondstring = split[1];
                        String firstThirdstring = split[2];

                        isCanPosting(firstString, firstSecondstring, firstThirdstring);
                    }
                    else
                    {
                        Toast toast = Toast.makeText(ListPOActivity.this,"Save Success, "+obj.getString("Message")+" ! ", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                        Intent intent = new Intent();
                        intent.putExtra("", "");
                        setResult(request_data_from, intent);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void isCanPosting(String idspn, String kodespn, String norevisi) {

        MProgressDialog.showProgressDialog(ListPOActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.isNikUserLoggedIn());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("PC.02.13");
        approvalEnt.setReference_id(kodespn+"-"+norevisi);
        approvalEnt.setMode("is_can_posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
        //approvalEnt.setApproval_Name("Approval SPN Proyek");
        approvalEnt.setTokenID(session.getUserToken());

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {
                Log.d("Testing","IS"+output);
                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

                if(jsonObject.get("IsCanPosting").getAsString().equals("true"))
                {
                    dialogApr = new Dialog(ListPOActivity.this);
                    dialogApr.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogApr.setContentView(R.layout.general_dialog_fix);
                    dialogApr.setCanceledOnTouchOutside(false);

                    TextView dTittle = (TextView) dialogApr.findViewById(R.id.dTittle);
                    TextView dContent = (TextView) dialogApr.findViewById(R.id.dContent);
                    Button dBtnCancel = (Button) dialogApr.findViewById(R.id.dBtnCancel);
                    Button dBtnSubmit = (Button) dialogApr.findViewById(R.id.dBtnSubmit);

                    dTittle.setText("SPN Save");
                    dContent.setText("Posting SPN number "+kodespn+" ?");

                    dBtnSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String readymixstring = listItemMark.toString();
                            if(readymixstring.contains("readymix"))
                            {

                                dialogConfirmationPosting = new Dialog(ListPOActivity.this);
                                dialogConfirmationPosting.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialogConfirmationPosting.setContentView(R.layout.general_dialog_fix2);
                                dialogConfirmationPosting.setCanceledOnTouchOutside(false);

                                TextView dTittleConfirm = (TextView) dialogConfirmationPosting.findViewById(R.id.dTittle);
                                TextView dContentConfirm = (TextView) dialogConfirmationPosting.findViewById(R.id.dContent);
                                Button dBtnCancelConfirm = (Button) dialogConfirmationPosting.findViewById(R.id.dBtnCancel);
                                Button dBtnSubmitConfirm = (Button) dialogConfirmationPosting.findViewById(R.id.dBtnSubmit);

                                dTittleConfirm.setText("Confirmation");
                                dContentConfirm.setText("Pastikan volume SPN sudah sesuai dengan volume di Berita Acara Hitung Bersama (joint measurement) ?");

                                dBtnSubmitConfirm.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        onPosting(idspn, kodespn, norevisi);
                                    }
                                });


                                dBtnCancelConfirm.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialogConfirmationPosting.dismiss();
                                        Intent intent = new Intent();
                                        intent.putExtra("", "");
                                        setResult(request_data_from, intent);
                                        finish();
                                    }
                                });

                                dialogConfirmationPosting.show();

                            }
                            else
                            {
                                onPosting(idspn, kodespn, norevisi);
                            }
                        }
                    });


                    dBtnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogApr.dismiss();
                            Intent intent = new Intent();
                            intent.putExtra("", "");
                            setResult(request_data_from, intent);
                            finish();
                        }
                    });

                    dialogApr.show();
                }
                else
                {
                    Toast toast = Toast.makeText(ListPOActivity.this,"SPN Request Success!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    Intent intent = new Intent();
                    intent.putExtra("", "");
                    setResult(request_data_from, intent);
                    finish();
                }

            }
        });
    }

    private void onPosting(String idspn, String kodespn, String norevisi) {

        MProgressDialog.showProgressDialog(ListPOActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.isNikUserLoggedIn());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("PC.02.13");
        approvalEnt.setReference_id(kodespn+"-"+norevisi);
        approvalEnt.setMode("posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
        //approvalEnt.setApproval_Name("Approval SPN Proyek");
        approvalEnt.setTokenID(session.getUserToken());

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {


                Log.d("OmOm2", output);
                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();
                //NOTES JIKA POSTING ERROR MAKA KASIH TRIGER CATCH MESSAGE = ""
//                if(jsonObject.get("IsCanApprove").getAsString().equals("true"))
//                {
//                    Log.d("OmOm3", output+kodespn);
                    updateSpnApprovalNumber(jsonObject.get("Approval_Number").getAsString(), idspn, kodespn);
//                }
//                else
//                {
                    Toast toast = Toast.makeText(ListPOActivity.this,"Your Posting Success!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

//                    Intent intent = new Intent();
//                    intent.putExtra("", "");
//                    setResult(request_data_from, intent);
//                    finish();
//                }

            }
        });
    }

    Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, 1000, 1000, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 1000, 0, 0, w, h);
        return bitmap;
    }

    private void updateSpnApprovalNumber(String ApprovalNumber, String idspn, String kodespn) {
        Log.d("OmOm4", ApprovalNumber);
        MProgressDialog.showProgressDialog(ListPOActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();

        SPNInsertItemQtyReceived spnInsertItemQtyReceived = new SPNInsertItemQtyReceived();
        spnInsertItemQtyReceived.setID(idspn);
        spnInsertItemQtyReceived.setKode_SPN(kodespn);
        spnInsertItemQtyReceived.setNo_Revisi("0");
        spnInsertItemQtyReceived.setKodeProyek(session.getKodeProyek());
        spnInsertItemQtyReceived.setTanggal_SPN(today);
        spnInsertItemQtyReceived.setKode_Zona(session.getKodeZona());
        spnInsertItemQtyReceived.setKode_Vendor(idvendor);
        spnInsertItemQtyReceived.setSurat_Jalan(etNoSuratJalan.getText().toString());
        spnInsertItemQtyReceived.setKeterangan(etKeterangan.getText().toString());
        spnInsertItemQtyReceived.setPengubah(session.isNikUserLoggedIn());
        spnInsertItemQtyReceived.setTokenID(session.getUserToken());
        spnInsertItemQtyReceived.setModeReq("posting");
//        spnInsertItemQtyReceived.setSPNDetail(null);
        spnInsertItemQtyReceived.setFormid("PC.02.13");
        spnInsertItemQtyReceived.setApproval_Number(ApprovalNumber);
        spnInsertItemQtyReceived.setStatus_SPN("true");
        String jsonString = gsonHeader.toJson(spnInsertItemQtyReceived);
        Log.d("FinalInputSuccess",jsonString.toString());

        controller.SaveGeneralObject(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                try {

                    JSONObject obj = new JSONObject(output);
                    Log.d("FinalInputSuccessParam",obj.toString());
                    if(!obj.getString("IsSucceed").equals("false")) {
                        dialogApr.dismiss();
//                        Log.d("OmOm5", output);
//                        Toast toast = Toast.makeText(ListPOActivity.this, kodespn + "SPN Posting Success!", Toast.LENGTH_LONG);
//                        toast.setGravity(Gravity.CENTER, 0, 0);
//                        toast.show();
//
//                        Intent intent = new Intent();
//                        intent.putExtra("", "");
//                        setResult(request_data_from, intent);
//                        finish();

                        dInputTitlePrint.setText("Print QrCode");
                        dNamaVendor.setText(namavendor);
                        dNoPO.setText(kodepo);
                        dNoDO.setText(etNoSuratJalan.getText());
                        dTglPenerima.setText(today);
                        dPenerima.setText(session.getUserName());
                        dSpnNumber.setText(kodespn);

                        Bitmap bitmap = encodeAsBitmap(kodespn);
                        ivContentDialogPrint.setImageBitmap(bitmap);
                        ivContentDialogPrint2.setImageBitmap(bitmap);

                        for(int i = 0; i < listPo.size(); i++) {
                            if(i % 3 == 0)
                            {

                                //Print Linear Layout Content
                                llDialogContent.setDrawingCacheEnabled(true);
                                llDialogContent.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                                //llDialogContent.layout(0, 0, llDialogContent.getMeasuredWidth(), llDialogContent.getMeasuredHeight());
                                llDialogContent.layout(0, 0, 816, 236);
                                llDialogContent.buildDrawingCache(true);
                                bitmapTemp.add(Bitmap.createBitmap(llDialogContent.getDrawingCache()));
                                llDialogContent.setDrawingCacheEnabled(false); // clear drawing cache
                                Canvas newCanvas;
//                                if(bitmapTemp.isEmpty()) {
//                                    newCanvas = new Canvas(bitmapTemp.get(0));
//                                }
//                                else
//                                {
                                    newCanvas = new Canvas(bitmapTemp.get(bitmapTemp.size()-1));
//                                }
                                Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                                paint.setColor(Color.BLACK);
                                paint.setTextSize(28);
                                paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                                newCanvas.drawText(namavendor, 240, 45, paint);
                                newCanvas.drawText("No SPN", 240, 75, paint);
                                newCanvas.drawText("Kode DO", 240, 100, paint);
                                newCanvas.drawText("No PO", 240, 125, paint);
                                newCanvas.drawText("Tgl.Penerima", 240, 190, paint);
                                newCanvas.drawText("Penerima", 240, 215, paint);

                                newCanvas.drawText(": " + kodespn, 415, 75, paint);
                                newCanvas.drawText(": " + etNoSuratJalan.getText().toString(), 415, 100, paint);
                                int k = 0;
                                for(int j=0; j < 3; j++)
                                {
                                    if(j+i < listKodePo.size()) {
                                        Log.d("listPo2", listKodePo.get(j+i));
                                        newCanvas.drawText(": " + listKodePo.get(j+i), 415, 125 + k, paint);
                                        k = k + 25;
                                    }
                                }

                                newCanvas.drawText(": " + today, 415, 190, paint);
                                newCanvas.drawText(": " + session.getUserName(), 415, 215, paint);
                            }
                        }

                        //Percobaan Print ImageView
//                        ivContentDialogPrint.buildDrawingCache();
//                        Bitmap bb = ivContentDialogPrint.getDrawingCache();

                        ivSample.setImageBitmap(bitmapTemp.get(0));
                        dBtnPrint.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                        Print();
                                        //bitmapTemp.clear();
                                        finish();
                            }
                        });

                        dBtnPrintClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogprint.dismiss();
                                Intent intent = new Intent();
                                intent.putExtra("", "");
                                setResult(request_data_from, intent);
                                finish();
                            }
                        });
                        dialogprint.show();
                    }
                    else
                    {
                            Toast toast = Toast.makeText(ListPOActivity.this,"Save Posting Failed, "+obj.getString("Message")+" ! ", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                            Intent intent = new Intent();
                            intent.putExtra("", "");
                            setResult(request_data_from, intent);
                            finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (WriterException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvTitle, tvIdTrans, tvTgl, titleTypeTrans, titleIndex;
        private final View rootView;
        private final ImageView imgArrow;

        HeaderViewHolder(View view) {
            super(view);
            rootView = view;
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvIdTrans = (TextView) view.findViewById(R.id.tvIdTrans);
            tvTgl = (TextView) view.findViewById(R.id.tvTgl);
            titleTypeTrans = (TextView) view.findViewById(R.id.titleTypeTrans);
            titleIndex = (TextView) view.findViewById(R.id.titleIndex);
            imgArrow = (ImageView) view.findViewById(R.id.imgArrow);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {

        private final View rootView;

        FooterViewHolder(View view) {
            super(view);

            rootView = view;
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        private final View rootView;
        private final ImageView imgItem;
        private final TextView tvListItemName;
        private final TextView tvListItemBrand;
        private final TextView tvListQtyPo;
        private final TextView tvListQtyReceived;
        private final TextView tvListQtyInput;
        private final TextView tvListUnit;
        private final TextView tvUnit2;
        private final TextView tvQtyRcvNotRejected;
        private final ImageView ivCreate;
        private final TextView txtNoPlat;
        private final TextView txtIdTdPo;
        private final TextView txtTransType;
        private final TextView txtTglDipilih;
        private final TextView txtIdThPo;
        private final TextView tvQtyDR;


        private final RelativeLayout llRowPo;

        ItemViewHolder(View view) {
            super(view);

            rootView = view;
            imgItem = (ImageView) view.findViewById(R.id.imgItem);
            tvListItemName = (TextView) view.findViewById(R.id.tvNamaVendor);
            tvListItemBrand = (TextView) view.findViewById(R.id.tvKodeSPN);
            tvListQtyPo = (TextView) view.findViewById(R.id.tvListQtyPo);
            tvListQtyReceived = (TextView) view.findViewById(R.id.tvQtyReceived);
            tvListQtyInput = (TextView) view.findViewById(R.id.tvQtyInput);
            tvListUnit = (TextView) view.findViewById(R.id.tvUnit);
            tvUnit2 = (TextView) view.findViewById(R.id.tvUnit2);
            ivCreate = (ImageView) view.findViewById(R.id.ivCreate);
            tvQtyRcvNotRejected = (TextView) view.findViewById(R.id.tvQtyRcvNotRejected);
            llRowPo = (RelativeLayout) view.findViewById(R.id.rootView);
            txtNoPlat = (TextView) view.findViewById(R.id.txtNoPlat);
            txtIdTdPo = (TextView) view.findViewById(R.id.txtIdTdPo);
            txtTransType = (TextView) view.findViewById(R.id.txtTransType);
            txtTglDipilih = (TextView) view.findViewById(R.id.txtTglDipilih);
            txtIdThPo = (TextView) view.findViewById(R.id.txtIdThPo);
            tvQtyDR = (TextView) view.findViewById(R.id.tvQtyDR);
        }
    }

    public void Print(){

        Printer myPrinter = new Printer();
        PrinterInfo myPrinterInfo = new PrinterInfo();
        PrinterStatus myPrinterStatus = new PrinterStatus();

        try{
            // Retrieve printer informations
            myPrinterInfo = myPrinter.getPrinterInfo();

            // Set printer informations
            myPrinterInfo.printerModel = PrinterInfo.Model.PT_P750W;
            myPrinterInfo.port=PrinterInfo.Port.NET;
            myPrinterInfo.printMode=PrinterInfo.PrintMode.FIT_TO_PAGE;
            myPrinterInfo.paperSize = PrinterInfo.PaperSize.CUSTOM;
            myPrinterInfo.orientation = PrinterInfo.Orientation.LANDSCAPE;
            myPrinterInfo.numberOfCopies = 1;

            if(Paper.book().exist("printer")) {
                String ip1 = Paper.book().read("printer").toString();
                String ip2 = ip1.replace("[","");
                String ipfinal = ip2.replace("]","");
                Log.d("LogTest", ipfinal);
                myPrinterInfo.ipAddress=ipfinal;
            }
//            else
//            {
//                myPrinterInfo.ipAddress="192.168.118.1";
//            }


            //myPrinterInfo.macAddress="40:49:0F:89:F4:F8"; //hidden for security reasons

            LabelInfo mLabelInfo = new LabelInfo();
            mLabelInfo.labelNameIndex = 5;
            mLabelInfo.isAutoCut = true;
            mLabelInfo.isEndCut = true;
            mLabelInfo.isHalfCut = false;
            mLabelInfo.isSpecialTape = false;
            myPrinter.setPrinterInfo(myPrinterInfo);
            myPrinter.setLabelInfo(mLabelInfo);

            try{
                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try  {
                            myPrinter.startCommunication();
                            for(int l = 0; l < bitmapTemp.size(); l++) {
                                    if(l < bitmapTemp.size()) {
                                        myPrinter.printImage(bitmapTemp.get(l));
                                    }
                        }
                            myPrinter.endCommunication();
                            //Log.i("printlog", "info" + printerStatus.errorCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.i("printlog", "info= " + e);
                        }
                    }
                });
                thread.start();

            }catch(Exception e){
                Log.i("printlog", "info2" + e);
            }

        }catch(Exception e){
            Log.i("printlog", "info3" + e);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cartoptions, menu);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if( requestCode == request_data_from_listpoactivity2 ) {
            if (data != null){

                MProgressDialog.showProgressDialog(getApplicationContext(), true, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        MProgressDialog.dismissProgressDialog();
                    }
                });

                String kodeporesult, idporesult, tgldipilihresult, transactiontype, kodeidtransaksi;
                kodeporesult = data.getExtras().getString("kodepo");
                idporesult = data.getExtras().getString("idpo");
                tgldipilihresult = data.getExtras().getString("tgldipilih");
                transactiontype = data.getExtras().getString("transactiontype");
                kodeidtransaksi = data.getExtras().getString("kodepo");
                listPoMark.add(idporesult);
                //listKodePo.add(kodeidtransaksi);
//                if(!listKodePo.contains(session.getKodeThTransaction())){
//                    listKodePo.add(session.getKodeThTransaction());
//                }
//                Log.d("woio", "kodeporesult"+kodeporesult);
//                Log.d("woio", "idporesult"+idporesult);
//                Log.d("woio", "tgldipilihresult"+tgldipilihresult);
                sectionAdapter.addSection(new NewsSection(NewsSection.INDEX0, kodeporesult, idporesult, tgldipilihresult, transactiontype));

            }
        }
    }

    @Override
    public void onBackPressed() {

        //Bundle sendBundle = new Bundle();
        //sendBundle.putString("projectname", "po tes");
        //Intent intent = new Intent(getApplicationContext(), ListPOActivity.class );
        //intent.putExtras(sendBundle);
        //setResult(request_data_from,intent);
        session.setQtyReceivedArrayListSPN("");
        this.finish();
    }

    public void DisplayCart(){

        isanydata = db.checkForTables();
        //jika ada list di listBeforePostSpnDetil maka table dclear dahulu
        if(isanydata)
        {
            db.deleteTable();
        }

        for(SPNInsertItemQtyReceivedDetil spndetil12:listBeforePostSpnDetil) {
            Log.d("LogDBInsert",spndetil12.getID_Transaksi_Detail()+spndetil12.getDeskripsi()+" : "+spndetil12.getVolume1());
        }

        for(int x = 0 ; x < listBeforePostSpnDetil.size(); x++) {
            Log.d("LogDBInsert", String.valueOf(x));
            db.addUser(new SPNInsertItemQtyReceivedDetilTemp(listBeforePostSpnDetil.get(x).getDeskripsi(), listBeforePostSpnDetil.get(x).getSpesifikasi(), Double.parseDouble(listBeforePostSpnDetil.get(x).getVolume1()), Integer.parseInt(listBeforePostSpnDetil.get(x).getIndexPosition())));
        }

        List<SPNInsertItemQtyReceivedDetilTemp> items = db.getAllItems();
        for (SPNInsertItemQtyReceivedDetilTemp cn : items) {
            String log = "Id: " + cn.getId() + " ,Deskripsi: " + cn.getDeskripsi() + " ,Spesifikasi: " +
                    cn.getSpesifikasi() + " ,Volume: " + cn.getVolume()+ " ,IdIndex: " + cn.getIdIndex();
            // Writing Contacts to log
            Log.d("LogDBBeforeGroup", log);
        }

        List<SPNInsertItemQtyReceivedDetilTemp> items2 = db.getOrderItems();
        StringBuilder sb=new StringBuilder();
        for(int y = 0 ; y < items2.size(); y++)
        {
            int counter = y+1;
            sb.append(counter+". "+items2.get(y).getDeskripsi() + items2.get(y).getSpesifikasi() +
                    " (" + items2.get(y).getVolume()+")\n");
        }

        tvItemPreSubmit.setText(sb.toString());

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_cart) {

            //DisplayCart();
            return true;
        }
        else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
