package com.totalbp.spnapps.activity;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.totalbp.spnapps.MainActivity;
import com.totalbp.spnapps.R;
import com.totalbp.spnapps.config.AppConfig;
import com.totalbp.spnapps.config.SessionManager;
import com.totalbp.spnapps.controller.AppController;
import com.totalbp.spnapps.interfaces.VolleyCallback;
import com.totalbp.spnapps.model.CommonEnt;
import com.totalbp.spnapps.receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Ezra.R on 23/08/2017.
 */

public class FilterAllSPNActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private SearchableSpinner spinnerKodeSPN,spinnerVendor, spinnerStatus;
    private EditText etStartDateAllSPN,etEndDateAllSpn;
    AppController controller;
    private SessionManager sessionManager;
    private String kodeVendor, kodeSpn, startDate, endDate, status;

    ArrayList<CommonEnt> ddlArrayNamaVendor = new ArrayList<>();
    ArrayList<CommonEnt> ddlArrayNamaMaterial = new ArrayList<>();

    DatePickerDialog datedialogAllSpn;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener dateStart,dateEnd;
    FloatingActionButton fABFilterSpn;

    private static final int request_data_filter_allspn_activity  = 97;
    ArrayList<CommonEnt> ddlArrayListStatus = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_spn);

        myCalendar = Calendar.getInstance();

        spinnerKodeSPN = (SearchableSpinner) findViewById(R.id.spinnerKodeSPN);
        spinnerVendor = (SearchableSpinner) findViewById(R.id.spinnerVendor);
        etStartDateAllSPN = (EditText) findViewById(R.id.etStartDateAllSPN);
        etEndDateAllSpn = (EditText) findViewById(R.id.etEndDateAllSpn);
        spinnerStatus = (SearchableSpinner) findViewById(R.id.spinnerStatus);

        fABFilterSpn = (FloatingActionButton) findViewById(R.id.fABFilterSpn);


        sessionManager = new SessionManager(this);
        controller = new AppController();

        getSupportActionBar().setTitle("All SPN Data Filter");

        initControls();
    }

    private void updateLabel(EditText text) {
        String myFormat = "yyyy-MM-dd";
        //String myFormat = "dd-MM-yyyy"; //In which you need put here "EEE, d MMM yyyy HH:mm:ss Z"
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        text.setText(sdf.format(myCalendar.getTime()));
        //validateField();
    }

    public void initControls(){
        spinnerVendor.setPositiveButton("OK");
        spinnerVendor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonEnt commonEnt = (CommonEnt) parent.getSelectedItem();
                kodeVendor = commonEnt.getValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerKodeSPN.setPositiveButton("OK");
        spinnerKodeSPN.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonEnt commonEnt = (CommonEnt) parent.getSelectedItem();
                kodeSpn = commonEnt.getParameter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerStatus.setPositiveButton("OK");
        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonEnt commonEnt = (CommonEnt) parent.getSelectedItem();
                status = commonEnt.getValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        etStartDateAllSPN.setOnFocusChangeListener(new View.OnFocusChangeListener() { //supaya calendar sekali click
            @Override
            @TargetApi(Build.VERSION_CODES.M)
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    datedialogAllSpn =  new DatePickerDialog(v.getContext(), dateStart, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));
                    datedialogAllSpn.show();
                    etStartDateAllSPN.setShowSoftInputOnFocus(false);
                } else {
                    datedialogAllSpn.hide();
                }
            }
        });

        etStartDateAllSPN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(v.getContext(), dateStart, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });



        dateStart = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(etStartDateAllSPN);
            }
        };

        etEndDateAllSpn.setOnFocusChangeListener(new View.OnFocusChangeListener() { //supaya calendar sekali click
            @Override
            @TargetApi(Build.VERSION_CODES.M)
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    datedialogAllSpn =  new DatePickerDialog(v.getContext(), dateEnd, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));
                    datedialogAllSpn.show();
                    etEndDateAllSpn.setShowSoftInputOnFocus(false);
                } else {
                    datedialogAllSpn.hide();
                }
            }
        });

        etEndDateAllSpn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(v.getContext(), dateEnd, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        dateEnd = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(etEndDateAllSpn);
            }
        };

        fABFilterSpn.setOnClickListener((View v) -> {
//            if( (etStartDate.getText().length() != 0) && (etEndDate.getText().length() != 0) ) {
                Bundle sendBundle = new Bundle();

                sendBundle.putString("kodeVendor", kodeVendor);
                sendBundle.putString("kodeSpn", kodeSpn);
                sendBundle.putString("status", status);
                sendBundle.putString("startDate", etStartDateAllSPN.getText().toString());
                sendBundle.putString("endDate", etEndDateAllSpn.getText().toString());
                Log.d("CECECE","startDate"+etStartDateAllSPN.getText().toString());
                Log.d("CECECE","endDate"+etEndDateAllSpn.getText().toString());
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtras(sendBundle);
                setResult(request_data_filter_allspn_activity, intent);
                finish();
//            }
//            else
//            {
//                Toast.makeText(getApplicationContext(), "Start Date & End Date required!", Toast.LENGTH_LONG).show();
//            }
        });

        getDataForDDL(spinnerVendor,"GetVendor","Nama_Vendor","ID", ddlArrayNamaVendor,"@kd_Proyek",sessionManager.getKodeProyek(),"@Bank","0","@IsDDLReport","0", "","", "","", "","", "","");
        getDataForDDL(spinnerKodeSPN,"GetListSpnDetails","Kode_SPN","ID", ddlArrayNamaMaterial,"@currentpage","1","@pagesize","500","@sortby","", "@wherecond"," AND a.Kode_SPN is not null AND a.Kode_Proyek = "+sessionManager.getKodeProyek()+" ", "@nik",sessionManager.isNikUserLoggedIn(), "@kodeproyek",sessionManager.getKodeProyek(), "@formid","PC.02.13");
        getDataManualForDDL(spinnerStatus,"GetVendor","nama_vendor","ID", ddlArrayListStatus,"@kd_Proyek",sessionManager.getKodeProyek(),"@Bank","0","@IsDDLReport","0", "","", "","", "","", "","");


    }

    public void getDataForDDL(final SearchableSpinner spinner, final String uniqueSPName,
                              final String paramName, final String paramVal,
                              final ArrayList<CommonEnt> ddlArrayList,
                              final String paramName1, final String paramVal1,
                              final String paramName2, final String paramVal2,
                              final String paramName3, final String paramVal3,
                              final String paramName4, final String paramVal4,
                              final String paramName5, final String paramVal5,
                              final String paramName6, final String paramVal6,
                              final String paramName7, final String paramVal7){
        ddlArrayList.clear();

        ArrayList<CommonEnt> commonEntArrayListRequest = new ArrayList<>();
        commonEntArrayListRequest.add(new CommonEnt("UniqueDesc",uniqueSPName));
        if(!paramName1.equals("")){
            commonEntArrayListRequest.add(new CommonEnt(paramName1,paramVal1));
        }
        if(!paramName2.equals("")){
            commonEntArrayListRequest.add(new CommonEnt(paramName2,paramVal2));
        }
        if(!paramName3.equals("")){
            commonEntArrayListRequest.add(new CommonEnt(paramName3,paramVal3));
        }
        if(!paramName4.equals("")){
            commonEntArrayListRequest.add(new CommonEnt(paramName4,paramVal4));
        }
        if(!paramName5.equals("")){
            commonEntArrayListRequest.add(new CommonEnt(paramName5,paramVal5));
        }
        if(!paramName6.equals("")){
            commonEntArrayListRequest.add(new CommonEnt(paramName6,paramVal6));
        }
        if(!paramName7.equals("")){
            commonEntArrayListRequest.add(new CommonEnt(paramName7,paramVal7));
        }

        controller.InqGeneralNew(getApplicationContext(), commonEntArrayListRequest, new VolleyCallback() {
            @Override
            public void onSuccess(JSONArray result) {
                try {
                    if (result.length() > 0) {
//                        CommonEnt itemEntsAll = new CommonEnt("ALL",
//                                "");
//                        ddlArrayList.add(itemEntsAll); //untuk item all
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject item = result.getJSONObject(i);
                            CommonEnt itemEnts = new CommonEnt(item.getString(paramName),
                                    item.getString(paramVal));
                            ddlArrayList.add(itemEnts);
                        }
                        //hideDialog();
                        ArrayAdapter<CommonEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                        spinner.setAdapter(adapterDDLGlobal);
                    }
                    else{
                        CommonEnt itemEnts = new CommonEnt("No Item Found",
                                "-");
                        ddlArrayList.add(itemEnts);
                        ArrayAdapter<CommonEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                        spinner.setAdapter(adapterDDLGlobal);
                    }
                    //hideDialog();
                }catch (JSONException e){
                    Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                }
            }


            @Override
            public void onSave(String output) {

            }
        }, sessionManager.getUrlConfig()+AppConfig.URL_PAGING_RESTFULL_NEWDLL);
    }

    public void getDataManualForDDL(final SearchableSpinner spinner, final String uniqueSPName,
                              final String paramName, final String paramVal,
                              final ArrayList<CommonEnt> ddlArrayList,
                              final String paramName1, final String paramVal1,
                              final String paramName2, final String paramVal2,
                              final String paramName3, final String paramVal3,
                              final String paramName4, final String paramVal4,
                              final String paramName5, final String paramVal5,
                              final String paramName6, final String paramVal6,
                              final String paramName7, final String paramVal7){
        ddlArrayList.clear();

                        CommonEnt itemEnts1 = new CommonEnt("NOT POSTED", "4");
                        CommonEnt itemEnts2 = new CommonEnt("WAITING FOR APPROVAL", "1");
                        CommonEnt itemEnts3 = new CommonEnt("ON PROGRESS", "2");
                        CommonEnt itemEnts4 = new CommonEnt("APPROVED", "3");
                        CommonEnt itemEnts5 = new CommonEnt("REJECTED", "9");
                        ddlArrayList.add(itemEnts1);
                        ddlArrayList.add(itemEnts2);
                        ddlArrayList.add(itemEnts3);
                        ddlArrayList.add(itemEnts4);
                        ddlArrayList.add(itemEnts5);
                        ArrayAdapter<CommonEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                        spinner.setAdapter(adapterDDLGlobal);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        AppController.getInstance().setConnectivityListener(this);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;


        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.etStartDateAllSPN), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        CoordinatorLayout.LayoutParams params =(CoordinatorLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);
        snackbar.show();

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
}
