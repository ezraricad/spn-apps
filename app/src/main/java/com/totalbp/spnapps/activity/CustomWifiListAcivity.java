package com.totalbp.spnapps.activity;
import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.totalbp.spnapps.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ezra.R on 14/03/2018.
 */

public class CustomWifiListAcivity extends AppCompatActivity {
    private TextView mainText, dITittle;
    private ListView wifiDeviceList;
    private WifiManager mainWifi;
    private WifiReceiver receiverWifi;
    private List<ScanResult> wifiList;
    private StringBuilder sb;
    private String ssid_selected=null;
    private Context context=null;
    private Dialog dialogWifi;
    private EditText dSSID, dPassword;
    private Button dIBtnCancel, dIBtnSubmit;

    private static final int MY_PERMISSIONS_REQUEST =1 ;

    Button connect_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_wifi);
        context=getApplicationContext();
        setTitle("Choose Switchbox");
//        connect_btn=(Button) findViewById(R.id.connect_btn);

        //Dialog untuk input text
        dialogWifi = new Dialog(CustomWifiListAcivity.this);
        dialogWifi.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWifi.setContentView(R.layout.general_dialog_wifi);
        dialogWifi.setCanceledOnTouchOutside(false);
        dITittle = (TextView) dialogWifi.findViewById(R.id.dITittle);
        dSSID = (EditText) dialogWifi.findViewById(R.id.dSSID);
        dPassword = (EditText) dialogWifi.findViewById(R.id.dPassword);
        dIBtnCancel = (Button) dialogWifi.findViewById(R.id.dBtnCancelWifi);
        dIBtnSubmit = (Button) dialogWifi.findViewById(R.id.dBtnSubmitWifi);

        wifiDeviceList=(ListView)findViewById(R.id.listView);
        mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if (!mainWifi.isWifiEnabled())
        {
            Toast.makeText(getApplicationContext(), "Turning WiFi ON...", Toast.LENGTH_LONG).show();
            mainWifi.setWifiEnabled(true);
        }

        wifiDeviceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
//                String ssid = '"' + wifiList.get(position).SSID + '"';
//                final String mac = wifiList.get(position).BSSID;
//                String pass = "";


//                LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
//                View promptView = inflater.inflate(R.layout.general_dialog_wifi, null);
//
//                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CustomWifiListAcivity.this);
//                alertDialogBuilder.setView(promptView);
//                final EditText ssid_et = (EditText) promptView.findViewById(R.id.dSSID);
//                final EditText pass_et = (EditText) promptView.findViewById(R.id.dPassword);

                dSSID.setText(wifiList.get(position).SSID);

                dIBtnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogWifi.dismiss();
                    }
                });

                dIBtnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(Integer.parseInt(String.valueOf(dPassword.getText().toString().length())) >= 8)
                        {

                            String ssid = '"' + wifiList.get(position).SSID + '"';
                            String password = '"' + dPassword.getText().toString() + '"';

                            System.out.println("ssid: "+ssid);
                            System.out.println("password: "+password);

                            //                                if(isConnectedTo(ssid)){ //see if we are already connected to the given ssid
                            //                                    Toast toast = Toast.makeText(CustomWifiListAcivity.this,"Wifi Connected to "+ssid, Toast.LENGTH_LONG);
                            //                                    toast.setGravity(Gravity.CENTER, 0, 0);
                            //                                    toast.show();
                            //                                }
                            //
                            //                                WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                            //                                WifiConfiguration wifiConfig = getWiFiConfig(ssid);
                            //                                if(wifiConfig == null){//if the given ssid is not present in the WiFiConfig, create a config for it
                            //                                    createWPAProfile(ssid,password);
                            //                                    wifiConfig = getWiFiConfig(ssid);
                            //                                }
                            //                                wm.disconnect();
                            //                                wm.enableNetwork(wifiConfig.networkId,true);
                            //                                wm.reconnect();
                            //                                Log.d("LogCreateReconnect","intiated connection to SSID"+ssid);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                connectToHost(getApplicationContext(), ssid, password);
                            }
                            else{
                                connectToHost2(getApplicationContext(), ssid, password);
                            }

                        }
                        else
                        {
                            Toast.makeText(CustomWifiListAcivity.this, "Password minimal 8 digit!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                dialogWifi.show();

            }
        });

    }

    public boolean isConnectedTo(String ssid){
        WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if(wm.getConnectionInfo().getSSID() == ssid){
            return true;
        }
        else {
            return false;
        }
    }

    public WifiConfiguration getWiFiConfig(String ssid) {
        WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        List<WifiConfiguration> wifiList=wm.getConfiguredNetworks();
        for (WifiConfiguration item : wifiList){
            if(item.SSID != null && item.SSID.equals(ssid)){
                return item;
            }
        }
        return null;
    }

    public void createWPAProfile(String ssid,String pass){
        Log.d("LogCreateWPAProfile","Saving SSID :"+ssid);
        WifiConfiguration conf=new WifiConfiguration();
        conf.SSID = ssid;
        conf.preSharedKey = pass;
        WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wm.addNetwork(conf);
        Log.d("LogCreateWPAProfile","saved SSID to WiFiManger");
    }


    public void connectToHost(Context context,String host,String password){
        mainWifi  = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiConfiguration wc=new WifiConfiguration();

        wc.SSID= host;
        wc.preSharedKey =  password;

        int netId=mainWifi.addNetwork(wc);


        try {
            mainWifi.enableNetwork(netId, true);
            mainWifi.setWifiEnabled(true);

//            System.out.println("enabled network");
        } catch (Exception e) {
            e.printStackTrace();
//            System.out.println(e.getMessage());
            Toast toast = Toast.makeText(CustomWifiListAcivity.this,e.getMessage(), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }


    public void connectToHost2(Context context,String host,String password){
        mainWifi  = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiConfiguration wc=new WifiConfiguration();

        wc.SSID= host;
        wc.preSharedKey =  password;
        wc.status = WifiConfiguration.Status.ENABLED;
        wc.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
        wc.allowedProtocols.set(WifiConfiguration.Protocol.WPA); // For WPA
        wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN); // For WPA2
        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);


        int netId=mainWifi.addNetwork(wc);


        try {
            mainWifi.enableNetwork(netId, true);
            mainWifi.setWifiEnabled(true);

            //System.out.println("enabled network");

        } catch (Exception e) {
            e.printStackTrace();
            Toast toast = Toast.makeText(CustomWifiListAcivity.this,e.getMessage(), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            //System.out.println(e.getMessage());
        }
    }

    public static class WiFiChngBrdRcr extends BroadcastReceiver { // shows a toast message to the user when device is connected to a AP
        private String TAG = "LogCreateChangeBrd";

        @Override
        public void onReceive(Context context, Intent intent) {

            //Log.d(TAG, "ConnectedLogWiFiChngBrdRcr:"+intent.getStringExtra("WIFI_INFO").toString());

            NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            if(networkInfo.getState() == NetworkInfo.State.CONNECTED)
            {
//                String bssid=intent.getStringExtra(WifiManager.EXTRA_BSSID);
//                Log.d(TAG, "Connected to BSSID:"+bssid);
                WifiInfo ssid= intent.getParcelableExtra(WifiManager.EXTRA_WIFI_INFO);
                String log="Connected to SSID:"+ssid;
                String logcomplete="Connected to SSID:"+ssid.getSupplicantState();
                Log.d(TAG,"Connected to SSID:"+logcomplete);
                if(ssid.getSupplicantState().toString().equals("DISCONNECTED"))
                {
                    Toast.makeText(context, "Wifi Connected Fail!", Toast.LENGTH_LONG).show();

//                    intent.putExtra("WIFI_INFO","WIFI_LOST");
//                    context.sendBroadcast(intent);
                    //finish();
                }
                else
                {
                    Toast.makeText(context, "Wifi Connected Success!", Toast.LENGTH_LONG).show();

//                    intent.putExtra("WIFI_INFO","WIFI_CONNECTED");
//                    context.sendBroadcast(intent);
                    //finish();
                }
            }
        }
    }

    public void CloseApp()
    {
        finish();
    }

    class WifiReceiver extends BroadcastReceiver {
        private String TAG = "LogCreateChangeBrd";
        public void onReceive(Context c, Intent intent) {
            Log.d(TAG, "ConnectedLogWiFiReceiver:"+intent.getAction().toString());

            String action = intent.getAction();
            if(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(action)){
                sb = new StringBuilder();
                wifiList = mainWifi.getScanResults();
                sb.append("\n Number Of Wifi connections :"+wifiList.size()+"\n\n");

                ArrayList<String> deviceList = new ArrayList<String>();
                for(int i = 0; i < wifiList.size(); i++){
                    sb.append(new Integer(i+1).toString() + ". ");
                    sb.append((wifiList.get(i)).toString());
                    sb.append("\n\n");
                    Log.d("wifiList",wifiList.get(i).SSID);
//                    if(wifiList.get(i).SSID.contains("750"))
//                    {
                        deviceList.add(wifiList.get(i).SSID);
//                    }
                }

                ArrayAdapter arrayAdapter = new ArrayAdapter(CustomWifiListAcivity.this,
                        android.R.layout.simple_list_item_1, deviceList.toArray());

                wifiDeviceList.setAdapter(arrayAdapter);
            }

        }

    }



    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiverWifi);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        receiverWifi = new CustomWifiListAcivity.WifiReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        registerReceiver(receiverWifi, intentFilter);
        getWifi();
    }
    private void getWifi() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Toast.makeText(CustomWifiListAcivity.this, "version>=marshmallow", Toast.LENGTH_SHORT).show();

            if (ContextCompat.checkSelfPermission(CustomWifiListAcivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(CustomWifiListAcivity.this, "Location Turn On Required!", Toast.LENGTH_SHORT).show();

                ActivityCompat.requestPermissions(CustomWifiListAcivity.this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST);

            }

            else{
                //Toast.makeText(CustomWifiListAcivity.this, "location turned on", Toast.LENGTH_SHORT).show();
                mainWifi.startScan();
            }

        }else {

            Toast.makeText(CustomWifiListAcivity.this, "Wifi Scanning", Toast.LENGTH_SHORT).show();
            mainWifi.startScan();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == MY_PERMISSIONS_REQUEST) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(CustomWifiListAcivity.this, "permission granted", Toast.LENGTH_SHORT).show();
                mainWifi.startScan();

            }else{
                Toast.makeText(CustomWifiListAcivity.this, "permission not granted", Toast.LENGTH_SHORT).show();
                return;
            }

        }
    }

}
