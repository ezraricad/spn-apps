package com.totalbp.spnapps.activity;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.totalbp.spnapps.R;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.totalbp.spnapps.MainActivity;
import com.totalbp.spnapps.R;
import com.totalbp.spnapps.config.AppConfig;
import com.totalbp.spnapps.config.SessionManager;
import com.totalbp.spnapps.controller.AppController;
import com.totalbp.spnapps.interfaces.VolleyCallback;
import com.totalbp.spnapps.model.CommonEnt;
import com.totalbp.spnapps.receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Ezra.R on 23/08/2017.
 */

public class FilterUpcomingActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private SearchableSpinner spinnerNamaVendor,spinnerItemMaterial, spinnerStatus;
    private EditText etStartDate,etEndDate;
    AppController controller;
    ArrayList<CommonEnt> ddlArrayNamaVendor = new ArrayList<>();
    ArrayList<CommonEnt> ddlArrayNamaMaterial = new ArrayList<>();
    private SessionManager sessionManager;
    DatePickerDialog datedialog;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener dateStart,dateEnd;
    FloatingActionButton floatingActionButtonSearchDataFilter;
    private static final int request_data_filter_upcoming_activity  = 98;
    String kodeVendor, kodeMaterial;
    public Integer searchActive = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_upcoming);

        spinnerNamaVendor = (SearchableSpinner) findViewById(R.id.spinnerNamaVendor);
        spinnerItemMaterial = (SearchableSpinner) findViewById(R.id.spinnerItemMaterial);
        etStartDate = (EditText) findViewById(R.id.etStartDate);
        etEndDate = (EditText) findViewById(R.id.etEndDate);
        myCalendar = Calendar.getInstance();
        floatingActionButtonSearchDataFilter = (FloatingActionButton) findViewById(R.id.action_header_search_btn);

        sessionManager = new SessionManager(this);
        controller = new AppController();

        getSupportActionBar().setTitle("Upcoming Data Filter");
        initControls();
    }

    public static ArrayList<String> getDates(String dateString1, String dateString2)
    {
        Log.d("LogFilterUpcoming",dateString1);
        ArrayList<String> dates = new ArrayList<String>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = dateFormat.parse(dateString1);
            date2 = dateFormat.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while(!cal1.after(cal2))
        {
            dates.add(dateFormat.format(cal1.getTime()));
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    public void initControls(){
        spinnerNamaVendor.setPositiveButton("OK");
//        spinnerWorkItems.setAdapter(adapterWorkItem);
        spinnerNamaVendor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonEnt commonEnt = (CommonEnt) parent.getSelectedItem();
                kodeVendor = commonEnt.getValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        spinnerItemMaterial.setPositiveButton("OK");
//        spinnerItemMaterial.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                final CommonEnt commonEnt = (CommonEnt) parent.getSelectedItem();
//                kodeMaterial = commonEnt.getValue();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });


        getDataForDDL(spinnerNamaVendor,"GetVendor","Nama_Vendor","ID", ddlArrayNamaVendor,"@kd_Proyek",sessionManager.getKodeProyek(),"@Bank","0","@IsDDLReport","0", "","");
//        getDataForDDL(spinnerItemMaterial,"GetMaterialInq","Deskripsi","Kode_Katalog", ddlArrayNamaMaterial,"@currentpage","1","@pagesize","500","@sortby","", "@wherecond","");

        etStartDate.setOnFocusChangeListener(new View.OnFocusChangeListener() { //supaya calendar sekali click
            @Override
            @TargetApi(Build.VERSION_CODES.M)
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    datedialog =  new DatePickerDialog(v.getContext(), dateStart, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));
                    datedialog.show();
                    etStartDate.setShowSoftInputOnFocus(false);
                } else {
                    datedialog.hide();
                }
            }
        });

        etStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(v.getContext(), dateStart, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });



        dateStart = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(etStartDate);
            }
        };

        etEndDate.setOnFocusChangeListener(new View.OnFocusChangeListener() { //supaya calendar sekali click
            @Override
            @TargetApi(Build.VERSION_CODES.M)
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    datedialog =  new DatePickerDialog(v.getContext(), dateEnd, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));
                    datedialog.show();
                    etEndDate.setShowSoftInputOnFocus(false);
                } else {
                    datedialog.hide();
                }
            }
        });

        etEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(v.getContext(), dateEnd, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        dateEnd = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(etEndDate);
            }
        };

        floatingActionButtonSearchDataFilter.setOnClickListener((View v) -> {
            if( (etStartDate.getText().length() != 0) && (etEndDate.getText().length() != 0) ) {

                List<String> dates = getDates(etStartDate.getText().toString(), etEndDate.getText().toString());
//                for(String date:dates)
//                {
//                    Log.d("LogFilterUpcoming",dates.toString());
//                }
                //String datesfinal = dates.toString();
                Bundle sendBundle = new Bundle();
                sendBundle.putString("kodeVendor", kodeVendor);
                sendBundle.putString("dateRange", (dates.toString()));
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtras(sendBundle);
                setResult(request_data_filter_upcoming_activity, intent);
                finish();
            }
            else
            {
                Bundle sendBundle = new Bundle();
                sendBundle.putString("kodeVendor", kodeVendor);
                sendBundle.putString("dateRange", (""));
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtras(sendBundle);
                setResult(request_data_filter_upcoming_activity, intent);
                finish();


            }
        });


    }

    private void updateLabel(EditText text) {
        String myFormat = "dd-MM-yyyy"; //In which you need put here "EEE, d MMM yyyy HH:mm:ss Z"
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        text.setText(sdf.format(myCalendar.getTime()));
        //validateField();
    }

    public void getDataForDDL(final SearchableSpinner spinner, final String uniqueSPName,
                              final String paramName, final String paramVal,
                              final ArrayList<CommonEnt> ddlArrayList,
                              final String paramName1, final String paramVal1,
                              final String paramName2, final String paramVal2,
                              final String paramName3, final String paramVal3,
                              final String paramName4, final String paramVal4){
        ddlArrayList.clear();

        ArrayList<CommonEnt> commonEntArrayListRequest = new ArrayList<>();
        commonEntArrayListRequest.add(new CommonEnt("UniqueDesc",uniqueSPName));
        if(!paramName1.equals("")){
            commonEntArrayListRequest.add(new CommonEnt(paramName1,paramVal1));
        }
        if(!paramName2.equals("")){
            commonEntArrayListRequest.add(new CommonEnt(paramName2,paramVal2));
        }
        if(!paramName3.equals("")){
            commonEntArrayListRequest.add(new CommonEnt(paramName3,paramVal3));
        }
        if(!paramName4.equals("")){
            commonEntArrayListRequest.add(new CommonEnt(paramName4,paramVal4));
        }

        controller.InqGeneralNew(getApplicationContext(), commonEntArrayListRequest, new VolleyCallback() {
            @Override
            public void onSuccess(JSONArray result) {
                try {
                    if (result.length() > 0) {
//                        CommonEnt itemEntsAll = new CommonEnt("ALL",
//                                "");
//                        ddlArrayList.add(itemEntsAll); //untuk item all
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject item = result.getJSONObject(i);
                            CommonEnt itemEnts = new CommonEnt(item.getString(paramName),
                                    item.getString(paramVal));
                            ddlArrayList.add(itemEnts);
                        }
                        //hideDialog();
                        ArrayAdapter<CommonEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                        spinner.setAdapter(adapterDDLGlobal);
                    }
                    else{
                        CommonEnt itemEnts = new CommonEnt("No Item Found",
                                "-");
                        ddlArrayList.add(itemEnts);
                        ArrayAdapter<CommonEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                        spinner.setAdapter(adapterDDLGlobal);
                    }
                    //hideDialog();
                }catch (JSONException e){
                    Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                }
            }


            @Override
            public void onSave(String output) {

            }
        }, sessionManager.getUrlConfig()+AppConfig.URL_PAGING_RESTFULL_NEWDLL);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        AppController.getInstance().setConnectivityListener(this);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;


        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.etStartDate), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        CoordinatorLayout.LayoutParams params =(CoordinatorLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);
        snackbar.show();

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
}
