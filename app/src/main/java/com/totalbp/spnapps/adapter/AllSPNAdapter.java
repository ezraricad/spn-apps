package com.totalbp.spnapps.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.totalbp.spnapps.R;
import com.totalbp.spnapps.model.NewSPNEnt;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by 140030 on 07/06/2017.
 */

public class AllSPNAdapter extends BaseAdapter {
    Context context;
    ArrayList<NewSPNEnt> commonEntArrayList;
    ArrayList<NewSPNEnt> arraylist;

    public AllSPNAdapter(Context context, ArrayList<NewSPNEnt> commonEntArrayList) {
        this.context = context;
        this.commonEntArrayList = commonEntArrayList;

        arraylist =  new ArrayList<NewSPNEnt>();
        arraylist.addAll(commonEntArrayList);
    }

    private class ViewHolder{
        TextView txtParameterId;
        TextView txtParameterKodeSPN;
        TextView txtParameterNoRevisi;
        TextView txtParameterKodeProyek;
        TextView txtParameterTglSpn;
        TextView txtParameterKodeZona;
        TextView txtParameterKodeVendor;
        TextView txtParameterSuratJalan;
        TextView txtParameterKeterangan;
        TextView txtParameterNamaVendor;
        TextView txtParameterStatusApproval;
        TextView txtParameterPoCount;
    }

    @Override
    public int getCount() {
        return commonEntArrayList.size();

    }

    @Override
    public Object getItem(int position) {
        return commonEntArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        NewSPNEnt rowItem = commonEntArrayList.get(position);
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.fragment_list_row_allspn,null);
            holder = new ViewHolder();

            holder.txtParameterId = (TextView) convertView.findViewById(R.id.textViewRowParameterId);
            holder.txtParameterKodeSPN = (TextView) convertView.findViewById(R.id.tvKodeSPN);
            holder.txtParameterNoRevisi = (TextView) convertView.findViewById(R.id.tvQtyReceived);
            holder.txtParameterKodeProyek = (TextView) convertView.findViewById(R.id.tvIdPo);
            holder.txtParameterTglSpn = (TextView) convertView.findViewById(R.id.tvIDVendor);
            holder.txtParameterKodeZona = (TextView) convertView.findViewById(R.id.textViewRowParameterKodeZona);
            holder.txtParameterKodeVendor = (TextView) convertView.findViewById(R.id.textViewRowParameterKodeVendor);
            holder.txtParameterSuratJalan = (TextView) convertView.findViewById(R.id.textViewRowParameterSuratJalan);
            holder.txtParameterKeterangan = (TextView) convertView.findViewById(R.id.textViewRowParameterKeterangan);
            holder.txtParameterNamaVendor = (TextView) convertView.findViewById(R.id.tvNamaVendor);
            holder.txtParameterStatusApproval = (TextView) convertView.findViewById(R.id.textViewRowParameterStatusApproval);
            holder.txtParameterPoCount = (TextView) convertView.findViewById(R.id.textViewRowParameterPoCount);
            convertView.setTag(holder);
        }
        else
            holder = (ViewHolder) convertView.getTag();
            holder.txtParameterId.setText(rowItem.getId());
            holder.txtParameterKodeSPN.setText(rowItem.getKode_spn());
            holder.txtParameterNoRevisi.setText(rowItem.getTgl_spn());
            holder.txtParameterKodeProyek.setText(rowItem.getKode_proyek());
            holder.txtParameterTglSpn.setText(rowItem.getTgl_spn());
            holder.txtParameterKodeZona.setText(rowItem.getKode_zona());
            holder.txtParameterKodeVendor.setText(rowItem.getKode_vendor());
            holder.txtParameterSuratJalan.setText(rowItem.getSurat_jalan());
            holder.txtParameterKeterangan.setText(rowItem.getKeterangan());
            holder.txtParameterNamaVendor.setText(rowItem.getNama_vendor());
            holder.txtParameterStatusApproval.setText(rowItem.getStatus_approval());
            holder.txtParameterPoCount.setText(rowItem.getPo_qty());

        //CheckedTextView checkedTextView = (CheckedTextView)convertView.findViewById(R.id.checkSort);
        //checkedTextView.setVisibility(View.INVISIBLE);

        return convertView;
    }


    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());

        commonEntArrayList.clear();

        if (charText.length() == 0) {
            commonEntArrayList.addAll(arraylist);

        } else {
            for (NewSPNEnt postDetail : arraylist) {
                if (charText.length() != 0 && postDetail.getId().toLowerCase(Locale.getDefault()).contains(charText)) {
                    commonEntArrayList.add(postDetail);
                }
                else if (charText.length() != 0 && postDetail.getKode_spn().toLowerCase(Locale.getDefault()).contains(charText)) {
                    commonEntArrayList.add(postDetail);
                }
                else if (charText.length() != 0 && postDetail.getNama_vendor().toLowerCase(Locale.getDefault()).contains(charText)) {
                    commonEntArrayList.add(postDetail);
                }
            }
        }

        notifyDataSetChanged();
    }

}
