package com.totalbp.spnapps.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.totalbp.spnapps.R;
import com.totalbp.spnapps.config.AppConfig;
import com.totalbp.spnapps.config.SessionManager;
import com.totalbp.spnapps.model.NewSPNEnt;
import com.totalbp.spnapps.utils.PicassoTransform.CircleTransform;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ezra.R on 15/11/2017.
 */

public class AllSPNRecyclerViewAdapter extends RecyclerView.Adapter<AllSPNRecyclerViewAdapter.MyViewHolder>{
    private Context mContext;
    private List<NewSPNEnt> itemRequest;
    private MessageAdapterListener listener;
    private SparseBooleanArray selectedItems;
    private static int currentSelectedIndex = -1;
    private SessionManager session;
    // array used to perform multiple animation at once
    private SparseBooleanArray animationItemsIndex;
    private boolean reverseAllAnimations = false;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        public TextView tvItemName, tvTower, tvFloor, tvZona, tvMoCode, tvCreatedDate;
        public RelativeLayout messageContainer;
        public TextView tvNamaVendor, tvKodeSPN,tvTglSPN,tvIDVendor, tvApproval, tvRevisi;
        public RelativeLayout rlContainer;
        public ImageView list_image;

        public MyViewHolder(View view) {
            super(view);

            tvKodeSPN = (TextView) view.findViewById(R.id.tvKodeSPN);
            tvTglSPN =  (TextView) view.findViewById(R.id.tvQtyReceived);
            tvNamaVendor = (TextView) view.findViewById(R.id.tvNamaVendor);
            tvApproval = (TextView) view.findViewById(R.id.tvApproval);
            rlContainer = (RelativeLayout) view.findViewById(R.id.thumbnail);
            list_image = (ImageView) view.findViewById(R.id.list_image);
            tvRevisi = (TextView) view.findViewById(R.id.tvRevisi);

            view.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {
            listener.onRowLongClicked(getAdapterPosition());
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
            return true;
        }
    }


    public AllSPNRecyclerViewAdapter(Context mContext, List<NewSPNEnt> itemrequest, MessageAdapterListener listener) {
        this.mContext = mContext;
        this.itemRequest = itemrequest;
        this.listener = listener;
        selectedItems = new SparseBooleanArray();
        animationItemsIndex = new SparseBooleanArray();
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_list_row_allspn, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        session = new SessionManager(mContext);
        NewSPNEnt rowItem = itemRequest.get(position);

        holder.tvKodeSPN.setText(rowItem.getKode_spn());
        holder.tvTglSPN.setText(rowItem.getTgl_spn());
        holder.tvNamaVendor.setText(rowItem.getNama_vendor());

        Picasso.with(mContext)
                .load(session.getUrlConfig()+AppConfig.URL_IMAGE_PREFIX+rowItem.getPathimagevendor())
                .placeholder(R.drawable.default_placeholder_small)
                .error(R.drawable.default_placeholder_small)
                .transform(new CircleTransform())
                .into(holder.list_image);

        Log.d("ImageList", rowItem.getPathimagevendor().toString());
       //holder.list_image.setImageBitmap(rowItem.getP);
        if(rowItem.getStatus_approval().equals("4"))
        {
            holder.tvApproval.setBackgroundResource(R.drawable.btn_yellowtrans);
            holder.tvApproval.setText("NOT POSTED");
        }
        else if(rowItem.getStatus_approval().equals("1"))
        {
            holder.tvApproval.setBackgroundResource(R.drawable.btn_orangetrans);
            holder.tvApproval.setText("WAITING FOR APPROVAL");
        }
        else if(rowItem.getStatus_approval().equals("2"))
        {
            holder.tvApproval.setBackgroundResource(R.drawable.btn_orangetrans);
            holder.tvApproval.setText("ON PROGRESS");
        }
        else if(rowItem.getStatus_approval().equals("3"))
        {
            holder.tvApproval.setBackgroundResource(R.drawable.btn_bluetrans);
            holder.tvApproval.setText("APPROVED");
        }
        else if(rowItem.getStatus_approval().equals("9"))
        {
            holder.tvApproval.setBackgroundResource(R.drawable.btn_redtrans);
            holder.tvApproval.setText("REJECTED");
        }

        if(!rowItem.getNo_revisi().equals("0")) {
            String Rev = "Rev "+rowItem.getNo_revisi().toString();
            holder.tvRevisi.setText(Rev);
        }
        else
        {
            holder.tvRevisi.setText("");
        }
//
//
//
//        holder.tvCreatedDate.setText(rowItem.getTanggal_Dibuat());

        //applyFile(holder, rowItem);
        // handle icon animation
        //applyIconAnimation(holder, position);

        // display profile image
        //applyProfilePicture(holder, rowItem);

        // apply click events
        applyClickEvents(holder, position);
    }

    private void applyClickEvents(MyViewHolder holder, final int position) {


        holder.rlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onMessageRowClicked(position);
            }
        });

        holder.list_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onIconImportantClicked(position);
            }
        });
         /*
        holder.messageContainer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.onRowLongClicked(position);
                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
                return true;
            }
        });
        */
    }

    private void applyFile(MyViewHolder holder, NewSPNEnt message) {
        /*
        if (!message.getFileUploadUrl().equals("")) {
            holder.ivFile.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_insert_drive_file_black_24dp));
            holder.ivFile.setColorFilter(ContextCompat.getColor(mContext, R.color.yellow));
        } else {
            holder.ivFile.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_attach_file_black_24dp));
            //holder.ivFile.setColorFilter(ContextCompat.getColor(mContext, R.color.yellow));
        }
        */
    }

    private void applyImportant(MyViewHolder holder, NewSPNEnt message) {
        /*
        if (message.isImm()) {
            holder.ivStar.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_star_black_24dp));
            holder.ivStar.setColorFilter(ContextCompat.getColor(mContext, R.color.yellow));
        } else {
            holder.ivStar.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_star_border_black_24dp));
            //holder.ivStar.setColorFilter(ContextCompat.getColor(mContext, R.color.yellow));
        }
        */
    }

    private void applyIconAnimation(MyViewHolder holder, int position) {
        /*
        if (selectedItems.get(position, false)) {
            holder.iconFront.setVisibility(View.GONE);
            resetIconYAxis(holder.iconBack);
            holder.iconBack.setVisibility(View.VISIBLE);
            holder.iconBack.setAlpha(1);
            if (currentSelectedIndex == position) {
                FlipAnimator.flipView(mContext, holder.iconBack, holder.iconFront, true);
                resetCurrentIndex();
            }
        } else {
            holder.iconBack.setVisibility(View.GONE);
            resetIconYAxis(holder.iconFront);
            holder.iconFront.setVisibility(View.VISIBLE);
            holder.iconFront.setAlpha(1);
            if ((reverseAllAnimations && animationItemsIndex.get(position, false)) || currentSelectedIndex == position) {
                FlipAnimator.flipView(mContext, holder.iconBack, holder.iconFront, false);
                resetCurrentIndex();
            }
        }
        */
    }


    // As the views will be reused, sometimes the icon appears as
    // flipped because older view is reused. Reset the Y-axis to 0
    private void resetIconYAxis(View view) {
        if (view.getRotationY() != 0) {
            view.setRotationY(0);
        }
    }

    public void resetAnimationIndex() {
        reverseAllAnimations = false;
        animationItemsIndex.clear();
    }

    //@Override
    //public long getItemId(int position) {
//        return messages.get(position).getId();
//    }



    @Override
    public int getItemCount() {
        return itemRequest.size();
    }

    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
            animationItemsIndex.delete(pos);
        } else {
            selectedItems.put(pos, true);
            animationItemsIndex.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        reverseAllAnimations = true;
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        itemRequest.remove(position);
        resetCurrentIndex();
    }

    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }

    public interface MessageAdapterListener {
        void onIconClicked(int position);

        void onIconImportantClicked(int position);

        void onMessageRowClicked(int position);

        void onRowLongClicked(int position);
    }

}
