package com.totalbp.spnapps.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalbp.spnapps.R;
import com.totalbp.spnapps.model.UpcomingListPoEnt;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by 140030 on 07/06/2017.
 */

public class ListPoActivity2Adapter extends BaseAdapter {
    Context context;
    ArrayList<UpcomingListPoEnt> commonEntArrayList;
    ArrayList<UpcomingListPoEnt> arraylist;
    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
    Date date;
    public ListPoActivity2Adapter(Context context, ArrayList<UpcomingListPoEnt> commonEntArrayList) {
        this.context = context;
        this.commonEntArrayList = commonEntArrayList;

        arraylist =  new ArrayList<UpcomingListPoEnt>();
        arraylist.addAll(commonEntArrayList);
    }

    private class ViewHolder{
        TextView tvListItemName;
        TextView tvListItemBrand;
        TextView tvItem;
        TextView tvIdPo;
        ImageView imgItem;
        TextView tvIDVendor;
        TextView tvTglRencanaKirim;
        TextView tvTransactionType;
    }

    @Override
    public int getCount() {
        return commonEntArrayList.size();

    }

    @Override
    public Object getItem(int position) {
        return commonEntArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        UpcomingListPoEnt rowItem = commonEntArrayList.get(position);



        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.activity2_list_po_row,null);
            holder = new ViewHolder();

            holder.tvListItemName= (TextView) convertView.findViewById(R.id.tvNamaVendor);
            holder.tvListItemBrand= (TextView) convertView.findViewById(R.id.tvKodeSPN);
            holder.tvItem = (TextView) convertView.findViewById(R.id.tvQtyReceived);
            holder.tvIdPo = (TextView) convertView.findViewById(R.id.tvIdPo);
            holder.imgItem = (ImageView) convertView.findViewById(R.id.imgItem);
            holder.tvIDVendor = (TextView) convertView.findViewById(R.id.tvIDVendor);
            holder.tvTglRencanaKirim = (TextView) convertView.findViewById(R.id.tvTglRencanaTerima);
            holder.tvTransactionType = (TextView) convertView.findViewById(R.id.tvTransactionType);

            convertView.setTag(holder);
        }
        else
            holder = (ViewHolder) convertView.getTag();
            holder.tvListItemName.setText(rowItem.getId());
            holder.tvListItemBrand.setText(rowItem.getKode_po());
            holder.tvItem.setText(rowItem.getItemname());
            holder.tvIdPo.setText(rowItem.getId_transaksi());
            holder.tvTglRencanaKirim.setText(rowItem.getTglrencanaterima());
            holder.tvTransactionType.setText(rowItem.getTransactiontype());

            if(rowItem.getTransactiontype().equals("Kontrak")) {
                holder.imgItem.setImageResource(R.drawable.ic_spn_basedkontrak);
                holder.imgItem.setBackgroundResource(R.drawable.btn_orange_selector);
            }
            else if(rowItem.getTransactiontype().equals("PO"))
            {
                holder.imgItem.setImageResource(R.drawable.ic_spn_basedpo);
                holder.imgItem.setBackgroundResource(R.drawable.btn_blue_selector);
            }
            else
            {
                holder.imgItem.setImageResource(R.drawable.ic_spn_basednpo);
                holder.imgItem.setBackgroundResource(R.drawable.btn_green_selector);
            }
        //holder.imgItem.setImageResource(null);

        //CheckedTextView checkedTextView = (CheckedTextView)convertView.findViewById(R.id.checkSort);
        //checkedTextView.setVisibility(View.INVISIBLE);

        return convertView;
    }

    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());

        commonEntArrayList.clear();

        if (charText.length() == 0) {
            commonEntArrayList.addAll(arraylist);

        } else {
            for (UpcomingListPoEnt postDetail : arraylist) {
                if (charText.length() != 0 && postDetail.getTglrencanaterima().toLowerCase(Locale.getDefault()).contains(charText)) {
                    commonEntArrayList.add(postDetail);
                }
                else if (charText.length() != 0 && postDetail.getId().toLowerCase(Locale.getDefault()).contains(charText)) {
                    commonEntArrayList.add(postDetail);
                }
                else if (charText.length() != 0 && postDetail.getKode_po().toLowerCase(Locale.getDefault()).contains(charText)) {
                    commonEntArrayList.add(postDetail);
                }
            }
        }

        notifyDataSetChanged();
    }

}
