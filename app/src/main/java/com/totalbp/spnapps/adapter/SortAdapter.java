package com.totalbp.spnapps.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.totalbp.spnapps.R;
import com.totalbp.spnapps.model.ProjectEnt;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by 140030 on 07/06/2017.
 */

public class SortAdapter extends BaseAdapter {
    Context context;
    ArrayList<ProjectEnt> commonEntArrayList;
    ArrayList<ProjectEnt> arraylist;

    public SortAdapter(Context context, ArrayList<ProjectEnt> commonEntArrayList) {
        this.context = context;
        this.commonEntArrayList = commonEntArrayList;

        arraylist =  new ArrayList<ProjectEnt>();
        arraylist.addAll(commonEntArrayList);
    }

    private class ViewHolder{
        TextView txtParameterId;
        TextView txtParameterName1;
        TextView txtParameterName2;
    }

    @Override
    public int getCount() {
        return commonEntArrayList.size();

    }

    @Override
    public Object getItem(int position) {
        return commonEntArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        ProjectEnt rowItem = commonEntArrayList.get(position);
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_row,null);
            holder = new ViewHolder();
            holder.txtParameterId = (TextView) convertView.findViewById(R.id.tvNamaVendor);
            holder.txtParameterName1 = (TextView) convertView.findViewById(R.id.tvKodeSPN);
            holder.txtParameterName2 = (TextView) convertView.findViewById(R.id.textViewRowParameterName2);

            convertView.setTag(holder);
        }
        else
            holder = (ViewHolder) convertView.getTag();
        holder.txtParameterId.setText(rowItem.getId());
        holder.txtParameterName1.setText(rowItem.getName1());
        holder.txtParameterName2.setText(rowItem.getName2());


        //CheckedTextView checkedTextView = (CheckedTextView)convertView.findViewById(R.id.checkSort);
        //checkedTextView.setVisibility(View.INVISIBLE);

        return convertView;
    }


    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());

        commonEntArrayList.clear();

        if (charText.length() == 0) {
            commonEntArrayList.addAll(arraylist);

        } else {
            for (ProjectEnt postDetail : arraylist) {
                if (charText.length() != 0 && postDetail.getId().toLowerCase(Locale.getDefault()).contains(charText)) {
                    commonEntArrayList.add(postDetail);
                }
                else if (charText.length() != 0 && postDetail.getName1().toLowerCase(Locale.getDefault()).contains(charText)) {
                    commonEntArrayList.add(postDetail);
                }
                else if (charText.length() != 0 && postDetail.getName2().toLowerCase(Locale.getDefault()).contains(charText)) {
                    commonEntArrayList.add(postDetail);
                }
            }
        }

        notifyDataSetChanged();
    }

}
