package com.totalbp.spnapps.spndb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.totalbp.spnapps.model.SPNInsertItemQtyReceivedDetilTemp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ezra.R on 06/04/2018.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 4;

    // Database Name
    private static final String DATABASE_NAME = "SPNtempDb";

    // Contacts table name
    private static final String TABLE_USERS = "listiteminput";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_DESKRIPSI = "deskripsi";
    private static final String KEY_SPESIFIKASI = "spesifikasi";
    private static final String KEY_VOLUME = "volume";
    private static final String KEY_IDINDEX = "idindex";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ITEM_TABLE = "CREATE TABLE " + TABLE_USERS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_DESKRIPSI + " TEXT," + KEY_SPESIFIKASI + " TEXT,"
                + KEY_VOLUME + " REAL," + KEY_IDINDEX + " INTEGER" + ")";
        db.execSQL(CREATE_ITEM_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    public void addUser(SPNInsertItemQtyReceivedDetilTemp item) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DESKRIPSI, item.getDeskripsi());
        values.put(KEY_SPESIFIKASI, item.getSpesifikasi());
        values.put(KEY_VOLUME, item.getVolume());
        values.put(KEY_IDINDEX, item.getIdIndex());
        // Inserting Row
        db.insert(TABLE_USERS, null, values);
        db.close(); // Closing database connection
    }

    // Getting single contact
    public SPNInsertItemQtyReceivedDetilTemp getItem(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_USERS, new String[] { KEY_ID,
                        KEY_DESKRIPSI,KEY_SPESIFIKASI, KEY_VOLUME }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        SPNInsertItemQtyReceivedDetilTemp item = new SPNInsertItemQtyReceivedDetilTemp(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), Double.parseDouble(cursor.getString(3)), Integer.parseInt(cursor.getString(4)));
        // return contact
        return item;
    }

    // Getting All Items
    public List<SPNInsertItemQtyReceivedDetilTemp> getAllItems() {
        List<SPNInsertItemQtyReceivedDetilTemp> userList = new ArrayList<SPNInsertItemQtyReceivedDetilTemp>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_USERS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SPNInsertItemQtyReceivedDetilTemp item = new SPNInsertItemQtyReceivedDetilTemp();
                item.setId(Integer.parseInt(cursor.getString(0)));
                item.setDeskripsi(cursor.getString(1));
                item.setSpesifikasi(cursor.getString(2));
                item.setVolume(Double.parseDouble(cursor.getString(3)));
                item.setIdIndex(Integer.parseInt(cursor.getString(4)));
                // Adding contact to list
                userList.add(item);
            } while (cursor.moveToNext());
        }

        // return contact list
        return userList;
    }

    // Getting All Items
    public List<SPNInsertItemQtyReceivedDetilTemp> getOrderItems() {
        List<SPNInsertItemQtyReceivedDetilTemp> userList = new ArrayList<SPNInsertItemQtyReceivedDetilTemp>();
        // Select All Query
        String selectQuery = "SELECT deskripsi,spesifikasi,SUM(volume) FROM " + TABLE_USERS + " GROUP BY deskripsi,spesifikasi";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SPNInsertItemQtyReceivedDetilTemp item = new SPNInsertItemQtyReceivedDetilTemp();
                item.setDeskripsi(cursor.getString(0));
                item.setSpesifikasi(cursor.getString(1));
                item.setVolume(Double.parseDouble(cursor.getString(2)));
                // Adding contact to list
                userList.add(item);
            } while (cursor.moveToNext());
        }

        // return contact list
        return userList;
    }

    // Updating single contact
    public int updateItem(SPNInsertItemQtyReceivedDetilTemp item) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DESKRIPSI, item.getDeskripsi());
        values.put(KEY_SPESIFIKASI, item.getSpesifikasi());
        values.put(KEY_VOLUME, item.getVolume());
        // updating row
        return db.update(TABLE_USERS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(item.getId()) });
    }

    public int updateSingleField(String Field, String Value)    //belum bisa dipastikan kebenaran fungsi ini
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Field, Value);
        return db.update(TABLE_USERS, values, KEY_ID + " = ?", new String[] { String.valueOf(1) });
    }
    // Deleting single contact
    public void deleteContact(SPNInsertItemQtyReceivedDetilTemp item) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_USERS, KEY_ID + " = ?",
                new String[] { String.valueOf(item.getId()) });
        db.close();
    }


    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT * FROM " + TABLE_USERS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    // Getting contacts Count
    public void deleteTable() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_USERS);

    }

    public boolean checkForTables(){

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " +TABLE_USERS, null);

        if(cursor != null){

            cursor.moveToFirst();

            int count = cursor.getInt(0);

            if(count > 0){
                return true;
            }

            cursor.close();
        }

        return false;
    }

}
