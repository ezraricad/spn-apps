package com.totalbp.spnapps.model;

import java.util.ArrayList;

/**
 * Created by Ezra.R on 08/08/2017.
 */

public class ChartSPNQtyReceiving {

    ArrayList<SPNInsertItemQtyReceivedDetil> SPNItemReceivingDetail;

    public ArrayList<SPNInsertItemQtyReceivedDetil> getSPNDetail() {
        return SPNItemReceivingDetail;
    }

    public void setSPNDetail(ArrayList<SPNInsertItemQtyReceivedDetil> SPNDetail) {
        this.SPNItemReceivingDetail = SPNDetail;
    }
}
