package com.totalbp.spnapps.model;

import java.util.ArrayList;

/**
 * Created by 140030 on 07/07/2017.
 */

public class SPNHeader {

    /**
     {"ID":"0","Kode_SPN":"1402-17G0010","No_Revisi":"0","KodeProyek":"1402",
     "Tanggal_SPN":"03-Aug-2017","Kode_Zona":"","Kode_Vendor":"24",
     "Surat_Jalan":"DR14022017080002","Keterangan":"SPN berdasarkan PO 021/1402/VI/2017",
     "Pengubah":"999999","TokenID":"0111-BD0A-4862-BC4E-0A93CCBA84BC","ModeReq":"SPN"}
     */
    String Pengubah;
    String TokenID;

    public String getPengubah() {
        return Pengubah;
    }

    public void setPengubah(String pengubah) {
        Pengubah = pengubah;
    }

    public String getTokenID() {
        return TokenID;
    }

    public void setTokenID(String tokenID) {
        TokenID = tokenID;
    }

    ArrayList<SPNDetil> SPNDetail;

    public SPNHeader() {
    }

    public ArrayList<SPNDetil> getSPNDetail() {
        return SPNDetail;
    }

    public void setSPNDetail(ArrayList<SPNDetil> SPNDetail) {
        this.SPNDetail = SPNDetail;
    }


}


