package com.totalbp.spnapps.model;

/**
 * Created by 140030 on 18/05/2017.
 */

public class AllItemsEnt {
    public String imageUrlPath;
    public String materialName;
    public String description;
    public String ordered_stock;
    public String current_stock;
    public String received_stock;
    public String spec;
    public String unit;
    public String keterangan;

    public String kode_transaksi;
    public String kode_material;
    public String kodeanak_kodematerial;
    public String keteranganpo;
    public String unit1;
    public String unit2;
    public String volume1;
    public String volume2;
    public String harga_satuan_org1;
    public String harga_satuan_org2;
    public String harga_satuan_std1;
    public String harga_satuan_std2;
    public String panjang_material;
    public String koefisien_konversi;
    public String merk;
    public String qtyAlreadyReceived;
    public String VolumeRencanaKirim;
    public String IdTdPo;
    public String TanggalRencanaKirimFormatEnglish;
    public String KodeThTransaction;
    public String spesifikasi;

    public String getTransactionType() {
        return TransactionType;
    }

    public void setTransactionType(String transactionType) {
        TransactionType = transactionType;
    }

    public String TransactionType;

    public String getQtyAlreadyReceived() {
        return qtyAlreadyReceived;
    }

    public void setQtyAlreadyReceived(String qtyAlreadyReceived) {
        this.qtyAlreadyReceived = qtyAlreadyReceived;
    }

    public String getVolumeRencanaKirim() {
        return VolumeRencanaKirim;
    }

    public void setVolumeRencanaKirim(String volumeRencanaKirim) {
        VolumeRencanaKirim = volumeRencanaKirim;
    }

    public String getIdTdPo() {
        return IdTdPo;
    }

    public void setIdTdPo(String idTdPo) {
        IdTdPo = idTdPo;
    }

    public String getTanggalRencanaKirimFormatEnglish() {
        return TanggalRencanaKirimFormatEnglish;
    }

    public void setTanggalRencanaKirimFormatEnglish(String tanggalRencanaKirimFormatEnglish) {
        TanggalRencanaKirimFormatEnglish = tanggalRencanaKirimFormatEnglish;
    }

    public String getReceived_stock() {
        return received_stock;
    }

    public void setReceived_stock(String received_stock) {
        this.received_stock = received_stock;
    }

    public String getKode_transaksi() {
        return kode_transaksi;
    }

    public void setKode_transaksi(String kode_transaksi) {
        this.kode_transaksi = kode_transaksi;
    }

    public String getKode_material() {
        return kode_material;
    }

    public void setKode_material(String kode_material) {
        this.kode_material = kode_material;
    }

    public String getKodeanak_kodematerial() {
        return kodeanak_kodematerial;
    }

    public void setKodeanak_kodematerial(String kodeanak_kodematerial) {
        this.kodeanak_kodematerial = kodeanak_kodematerial;
    }

    public String getKeteranganpo() {
        return keteranganpo;
    }

    public void setKeteranganpo(String keteranganpo) {
        this.keteranganpo = keteranganpo;
    }

    public String getUnit1() {
        return unit1;
    }

    public void setUnit1(String unit1) {
        this.unit1 = unit1;
    }

    public String getUnit2() {
        return unit2;
    }

    public void setUnit2(String unit2) {
        this.unit2 = unit2;
    }

    public String getVolume1() {
        return volume1;
    }

    public void setVolume1(String volume1) {
        this.volume1 = volume1;
    }

    public String getVolume2() {
        return volume2;
    }

    public void setVolume2(String volume2) {
        this.volume2 = volume2;
    }

    public String getHarga_satuan_org1() {
        return harga_satuan_org1;
    }

    public void setHarga_satuan_org1(String harga_satuan_org1) {
        this.harga_satuan_org1 = harga_satuan_org1;
    }

    public String getHarga_satuan_org2() {
        return harga_satuan_org2;
    }

    public void setHarga_satuan_org2(String harga_satuan_org2) {
        this.harga_satuan_org2 = harga_satuan_org2;
    }

    public String getHarga_satuan_std1() {
        return harga_satuan_std1;
    }

    public void setHarga_satuan_std1(String harga_satuan_std1) {
        this.harga_satuan_std1 = harga_satuan_std1;
    }

    public String getHarga_satuan_std2() {
        return harga_satuan_std2;
    }

    public void setHarga_satuan_std2(String harga_satuan_std2) {
        this.harga_satuan_std2 = harga_satuan_std2;
    }

    public String getPanjang_material() {
        return panjang_material;
    }

    public void setPanjang_material(String panjang_material) {
        this.panjang_material = panjang_material;
    }

    public String getKoefisien_konversi() {
        return koefisien_konversi;
    }

    public void setKoefisien_konversi(String koefisien_konversi) {
        this.koefisien_konversi = koefisien_konversi;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public AllItemsEnt() {
    }

    public String getKodeThTransaction() {
        return KodeThTransaction;
    }

    public void setKodeThTransaction(String kodeThTransaction) {
        KodeThTransaction = kodeThTransaction;
    }

    public String getSpesifikasi() {
        return spesifikasi;
    }

    public void setSpesifikasi(String spesifikasi) {
        this.spesifikasi = spesifikasi;
    }

    public AllItemsEnt(String imageUrlPath, String materialName,
                       String description, String ordered,
                       String current, String received, String specification,
                       String units, String keterangan,
                       String kode_transaksi, String kode_material,
                       String kodeanak_kodematerial, String keteranganpo,
                       String unit1, String unit2,
                       String volume1, String volume2,
                       String harga_satuan_org1, String harga_satuan_org2,
                       String harga_satuan_std1, String harga_satuan_std2,
                       String panjang_material, String koefisien_konversi, String merk, String qtyAlreadyReceived,
                       String VolumeRencanaKirim, String IdTdPo, String TanggalRencanaKirimFormatEnglish, String KodeThTransaction, String TransactionType, String spesifikasi) {


        this.imageUrlPath = imageUrlPath;
        this.materialName = materialName;
        this.description = description;
        this.ordered_stock = ordered;
        this.current_stock = current;
        this.received_stock = received;
        this.spec = specification;
        this.unit = units;
        this.keterangan = keterangan;

        this.kode_transaksi = kode_transaksi;
        this.kode_material = kode_material;
        this.kodeanak_kodematerial = kodeanak_kodematerial;
        this.keteranganpo = keteranganpo;
        this.unit1 = unit1;
        this.unit2 = unit2;
        this.volume1 = volume1;
        this.volume2 = volume2;
        this.harga_satuan_org1 = harga_satuan_org1;
        this.harga_satuan_org2 = harga_satuan_org2;
        this.harga_satuan_std1 = harga_satuan_std1;
        this.harga_satuan_std2 = harga_satuan_std2;
        this.panjang_material = panjang_material;
        this.koefisien_konversi = koefisien_konversi;
        this.merk = merk;
        this.qtyAlreadyReceived = qtyAlreadyReceived;
        this.VolumeRencanaKirim= VolumeRencanaKirim;
        this.IdTdPo = IdTdPo;
        this.TanggalRencanaKirimFormatEnglish = TanggalRencanaKirimFormatEnglish;
        this.KodeThTransaction = KodeThTransaction;
        this.TransactionType = TransactionType;
        this.spesifikasi = spesifikasi;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getReceived() {
        return received_stock;
    }

    public void setReceived(String received_stock) {
        this.received_stock = received_stock;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getOrdered_stock() {
        return ordered_stock;
    }

    public void setOrdered_stock(String ordered_stock) {
        this.ordered_stock = ordered_stock;
    }

    public String getCurrent_stock() {
        return current_stock;
    }

    public void setCurrent_stock(String current_stock) {
        this.current_stock = current_stock;
    }

    public String getImageUrlPath() {
        return imageUrlPath;
    }

    public void setImageUrlPath(String imageUrlPath) {
        this.imageUrlPath = imageUrlPath;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
