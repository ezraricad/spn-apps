package com.totalbp.spnapps.model;

public class ItemsEntity {


    public ItemsEntity() {
    }

    public String getId_item() {
        return id_item;
    }

    public void setId_item(String id_item) {
        this.id_item = id_item;
    }

    public String getNama_item() {
        return nama_item;
    }

    public void setNama_item(String nama_item) {
        this.nama_item = nama_item;
    }

    public String getMerk_item() {
        return merk_item;
    }

    public void setMerk_item(String merk_item) {
        this.merk_item = merk_item;
    }

    public String getQty_before() {
        return qty_before;
    }

    public void setQty_before(String qty_before) {
        this.qty_before = qty_before;
    }

    public String getQty_input() {
        return qty_input;
    }

    public void setQty_input(String qty_input) {
        this.qty_input = qty_input;
    }

    public String id_item;
    public String nama_item;
    public String merk_item;
    public String qty_before;
    public String qty_input;
    public String unit;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }



    public ItemsEntity(String id_item, String nama_item, String merk_item, String qty_before, String qty_input, String unit) {
        this.id_item = id_item;
        this.nama_item = nama_item;
        this.merk_item = merk_item;
        this.qty_before = qty_before;
        this.qty_input = qty_input;
        this.unit = unit;
    }
}

