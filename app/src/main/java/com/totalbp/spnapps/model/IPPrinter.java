package com.totalbp.spnapps.model;

/**
 * Created by RUS on 31.08.2016.
 */

public class IPPrinter {
    public IPPrinter() {
    }

    public String getIp() {
        return Ip;
    }

    public void setIp(String ip) {
        Ip = ip;
    }

    public IPPrinter(String ip) {

        Ip = ip;
    }

    public String Ip;

}
