package com.totalbp.spnapps.model;

/**
 * Created by Ezra
 */

public class PoEnt {

    public String po_code;
    public String vendor;

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String other;

    public PoEnt(String po_code, String vendor, String other) {
        this.po_code = po_code;
        this.vendor = vendor;
        this.other = other;
    }

    public PoEnt() {
    }

    public String getPo_code() {
        return po_code;
    }

    public void setPo_code(String po_code) {
        this.po_code = po_code;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }
}

