package com.totalbp.spnapps.model;

/**
 * Created by 140030 on 07/07/2017.
 */

public class SPNInsertItemQtyReceivedDetil {

    /**
    [{"ID":"215","Tipe_Transaksi":"PO","Kode_Transaksi":"226","No_Urut":"0",
            "Kode_Aktivitas":"","Kode_Material":"247","Kd_Anak_Kd_Material":"2934",
            "Keterangan":"Test","Unit1":"unit","Unit2":"unit","Volume1":"-600.0000",
            "Volume2":"-600.0000","Harga_Satuan_Org1":"0","Harga_Satuan_Org2":"0",
            "Harga_Satuan_Std1":"0","Harga_Satuan_Std2":"0","Panjang_Material":"1",
            "Koefisien_Konversi":"1","Kode_Lokasi":"","Surat_Jalan":"","No_Kendaraan":"B1324UX"}]
 */
    String ID;
    String Tipe_Transaksi;
    String ID_Transaksi;
    String Deskripsi;
    String No_Urut;
    String Kode_Aktivitas;
    String Kode_Material;
    String Kd_Anak_Kd_Material;
    String Keterangan;
    String Unit1;
    String Unit2;
    String Volume1;
    String Volume2;
    String Harga_Satuan_Org1;
    String Harga_Satuan_Org2;
    String Harga_Satuan_Std1;
    String Harga_Satuan_Std2;
    String Panjang_Material;
    String Koefisien_Konversi;
    String Kode_Lokasi;
    String Surat_Jalan;
    String No_Kendaraan;

    public String getIDTdSpn() {
        return IDTdSpn;
    }

    public void setIDTdSpn(String IDTdSpn) {
        this.IDTdSpn = IDTdSpn;
    }

    String IDTdSpn;

    public String getID_Transaksi_Detail() {
        return ID_Transaksi_Detail;
    }

    public void setID_Transaksi_Detail(String ID_Transaksi_Detail) {
        this.ID_Transaksi_Detail = ID_Transaksi_Detail;
    }

    public String getID_Transaksi() {
        return ID_Transaksi;
    }

    public void setID_Transaksi(String ID_Transaksi) {
        this.ID_Transaksi = ID_Transaksi;
    }

    String ID_Transaksi_Detail;

    public String getSpesifikasi() {
        return Spesifikasi;
    }

    public void setSpesifikasi(String spesifikasi) {
        Spesifikasi = spesifikasi;
    }

    String Spesifikasi;
    String Flag_Detail_Kontrak;
    String IndexPosition;

    public SPNInsertItemQtyReceivedDetil() {
    }

    public String getFlag_Detail_Kontrak() {
        return Flag_Detail_Kontrak;
    }

    public void setFlag_Detail_Kontrak(String flag_Detail_Kontrak) {
        Flag_Detail_Kontrak = flag_Detail_Kontrak;
    }

    public String getIndexPosition() {
        return IndexPosition;
    }

    public void setIndexPosition(String indexPosition) {
        IndexPosition = indexPosition;
    }

    public SPNInsertItemQtyReceivedDetil(String id, String tipe_Transaksi, String Id_Transaksi,
                                         String no_Urut, String kode_Aktivitas, String kode_Material,
                                         String kd_Anak_Kd_Material, String keterangan, String unit1,
                                         String unit2, String volume1, String volume2, String harga_Satuan_Org1,
                                         String harga_Satuan_Org2, String harga_Satuan_Std1, String harga_Satuan_Std2,
                                         String panjang_Material, String koefisien_Konversi, String kode_Lokasi,
                                         String surat_Jalan, String no_Kendaraan, String deskripsi, String ID_Transaksi_Detail, String IDTdSpn, String Spesifikasi, String Flag_Detail_Kontrak, String IndexPosition) {
        ID = id;
        Tipe_Transaksi = tipe_Transaksi;
        ID_Transaksi = Id_Transaksi;
        No_Urut = no_Urut;
        Kode_Aktivitas = kode_Aktivitas;
        Kode_Material = kode_Material;
        Kd_Anak_Kd_Material = kd_Anak_Kd_Material;
        Keterangan = keterangan;
        Unit1 = unit1;
        Unit2 = unit2;
        Volume1 = volume1;
        Volume2 = volume2;
        Harga_Satuan_Org1 = harga_Satuan_Org1;
        Harga_Satuan_Org2 = harga_Satuan_Org2;
        Harga_Satuan_Std1 = harga_Satuan_Std1;
        Harga_Satuan_Std2 = harga_Satuan_Std2;
        Panjang_Material = panjang_Material;
        Koefisien_Konversi = koefisien_Konversi;
        Kode_Lokasi = kode_Lokasi;
        Surat_Jalan = surat_Jalan;
        No_Kendaraan = no_Kendaraan;
        Deskripsi = deskripsi;
        this.ID_Transaksi_Detail = ID_Transaksi_Detail;
        this.IDTdSpn = IDTdSpn;
        this.Spesifikasi = Spesifikasi;
        this.Flag_Detail_Kontrak = Flag_Detail_Kontrak;
        this.IndexPosition = IndexPosition;


    }

    public String getDeskripsi() {
        return Deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        Deskripsi = deskripsi;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTipe_Transaksi() {
        return Tipe_Transaksi;
    }

    public void setTipe_Transaksi(String tipe_Transaksi) {
        Tipe_Transaksi = tipe_Transaksi;
    }

    public String getKode_Transaksi() {
        return ID_Transaksi;
    }

    public void setKode_Transaksi(String Id_Transaksi) {
        ID_Transaksi = Id_Transaksi;
    }

    public String getNo_Urut() {
        return No_Urut;
    }

    public void setNo_Urut(String no_Urut) {
        No_Urut = no_Urut;
    }

    public String getKode_Aktivitas() {
        return Kode_Aktivitas;
    }

    public void setKode_Aktivitas(String kode_Aktivitas) {
        Kode_Aktivitas = kode_Aktivitas;
    }

    public String getKode_Material() {
        return Kode_Material;
    }

    public void setKode_Material(String kode_Material) {
        Kode_Material = kode_Material;
    }

    public String getKd_Anak_Kd_Material() {
        return Kd_Anak_Kd_Material;
    }

    public void setKd_Anak_Kd_Material(String kd_Anak_Kd_Material) {
        Kd_Anak_Kd_Material = kd_Anak_Kd_Material;
    }

    public String getKeterangan() {
        return Keterangan;
    }

    public void setKeterangan(String keterangan) {
        Keterangan = keterangan;
    }

    public String getUnit1() {
        return Unit1;
    }

    public void setUnit1(String unit1) {
        Unit1 = unit1;
    }

    public String getUnit2() {
        return Unit2;
    }

    public void setUnit2(String unit2) {
        Unit2 = unit2;
    }

    public String getVolume1() {
        return Volume1;
    }

    public void setVolume1(String volume1) {
        Volume1 = volume1;
    }

    public String getVolume2() {
        return Volume2;
    }

    public void setVolume2(String volume2) {
        Volume2 = volume2;
    }

    public String getHarga_Satuan_Org1() {
        return Harga_Satuan_Org1;
    }

    public void setHarga_Satuan_Org1(String harga_Satuan_Org1) {
        Harga_Satuan_Org1 = harga_Satuan_Org1;
    }

    public String getHarga_Satuan_Org2() {
        return Harga_Satuan_Org2;
    }

    public void setHarga_Satuan_Org2(String harga_Satuan_Org2) {
        Harga_Satuan_Org2 = harga_Satuan_Org2;
    }

    public String getHarga_Satuan_Std1() {
        return Harga_Satuan_Std1;
    }

    public void setHarga_Satuan_Std1(String harga_Satuan_Std1) {
        Harga_Satuan_Std1 = harga_Satuan_Std1;
    }

    public String getHarga_Satuan_Std2() {
        return Harga_Satuan_Std2;
    }

    public void setHarga_Satuan_Std2(String harga_Satuan_Std2) {
        Harga_Satuan_Std2 = harga_Satuan_Std2;
    }

    public String getPanjang_Material() {
        return Panjang_Material;
    }

    public void setPanjang_Material(String panjang_Material) {
        Panjang_Material = panjang_Material;
    }

    public String getKoefisien_Konversi() {
        return Koefisien_Konversi;
    }

    public void setKoefisien_Konversi(String koefisien_Konversi) {
        Koefisien_Konversi = koefisien_Konversi;
    }

    public String getKode_Lokasi() {
        return Kode_Lokasi;
    }

    public void setKode_Lokasi(String kode_Lokasi) {
        Kode_Lokasi = kode_Lokasi;
    }

    public String getSurat_Jalan() {
        return Surat_Jalan;
    }

    public void setSurat_Jalan(String surat_Jalan) {
        Surat_Jalan = surat_Jalan;
    }

    public String getNo_Kendaraan() {
        return No_Kendaraan;
    }

    public void setNo_Kendaraan(String no_Kendaraan) {
        No_Kendaraan = no_Kendaraan;
    }


}


