package com.totalbp.spnapps.model;

/**
 * Created by 140030 on 07/07/2017.
 */

public class SPNInsertItemQtyReceivedDetilTemp {

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDeskripsi() {
        return Deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        Deskripsi = deskripsi;
    }

    public String getSpesifikasi() {
        return Spesifikasi;
    }

    public void setSpesifikasi(String spesifikasi) {
        Spesifikasi = spesifikasi;
    }

    public double getVolume() {
        return Volume;
    }

    public void setVolume(double volume) {
        Volume = volume;
    }

    int Id;
    String Deskripsi;
    String Spesifikasi;
    double Volume;
    int IdIndex;

    public SPNInsertItemQtyReceivedDetilTemp(int id, String deskripsi, String spesifikasi, double volume, int idIndex) {
        Id = id;
        Deskripsi = deskripsi;
        Spesifikasi = spesifikasi;
        Volume = volume;
        IdIndex = idIndex;
    }

    public int getIdIndex() {
        return IdIndex;
    }

    public void setIdIndex(int idIndex) {
        IdIndex = idIndex;
    }

    public SPNInsertItemQtyReceivedDetilTemp(String deskripsi, String spesifikasi, double volume, int idIndex) {
        Deskripsi = deskripsi;
        Spesifikasi = spesifikasi;
        Volume = volume;
        IdIndex = idIndex;
    }

    public SPNInsertItemQtyReceivedDetilTemp() {
    }

}


