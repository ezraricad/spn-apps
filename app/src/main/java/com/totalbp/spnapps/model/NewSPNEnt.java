package com.totalbp.spnapps.model;

/**
 * Created by 140030 on 07/06/2017.
 * {"ID":"1181","Kode_SPN":"1402-17G0010","No_Revisi":"0",
 * "Kode_Proyek":"1402","Tanggal_SPN":"03-Aug-2017",
 * "Kode_Zona":"","Kode_Vendor":"24","Surat_Jalan":"DR14022017080002",
 * "Keterangan":"SPN berdasarkan PO 021/1402/VI/2017"}
 *
 called: [{"No_Revisi":"0",
 "Kode_Proyek":"1402",
 "No_Verify":"",
 "Tgl_SPN":"8\/1\/2017 12:00:00 AM",
 "Tanggal_SPN":"01-Aug-2017",
 "Kode_SPN":"1402-17G0009",
 "IsUserFinalApproval":"0",
 "Status_Approval":"2",
 "Pengubah":"Testing01",
 "Pembuat":"Cis Testing 01",
 "WaktuBuat":"8\/1\/2017 4:02:50 PM",
 "Keterangan":"test",
 "Flag_Posting":"0",
 "Waktu_Verify":"",
 "Approval_Number":"APP1708040018",
 "Status_SPN":true,
 "Kode_Vendor":"3",
 "Kode_Zona":"",
 "Nama_Zona":"",
 "Nama_Vendor":"DEFGHI",
 "ID":"1180",
 "User_Verify":"",
 "WaktuUbah":"8\/4\/2017 11:53:28 AM",
 "Flag_Transfer":"",
 "Surat_Jalan":"123"},


 */

public class NewSPNEnt {


    public NewSPNEnt() {
    }

    public NewSPNEnt(String id, String kode_spn, String no_revisi, String kode_proyek, String tgl_spn, String kode_zona, String kode_vendor, String surat_jalan, String keterangan, String nama_vendor,String status_approval, String po_qty, String approval_number, String pathimagevendor) {
        this.id = id;
        this.kode_spn = kode_spn;
        this.no_revisi = no_revisi;
        this.kode_proyek = kode_proyek;
        this.tgl_spn = tgl_spn;
        this.kode_zona = kode_zona;
        this.kode_vendor = kode_vendor;
        this.surat_jalan = surat_jalan;
        this.keterangan = keterangan;
        this.nama_vendor = nama_vendor;
        this.status_approval = status_approval;
        this.po_qty = po_qty;
        this.approval_number = approval_number;
        this.pathimagevendor = pathimagevendor;
    }

    public String id;
    public String kode_spn;
    public String no_revisi;
    public String kode_proyek;
    public String tgl_spn;
    public String kode_zona;
    public String kode_vendor;
    public String surat_jalan;
    public String keterangan;
    public String nama_vendor;
    public String status_approval;

    public String getPathimagevendor() {
        return pathimagevendor;
    }

    public void setPathimagevendor(String pathimagevendor) {
        this.pathimagevendor = pathimagevendor;
    }

    public String pathimagevendor;

    public String getApproval_number() {
        return approval_number;
    }

    public void setApproval_number(String approval_number) {
        this.approval_number = approval_number;
    }

    public String approval_number;

    public String getPo_qty() {
        return po_qty;
    }

    public void setPo_qty(String po_qty) {
        this.po_qty = po_qty;
    }

    public String po_qty;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKode_spn() {
        return kode_spn;
    }

    public void setKode_spn(String kode_spn) {
        this.kode_spn = kode_spn;
    }

    public String getNo_revisi() {
        return no_revisi;
    }

    public void setNo_revisi(String no_revisi) {
        this.no_revisi = no_revisi;
    }

    public String getKode_proyek() {
        return kode_proyek;
    }

    public void setKode_proyek(String kode_proyek) {
        this.kode_proyek = kode_proyek;
    }

    public String getTgl_spn() {
        return tgl_spn;
    }

    public void setTgl_spn(String tgl_spn) {
        this.tgl_spn = tgl_spn;
    }

    public String getKode_zona() {
        return kode_zona;
    }

    public void setKode_zona(String kode_zona) {
        this.kode_zona = kode_zona;
    }

    public String getKode_vendor() {
        return kode_vendor;
    }

    public void setKode_vendor(String kode_vendor) {
        this.kode_vendor = kode_vendor;
    }

    public String getSurat_jalan() {
        return surat_jalan;
    }

    public void setSurat_jalan(String surat_jalan) {
        this.surat_jalan = surat_jalan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getNama_vendor() {
        return nama_vendor;
    }

    public void setNama_vendor(String nama_vendor) {
        this.nama_vendor = nama_vendor;
    }

    public String getStatus_approval() {
        return status_approval;
    }

    public void setStatus_approval(String status_approval) {
        this.status_approval = status_approval;
    }
}

