package com.totalbp.spnapps.model;

import java.util.ArrayList;

/**
 * Created by 140030 on 07/07/2017.
 */

public class SPNInsertItemQtyReceived {

    /**
     {"ID":"0","Kode_SPN":"1402-17G0010","No_Revisi":"0","KodeProyek":"1402",
     "Tanggal_SPN":"03-Aug-2017","Kode_Zona":"","Kode_Vendor":"24",
     "Surat_Jalan":"DR14022017080002","Keterangan":"SPN berdasarkan PO 021/1402/VI/2017",
     "Pengubah":"999999","TokenID":"0111-BD0A-4862-BC4E-0A93CCBA84BC","ModeReq":"SPN"}
     */
    String ID;
    String Kode_SPN;
    String No_Revisi;
    String KodeProyek;
    String Tanggal_SPN;
    String Kode_Zona;
    String Kode_Vendor;
    String Surat_Jalan;
    String Keterangan;
    String Pengubah;
    String TokenID;
    String ModeReq;
    String AppprovalNo;
    String Status_SPN;

    public String getNoKendaraan() {
        return No_Kendaraan;
    }

    public void setNoKendaraan(String noKendaraan) {
        No_Kendaraan = noKendaraan;
    }

    String No_Kendaraan;

    public String getFormid() {
        return formid;
    }

    public void setFormid(String formid) {
        this.formid = formid;
    }

    String formid;

    public String getStatus_SPN() {
        return Status_SPN;
    }

    public void setStatus_SPN(String status_SPN) {
        Status_SPN = status_SPN;
    }



    public String getApproval_Number() {
        return AppprovalNo;
    }

    public void setApproval_Number(String approval_Number) {
        AppprovalNo = approval_Number;
    }



    ArrayList<SPNInsertItemQtyReceivedDetil> SPNDetail;

    public SPNInsertItemQtyReceived() {
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getKode_SPN() {
        return Kode_SPN;
    }

    public void setKode_SPN(String kode_SPN) {
        Kode_SPN = kode_SPN;
    }

    public String getNo_Revisi() {
        return No_Revisi;
    }

    public void setNo_Revisi(String no_Revisi) {
        No_Revisi = no_Revisi;
    }

    public String getKodeProyek() {
        return KodeProyek;
    }

    public void setKodeProyek(String kodeProyek) {
        KodeProyek = kodeProyek;
    }

    public String getTanggal_SPN() {
        return Tanggal_SPN;
    }

    public void setTanggal_SPN(String tanggal_SPN) {
        Tanggal_SPN = tanggal_SPN;
    }

    public String getKode_Zona() {
        return Kode_Zona;
    }

    public void setKode_Zona(String kode_Zona) {
        Kode_Zona = kode_Zona;
    }

    public String getKode_Vendor() {
        return Kode_Vendor;
    }

    public void setKode_Vendor(String kode_Vendor) {
        Kode_Vendor = kode_Vendor;
    }

    public String getSurat_Jalan() {
        return Surat_Jalan;
    }

    public void setSurat_Jalan(String surat_Jalan) {
        Surat_Jalan = surat_Jalan;
    }

    public String getKeterangan() {
        return Keterangan;
    }

    public void setKeterangan(String keterangan) {
        Keterangan = keterangan;
    }

    public String getPengubah() {
        return Pengubah;
    }

    public void setPengubah(String pengubah) {
        Pengubah = pengubah;
    }


    public String getTokenID() {
        return TokenID;
    }

    public void setTokenID(String tokenID) {
        TokenID = tokenID;
    }


    public String getModeReq() {
        return ModeReq;
    }


    public void setModeReq(String modeReq) {
        ModeReq = modeReq;
    }

    public ArrayList<SPNInsertItemQtyReceivedDetil> getSPNDetail() {
        return SPNDetail;
    }

    public void setSPNDetail(ArrayList<SPNInsertItemQtyReceivedDetil> SPNDetail) {
        this.SPNDetail = SPNDetail;
    }


}


