package com.totalbp.spnapps.model;

/**
 * Created by 140030 on 18/05/2017.
 */

public class AllItemsSpnDetilEnt {
    public String imageUrlPath;
    public String materialName;
    public String description;
    public String ordered_stock;
    public String current_stock;
    public String received_stock;
    public String spec;
    public String unit;
    public String keterangan;

    public String kode_transaksi;
    public String kode_material;
    public String kodeanak_kodematerial;
    public String keteranganpo;
    public String unit1;
    public String unit2;
    public String volume1;
    public String volume2;
    public String harga_satuan_org1;
    public String harga_satuan_org2;
    public String harga_satuan_std1;
    public String harga_satuan_std2;
    public String panjang_material;
    public String koefisien_konversi;
    public String merk;
    public String qtyAlreadyReceived;
    public String VolumeRencanaKirim;
    public String IdTdPo;
    public String TanggalRencanaKirimFormatEnglish;
    public String SpnDetilID;
    public String Volume1Spn;
    public String NO_Revisi;
    public String Surat_Jalan;
    public String No_Kendaraan;
    public String KeteranganTdSpn;
    public String Kode_Lokasi;
    public String spesifikasi;

    public String getNo_Urut() {
        return No_Urut;
    }

    public void setNo_Urut(String no_Urut) {
        No_Urut = no_Urut;
    }

    public String No_Urut;


    public String getSurat_Jalan() {
        return Surat_Jalan;
    }

    public void setSurat_Jalan(String surat_Jalan) {
        Surat_Jalan = surat_Jalan;
    }

    public String getNo_Kendaraan() {
        return No_Kendaraan;
    }

    public void setNo_Kendaraan(String no_Kendaraan) {
        No_Kendaraan = no_Kendaraan;
    }

    public String getKeteranganTdSpn() {
        return KeteranganTdSpn;
    }

    public void setKeteranganTdSpn(String keteranganTdSpn) {
        KeteranganTdSpn = keteranganTdSpn;
    }

    public String getKode_Lokasi() {
        return Kode_Lokasi;
    }

    public void setKode_Lokasi(String kode_Lokasi) {
        Kode_Lokasi = kode_Lokasi;
    }

    public String getTransactionType() {
        return TransactionType;
    }

    public void setTransactionType(String transactionType) {
        TransactionType = transactionType;
    }

    public String TransactionType;

    public String getQtyAlreadyReceived() {
        return qtyAlreadyReceived;
    }

    public void setQtyAlreadyReceived(String qtyAlreadyReceived) {
        this.qtyAlreadyReceived = qtyAlreadyReceived;
    }

    public String getVolumeRencanaKirim() {
        return VolumeRencanaKirim;
    }

    public void setVolumeRencanaKirim(String volumeRencanaKirim) {
        VolumeRencanaKirim = volumeRencanaKirim;
    }

    public String getIdTdPo() {
        return IdTdPo;
    }

    public void setIdTdPo(String idTdPo) {
        IdTdPo = idTdPo;
    }

    public String getTanggalRencanaKirimFormatEnglish() {
        return TanggalRencanaKirimFormatEnglish;
    }

    public void setTanggalRencanaKirimFormatEnglish(String tanggalRencanaKirimFormatEnglish) {
        TanggalRencanaKirimFormatEnglish = tanggalRencanaKirimFormatEnglish;
    }

    public String getReceived_stock() {
        return received_stock;
    }

    public void setReceived_stock(String received_stock) {
        this.received_stock = received_stock;
    }

    public String getKode_transaksi() {
        return kode_transaksi;
    }

    public void setKode_transaksi(String kode_transaksi) {
        this.kode_transaksi = kode_transaksi;
    }

    public String getKode_material() {
        return kode_material;
    }

    public void setKode_material(String kode_material) {
        this.kode_material = kode_material;
    }

    public String getKodeanak_kodematerial() {
        return kodeanak_kodematerial;
    }

    public void setKodeanak_kodematerial(String kodeanak_kodematerial) {
        this.kodeanak_kodematerial = kodeanak_kodematerial;
    }

    public String getKeteranganpo() {
        return keteranganpo;
    }

    public void setKeteranganpo(String keteranganpo) {
        this.keteranganpo = keteranganpo;
    }

    public String getUnit1() {
        return unit1;
    }

    public void setUnit1(String unit1) {
        this.unit1 = unit1;
    }

    public String getUnit2() {
        return unit2;
    }

    public void setUnit2(String unit2) {
        this.unit2 = unit2;
    }

    public String getVolume1() {
        return volume1;
    }

    public void setVolume1(String volume1) {
        this.volume1 = volume1;
    }

    public String getVolume2() {
        return volume2;
    }

    public void setVolume2(String volume2) {
        this.volume2 = volume2;
    }

    public String getHarga_satuan_org1() {
        return harga_satuan_org1;
    }

    public void setHarga_satuan_org1(String harga_satuan_org1) {
        this.harga_satuan_org1 = harga_satuan_org1;
    }

    public String getHarga_satuan_org2() {
        return harga_satuan_org2;
    }

    public void setHarga_satuan_org2(String harga_satuan_org2) {
        this.harga_satuan_org2 = harga_satuan_org2;
    }

    public String getHarga_satuan_std1() {
        return harga_satuan_std1;
    }

    public void setHarga_satuan_std1(String harga_satuan_std1) {
        this.harga_satuan_std1 = harga_satuan_std1;
    }

    public String getHarga_satuan_std2() {
        return harga_satuan_std2;
    }

    public void setHarga_satuan_std2(String harga_satuan_std2) {
        this.harga_satuan_std2 = harga_satuan_std2;
    }

    public String getPanjang_material() {
        return panjang_material;
    }

    public void setPanjang_material(String panjang_material) {
        this.panjang_material = panjang_material;
    }

    public String getKoefisien_konversi() {
        return koefisien_konversi;
    }

    public void setKoefisien_konversi(String koefisien_konversi) {
        this.koefisien_konversi = koefisien_konversi;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public AllItemsSpnDetilEnt() {
    }

    public String getSpnDetilID() {
        return SpnDetilID;
    }

    public void setSpnDetilID(String spnDetilID) {
        SpnDetilID = spnDetilID;
    }

    public String getVolume1Spn() {
        return Volume1Spn;
    }

    public void setVolume1Spn(String volume1Spn) {
        Volume1Spn = volume1Spn;
    }

    public String getNO_Revisi() {
        return NO_Revisi;
    }

    public void setNO_Revisi(String NO_Revisi) {
        this.NO_Revisi = NO_Revisi;
    }

    public String getSpesifikasi() {
        return spesifikasi;
    }

    public void setSpesifikasi(String spesifikasi) {
        this.spesifikasi = spesifikasi;
    }

    public AllItemsSpnDetilEnt(String imageUrlPath, String materialName,
                               String description, String ordered,
                               String current, String received, String specification,
                               String units, String keterangan,
                               String kode_transaksi, String kode_material,
                               String kodeanak_kodematerial, String keteranganpo,
                               String unit1, String unit2,
                               String volume1, String volume2,
                               String harga_satuan_org1, String harga_satuan_org2,
                               String harga_satuan_std1, String harga_satuan_std2,
                               String panjang_material, String koefisien_konversi, String merk, String qtyAlreadyReceived,
                               String VolumeRencanaKirim, String IdTdPo, String TanggalRencanaKirimFormatEnglish, String SpnDetilID, String Volume1Spn, String NO_Revisi, String TransactionType, String Surat_Jalan, String No_Kendaraan, String KeteranganTdSpn, String Kode_Lokasi, String No_Urut, String spesifikasi) {


        this.imageUrlPath = imageUrlPath;
        this.materialName = materialName;
        this.description = description;
        this.ordered_stock = ordered;
        this.current_stock = current;
        this.received_stock = received;
        this.spec = specification;
        this.unit = units;
        this.keterangan = keterangan;

        this.kode_transaksi = kode_transaksi;
        this.kode_material = kode_material;
        this.kodeanak_kodematerial = kodeanak_kodematerial;
        this.keteranganpo = keteranganpo;
        this.unit1 = unit1;
        this.unit2 = unit2;
        this.volume1 = volume1;
        this.volume2 = volume2;
        this.harga_satuan_org1 = harga_satuan_org1;
        this.harga_satuan_org2 = harga_satuan_org2;
        this.harga_satuan_std1 = harga_satuan_std1;
        this.harga_satuan_std2 = harga_satuan_std2;
        this.panjang_material = panjang_material;
        this.koefisien_konversi = koefisien_konversi;
        this.merk = merk;
        this.qtyAlreadyReceived = qtyAlreadyReceived;
        this.VolumeRencanaKirim= VolumeRencanaKirim;
        this.IdTdPo = IdTdPo;
        this.TanggalRencanaKirimFormatEnglish = TanggalRencanaKirimFormatEnglish;
        this.SpnDetilID = SpnDetilID;
        this.Volume1Spn = Volume1Spn;
        this.NO_Revisi = NO_Revisi;
        this.TransactionType = TransactionType;
        this.Surat_Jalan  = Surat_Jalan;
        this.No_Kendaraan  = No_Kendaraan;
        this.KeteranganTdSpn  = KeteranganTdSpn;
        this.Kode_Lokasi  = Kode_Lokasi;
        this.No_Urut = No_Urut;
        this.spesifikasi = spesifikasi;


    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getReceived() {
        return received_stock;
    }

    public void setReceived(String received_stock) {
        this.received_stock = received_stock;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getOrdered_stock() {
        return ordered_stock;
    }

    public void setOrdered_stock(String ordered_stock) {
        this.ordered_stock = ordered_stock;
    }

    public String getCurrent_stock() {
        return current_stock;
    }

    public void setCurrent_stock(String current_stock) {
        this.current_stock = current_stock;
    }

    public String getImageUrlPath() {
        return imageUrlPath;
    }

    public void setImageUrlPath(String imageUrlPath) {
        this.imageUrlPath = imageUrlPath;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
