package com.totalbp.spnapps.model;

/**
 * Created by 140030 on 07/06/2017.
 * {"ID":"1181","Kode_SPN":"1402-17G0010","No_Revisi":"0",
 * "Kode_Proyek":"1402","Tanggal_SPN":"03-Aug-2017",
 * "Kode_Zona":"","Kode_Vendor":"24","Surat_Jalan":"DR14022017080002",
 * "Keterangan":"SPN berdasarkan PO 021/1402/VI/2017"}
 */

public class ProjectEnt {


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public  String id;
    public  String name1;
    public  String name2;

    public ProjectEnt() {
    }


    public ProjectEnt(String id, String name1, String name2) {
        this.id = id;
        this.name1 = name1;
        this.name2 = name2;
}

}

