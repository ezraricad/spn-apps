package com.totalbp.spnapps.model;

/**
 * Created by 140030 on 07/06/2017.
 * {"ID":"1181","Kode_SPN":"1402-17G0010","No_Revisi":"0",
 * "Kode_Proyek":"1402","Tanggal_SPN":"03-Aug-2017",
 * "Kode_Zona":"","Kode_Vendor":"24","Surat_Jalan":"DR14022017080002",
 * "Keterangan":"SPN berdasarkan PO 021/1402/VI/2017"}
 */

public class SpinnerEnt {


    public SpinnerEnt(String value1, String value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    public  String value1;
    public  String value2;


    public SpinnerEnt() {
    }


}

