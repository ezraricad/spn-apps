package com.totalbp.spnapps.model;

/**
 * Created by Ezra, 14 Agustus 2017.
 */

public class UpcomingListPoEnt {

    public String id;
    public String kode_po;
    public String itemname;
    private String idvendor;

    public String getNodr() {
        return nodr;
    }

    public void setNodr(String nodr) {
        this.nodr = nodr;
    }

    private String nodr;

    public String getTglrencanaterima2() {
        return tglrencanaterima2;
    }

    public void setTglrencanaterima2(String tglrencanaterima2) {
        this.tglrencanaterima2 = tglrencanaterima2;
    }

    private String tglrencanaterima2;

    public String getTglrencanaterima() {
        return tglrencanaterima;
    }

    public String getIdvendor() {
        return idvendor;
    }

    public void setIdvendor(String idvendor) {
        this.idvendor = idvendor;
    }

    public void setTglrencanaterima(String tglrencanaterima) {
        this.tglrencanaterima = tglrencanaterima;
    }

    public String tglrencanaterima;

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public String transactiontype;

    public String getId_transaksi() {
        return id_transaksi;
    }

    public void setId_transaksi(String id_transaksi) {
        this.id_transaksi = id_transaksi;
    }

    public String id_transaksi;

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }



    public UpcomingListPoEnt() {
    }


    public UpcomingListPoEnt(String id, String kode_po, String itemname, String id_transaksi, String tglrencanaterima, String idvendor, String tglrencanaterima2, String transactiontype, String nodr) {
        this.id = id;
        this.kode_po = kode_po;
        this.itemname = itemname;
        this.id_transaksi = id_transaksi;
        this.tglrencanaterima = tglrencanaterima;
        this.idvendor = idvendor;
        this.tglrencanaterima2 = tglrencanaterima2;
        this.transactiontype = transactiontype;
        this.nodr = nodr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKode_po() {
        return kode_po;
    }

    public void setKode_po(String kode_po) {
        this.kode_po = kode_po;
    }






}

