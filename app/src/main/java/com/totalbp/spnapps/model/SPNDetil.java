package com.totalbp.spnapps.model;

/**
 * Created by 140030 on 07/07/2017.
 */

public class SPNDetil {

    String Kode_SPN;
    String NO_Revisi;
    String No_Urut;

    public String getKode_SPN() {
        return Kode_SPN;
    }

    public void setKode_SPN(String kode_SPN) {
        Kode_SPN = kode_SPN;
    }

    public String getNO_Revisi() {
        return NO_Revisi;
    }

    public void setNO_Revisi(String NO_Revisi) {
        this.NO_Revisi = NO_Revisi;
    }

    public String getNo_Urut() {
        return No_Urut;
    }

    public void setNo_Urut(String no_Urut) {
        No_Urut = no_Urut;
    }

    public SPNDetil() {
    }



}


